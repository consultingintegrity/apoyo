<?php require_once('../Connections/asesorias.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }
$hostname_pendientes = "localhost";
$database_pendientes = "psicologo";
$username_pendientes = "root";
$password_pendientes = "";
$pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(),E_USER_ERROR); 
	
  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes,$theValue) : mysqli_escape_string($mximg7,$theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['TEL'])) {
  $loginUsername=$_POST['TEL'];
  $password=$_POST['PSW'];
  $MM_fldUserAuthorization = "TIPO";
  $MM_redirectLoginSuccess = "inicio.php?E=1";
  $MM_redirectLoginFailed = "index.php?A=1";
  $MM_redirecttoReferrer = false;
mysqli_query($pendientes,"SET NAMES 'utf8'");	
  mysqli_select_db($pendientes,$database_pendientes );
  	
  $LoginRS__query=sprintf("SELECT ID, `NO`, PSW, TIPO, NOMBRE, IDCLIENTE, TEL, SEUDONIMO FROM usuarios WHERE TEL=%s AND PSW=%s",
  GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysqli_query($pendientes,$LoginRS__query ) or die(mysqli_error());
  $loginFoundUser = mysqli_num_rows($LoginRS);
  $row = $LoginRS->fetch_array();
  if ($loginFoundUser) {
    
    $loginStrGroup  = $row['TIPO'];
	$nombreStrGroup  = $row['NOMBRE'];
	$idquienStrGroup  = $row['ID'];
	$mmSEUDONIMO = $row['SEUDONIMO'];
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername; // TEL
    $_SESSION['MM_UserGroup'] = $loginStrGroup; // TIPO
	$_SESSION['MM_Nombre'] = $nombreStrGroup; // NOMBRE
	$_SESSION['MM_IdQuien'] = $idquienStrGroup; // ID usuario
	$_SESSION['MM_SEUDONIMO'] = $mmSEUDONIMO; // ID SEUDONIMO

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include("google.php"); ?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="apoyo-psicologico.mx">
  <meta name="author" content="aaltaan.com">
  <link rel="icon" href="http://apoyo-psicologico.mx/wp-content/uploads/2021/04/favicon-32x32-1.png" sizes="32x32" />
  <link rel="icon" href="http://apoyo-psicologico.mx/wp-content/uploads/2020/04/cropped-logo_512_ico-192x192.jpg" sizes="192x192" />
  <link rel="apple-touch-icon" href="http://apoyo-psicologico.mx/wp-content/uploads/2020/04/cropped-logo_512_ico-180x180.jpg" />

  <title>apoyo-psicologico.mx</title>

  <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">

  <link rel="stylesheet" href="../css/quirk.css">

  <script src="../lib/modernizr/modernizr.js"></script>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body class="signwrapper">

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <div class="panel signin">
    <div class="panel-heading">
		<!--<img class="logo_image" src="http://apoyo-psicologico.mx/wp-content/uploads/2020/04/logo-retina-1-300x85-1.png" >-->
      <h1>Apoyo Psicológico</h1>
      <h4 class="panel-title">Bienvenid@</h4>
    </div>
    <div class="panel-body">
      
      <form class="form-signin" id="login" name="login" method="POST" action="<?php echo $loginFormAction; ?>">
        <div class="form-group mb10">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" class="form-control" placeholder="Usuario" name="TEL">
          </div>
        </div>
        <div class="form-group nomargin">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="password" class="form-control" placeholder="Contraseña" name="PSW">
          </div>
        </div>
        <!-- <a href="recupera.php" class="forgot">Olvidaste tu contraseña?</a>--> 
        <div class="text-center" style="padding: 15px;">Horario de atención de lunes a domingo de 08:00 a 00:00 h.</div>
        <div class="form-group">
          <!--<button class="btn btn-success btn-quirk btn-block">Entrar/Continuidad de consulta</button>-->
          <button class="btn btn-success btn-quirk btn-block">Iniciar Sesión</button>
        </div>
        <div class="form-group">
        <a href="index.php" class="btn btn-primary btn-quirk btn-block btn-stroke">Crear cuenta nueva</a>
        <!--<a href="index.php" class="btn btn-primary btn-quirk btn-block btn-stroke">Consulta en línea primera vez</a>-->
      </div>
    </div>
      
      </form>
      <hr class="invisible">
      
  </div><!-- panel -->

</body>
</html>
