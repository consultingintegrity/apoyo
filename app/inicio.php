<?php require_once('../Connections/asesorias.php'); ?><?php require_once('acs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $hostname_pendientes = "localhost";
    $database_pendientes = "psicologo";
    $username_pendientes = "root";
    $password_pendientes = "";
    $pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(), E_USER_ERROR);

    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}

$maxRows_pen = 800;
$pageNum_pen = 0;
if (isset($_GET['pageNum_pen'])) {
  $pageNum_pen = $_GET['pageNum_pen'];
}
$startRow_pen = $pageNum_pen * $maxRows_pen;

$mmFILTROus = " and a.IDQUIEN =" . $_SESSION['MM_IdQuien'];
if ($_SESSION['MM_UserGroup'] == 1) {
  $mmFILTROus = " ";
}
if ($_SESSION['MM_UserGroup'] == 4) {
  $mmFILTROus = " and a.IDASIGNADO=" . $_SESSION['MM_IdQuien'];
}
if (isset($_GET['MC'])) {
  $mmFILTROus = "and a.IDASIGNADO=" . $_SESSION['MM_IdQuien'];
}

$mmFILTROestatus = "";
if (isset($_GET['EST'])) {
  $mmFILTROestatus = " and a.ESTATUS =" . $_GET['EST'];
}


mysqli_query($pendientes, "SET NAMES 'utf8'");
mysqli_select_db($pendientes, $database_pendientes);
$query_pen = "SELECT a.*, b.NOMBRE as NOMASIGNADO FROM pendiente a, usuarios b WHERE  a.IDASIGNADO=b.ID and a.ID >=1 " . $mmFILTROus . $mmFILTROestatus . " ORDER BY a.ID DESC";

$query_limit_pen = sprintf("%s LIMIT %d, %d", $query_pen, $startRow_pen, $maxRows_pen);

$pen = mysqli_query($pendientes, $query_limit_pen) or die(mysqli_error($pendientes));
$row_pen = mysqli_fetch_array($pen);

if (isset($_GET['totalRows_pen'])) {
  $totalRows_pen = $_GET['totalRows_pen'];
} else {
  $all_pen = mysqli_query($pendientes, $query_pen);
  $totalRows_pen = mysqli_num_rows($all_pen);
}
$totalPages_pen = ceil($totalRows_pen / $maxRows_pen) - 1;

$queryString_pen = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (
      stristr($param, "pageNum_pen") == false &&
      stristr($param, "totalRows_pen") == false
    ) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_pen = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_pen = sprintf("&totalRows_pen=%d%s", $totalRows_pen, $queryString_pen);

$mmP = "i";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("google.php"); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Contacto Nikon">
    <meta name="author" content="aaltaan.com">
    <!-- <meta http-equiv="refresh" content="10" />-->
    <!--actualiza la pagina cada 10 segundos-->
    <link rel="shortcut icon" href="/favicon.png">

    <title>apoyo-psicologico.mx - Inicio</title>

    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css">
    <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css">
    <link rel="stylesheet" href="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../lib/select2/select2.css">
    <link rel="stylesheet" href="../css/quirk.css">
    <script src="../lib/modernizr/modernizr.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body>
    <header><?php include("header.php"); ?></header>
    <section>
        <div class="leftpanel">
            <?php include("leftpanel.php"); ?>
            <!-- leftpanelinner -->
        </div><!-- leftpanel -->
        <div class="mainpanel">
            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-9 col-lg-8 dash-left">
                        <div class="panel panel-announcement">
                            <ul class="panel-options">
                                <li>
                                    <a href="inicio.php" title="recargar"><i class="fa fa-refresh"></i></a>
                                </li>
                                <li>
                                    <a class="panel-remove"><i class="fa fa-remove"></i></a>
                                </li>
                            </ul>
                            <div class="panel-heading">
                                <h4 class="panel-title">Bienvenid@ <?php echo $_SESSION['MM_SEUDONIMO']; ?></h4>
                            </div>
                            <div class="panel-body">
                                <h2>¡Gracias por contactarnos!</h2>
                                <p>Aquí podrás revisar el historial de tus consultas.<br>
                                    Te recuerdo que tendrás hasta tres consultas de continuidad por cada solicitud si
                                    así lo requieres.<br>
                                    Después de tus tres consultas de continuidad, si necesitas una nueva oprime el botón
                                    <strong>“Nueva Consulta”</strong> y en breve un especialista te contactará.<br>
                                    
                                    <strong>Horario de atención de lunes a domingo de 08:00 a 00:00 h.</strong>
                                </p>
                            </div>
                        </div><!-- panel -->
                        <!-- panel -->
                        <div class="panel">
                            <ul class="panel-options">
                                <li>
                                    <a href="inicio.php" title="recargar"><i class="fa fa-refresh"></i></a>
                                </li>
                                <li>
                                    <a class="panel-remove"><i class="fa fa-remove"></i></a>
                                </li>
                            </ul>
                            <div class="panel-heading">
                                <h4 class="panel-title">HISTORIAL DE CONSULTAS</h4>
                                En esta sección podrás encontrar todas tus consultas ordenadas por folio, estatus y
                                fechas.
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="dataTable1" class="table table-bordered table-striped-col">
                                        <thead>
                                            <tr>
                                                <th>FECHA EMISIÓN</th>
                                                <th>ESTATUS </th>
                                                <th>FOLIO</th>
                                                <th>TIPO DE SOLICITUD</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php if ($totalRows_pen >= 1) 
                      {
                        do { ?>
                                            <tr>
                                                <td>
                                                    <span class="text-muted">
                                                        <?php echo date("Y-m-d H:i ", strtotime($row_pen['SOLICITUD'])); ?>
                                                    </span>
                                                </td>
                                                <td>
                                                    <span class="label <?php echo $MMcolor[$row_pen['ESTATUS']]; ?>">
                                                        <?php echo $MMestatus[$row_pen['ESTATUS']]; ?>
                                                    </span>
                                                    <br>
                                                    <?php if ($row_pen['ESTATUS'] != 1) { ?>
                                                    <?php echo $row_pen['NOMASIGNADO']; ?><br><?php } ?>
                                                    <br>
                                                    <?php if ($row_pen['SOLICITUD'] != $row_pen['ENTREGASOL']) { ?>
                                                    <span class="text-muted">Asignada al:
                                                        <?php echo date("Y-m-d H:i ", strtotime($row_pen['ENTREGASOL'])); ?></span><?php } ?>
                                                </td>
                                                <td>
                                                    AYP-10<?php echo $row_pen['ID']; ?>
                                                    <?php if ($row_pen['ESTATUS'] < 3 or $_SESSION['MM_UserGroup'] < 5) { ?><a
                                                        href="detalle.php?ID=<?php echo $row_pen['ID']; ?>"
                                                        title="Entrar a la consulta"><strong>/ Entrar a la
                                                            consulta</strong></a><?php } ?><br>
                                                    <?php echo $row_pen['QUIEN']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row_pen['TITULO']; ?>
                                                </td>
                                            </tr>
                                            <?php  } while ($row_pen = mysqli_fetch_array($pen));
                      } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- row -->
                    </div><!-- col-md-9 -->
                    <?php include("ban_lateral.php"); ?>
                    <!-- col-md-3 -->
                </div><!-- row -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->

    </section>

    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../lib/jquery-toggles/toggles.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="../lib/select2/select2.js"></script>

    <script src="../js/quirk.js"></script>

    <script>
    $(document).ready(function() {

        // Add event listener for opening and closing details
        $('#exRowTable tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        function format(d) {
            // `d` is the original data object for the row
            return '<h4>' + d.name + '<small>' + d.position + '</small></h4>' +
                '<p class="nomargin">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';
        }

        // Select2
        $('select').select2({
            minimumResultsForSearch: Infinity
        });

    });

    var table = $('#dataTable1').DataTable({
        "order": [[ 0, "desc" ]],
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },


    });
    </script>
    <?php if ($_SESSION['MM_UserGroup'] != 5) { // valida que el usuario no sea paciente 
  ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/0.0.11/push.min.js"></script>

    <?php $m1 = $cuantosG; /// temporal para pruebas	
    ?>

    <script type="text/javascript">
    Push.Permission.request();

    $(document).ready(function() {

        var mmcuantosPin = '';
        var mmcuantosP = '';


        $.getJSON('c_cuantos.php', {
            id: "1"
        }, function(mmCUANTOS) {
            mmcuantosPin = mmCUANTOS.respuesta;
            //$('#grs1').text( mmcuantosPin);
            //$('#grs2').text( mmcuantosPin);
        });



        function updategr() {

            $.getJSON('c_cuantos.php', {
                id: "1"
            }, function(mmCUANTOS) {
                mmcuantosP = mmCUANTOS.respuesta;
                //$('#grs2').text( mmcuantosP);
            });


            if (mmcuantosP > mmcuantosPin) {
                $('#cuantospen').text(mmcuantosP + "+");

                Push.create('Nuevo mensage', {
                    body: 'llego una nueva solicitud',
                    icon: '/favicon.png',
                    timeout: 8000, // Timeout before notification closes automatically.
                    vibrate: [100, 100, 100], // An array of vibration pulses for mobile devices.
                    onClick: function() {
                        // Callback for when the notification is clicked. 
                        console.log(this);
                    }
                });
                mmcuantosPin = mmcuantosP;
            }


        }
        setInterval(updategr, 9000);



    });
    </script>

    <?php } // termina si el suario es != 5 
  ?>
    <!--<?php //echo $query_limit_pen; 
      ?> -->
</body>

</html>
<?php
mysqli_free_result($pen);
mysqli_close($pendientes);
?>