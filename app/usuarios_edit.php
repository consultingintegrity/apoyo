<?php require_once('../Connections/asesorias.php'); ?><?php require_once('acs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $hostname_pendientes = "localhost";
    $database_pendientes = "psicologo";
    $username_pendientes = "root";
    $password_pendientes = "";
    $pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(), E_USER_ERROR);


    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}



$mmFILTROus = "-1";
if (isset($_GET['IDU'])) {
  $mmFILTROus = "and b.ID=" . $_GET['IDU'];
}

mysqli_query($pendientes, "SET NAMES 'utf8'");
mysqli_select_db($pendientes, $database_pendientes);
$query_pen = "SELECT b.* FROM  usuarios b WHERE  b.ID >=1 " . $mmFILTROus;
$pen = mysqli_query($pendientes, $query_pen) or die(mysqli_error($pendientes));
$row_pen = mysqli_fetch_array($pen);
$totalRows_pen = 1;
////////////////////////////////////////////

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf(
    "UPDATE usuarios SET TIPO=%s,  ST=%s WHERE ID=%s",
    GetSQLValueString($_POST['TIPO'], "int"),
    GetSQLValueString($_POST['ST'], "int"),
    GetSQLValueString($_POST['ID'], "int")
  );

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $updateSQL) or die(mysqli_error($pendientes));

  $updateGoTo = "usuarios.php";

  header(sprintf("Location: %s", $updateGoTo));
}


///////////////////////////////////////////////////////
$mmP = "us";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Contacto Nikon">
  <meta name="author" content="aaltaan.com">
  <link rel="shortcut icon" href="/favicon.png">

  <title>apoyo-psicologico.mx - edit</title>

  <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
  <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css">
  <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css">
  <link rel="stylesheet" href="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
  <link rel="stylesheet" href="../lib/select2/select2.css">
  <link rel="stylesheet" href="../css/quirk.css">
  <script src="../lib/modernizr/modernizr.js"></script>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body>

  <header><?php include("header.php"); ?></header>

  <section>

    <div class="leftpanel">
      <?php include("leftpanel.php"); ?>
      <!-- leftpanelinner -->
    </div><!-- leftpanel -->

    <div class="mainpanel">

      <div class="contentpanel">

        <div class="row">
          <div class="col-md-9 col-lg-8 dash-left">

            <!-- panel -->
            <div class="panel">
              <ul class="panel-options">
                <li><a><i class="fa fa-refresh"></i></a></li>
                <li><a class="panel-remove"><i class="fa fa-remove"></i></a></li>
              </ul>
              <div class="panel-heading">
                <h4 class="panel-title">EstOS SON LOS USUARIOS REGISTRADOS.</h4>
                En esta sección podrás encontrar todas a los usuarios registrados.
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">

                    <table id="dataTable1" class="table table-bordered table-striped-col">
                      <thead>
                        <tr>
                          <th>NOMBRE</th>
                          <th>TELÉFONO</th>
                          <th>TIPO</th>
                          <th>ESTATUS</th>

                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                          <td><i class="fa fa-user"></i> <?php echo $row_pen['NOMBRE']; ?></td>
                          <td>
                            <input type="text" name="TEL" id="TEL" value="<?php echo $row_pen['TEL']; ?>">
                          </td>
                          <td>


                            <select class="form-control" style="width: 100%" name="TIPO" id="TIPO">
                              <option value="1" <?php if ($row_pen['TIPO'] == 1) { ?>selected="selected" <?php } ?>>Cordinador</option>
                              <option value="4" <?php if ($row_pen['TIPO'] == 4) { ?>selected="selected" <?php } ?>>Psicólogo</option>
                            </select>
                          </td>
                          <td>
                            <select class="form-control" style="width: 100%" name="ST" id="ST">
                              <option value="1" <?php if ($row_pen['ST'] == 1) { ?>selected="selected" <?php } ?>>Activo</option>
                              <option value="2" <?php if ($row_pen['ST'] == 2) { ?>selected="selected" <?php } ?>>Inactivo</option>
                            </select>
                          </td>

                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td><button class="btn btn-success btn-quirk btn-block" style="width: 100%" type="submit" name="submit" id="submit"><i class="fa fa-user-plus"></i> Guardar</button>
                            <input type="hidden" name="MM_update" value="form1" /><input type="hidden" name="ID" value="<?php echo $row_pen['ID']; ?>" />
                          </td>
                          <td>&nbsp;</td>
                        </tr>

                      </tbody>
                    </table>
                  </form>
                </div>
              </div>
            </div>
            <!-- row -->
          </div><!-- col-md-9 -->
          <?php include("ban_lateral.php"); ?>

          <!-- col-md-3 -->

        </div><!-- row -->

      </div><!-- contentpanel -->
    </div><!-- mainpanel -->

  </section>

  <script src="../lib/jquery/jquery.js"></script>
  <script src="../lib/jquery-ui/jquery-ui.js"></script>
  <script src="../lib/bootstrap/js/bootstrap.js"></script>
  <script src="../lib/jquery-toggles/toggles.js"></script>
  <script src="../lib/datatables/jquery.dataTables.js"></script>
  <script src="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
  <script src="../lib/select2/select2.js"></script>

  <script src="../js/quirk.js"></script>
  <script>
    $(document).ready(function() {
      'use strict';
      $('#dataTable1').DataTable();
      var exRowTable = $('#exRowTable').DataTable({
        responsive: true,
        'fnDrawCallback': function(oSettings) {
          $('#exRowTable_paginate ul').addClass('pagination-active-success');
        },
        'ajax': 'ajax/objects.txt',
        'columns': [{
            'class': 'details-control',
            'orderable': false,
            'data': null,
            'defaultContent': ''
          },
          {
            'data': 'name'
          },
          {
            'data': 'position'
          },
          {
            'data': 'office'
          },
          {
            'data': 'start_date'
          },
          {
            'data': 'salary'
          }
        ],
        'order': [
          [1, 'asc']
        ]
      });

      // Add event listener for opening and closing details
      $('#exRowTable tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = exRowTable.row(tr);

        if (row.child.isShown()) {
          // This row is already open - close it
          row.child.hide();
          tr.removeClass('shown');
        } else {
          // Open this row
          row.child(format(row.data())).show();
          tr.addClass('shown');
        }
      });

      function format(d) {
        // `d` is the original data object for the row
        return '<h4>' + d.name + '<small>' + d.position + '</small></h4>' +
          '<p class="nomargin">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';
      }

      // Select2
      $('select').select2({
        minimumResultsForSearch: Infinity
      });

    });

    var table = $('#dataTable1').DataTable({
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
      "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "Mostrar _MENU_ Entradas",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar:",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    },
  });
    });
  </script>

  <!--<?php //echo $query_limit_pen; 
      ?> -->
</body>

</html>
<?php
mysqli_free_result($pen);
?>