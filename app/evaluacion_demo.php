<?php 
date_default_timezone_set('America/Mexico_City');
require_once('../Connections/asesorias.php'); ?><?php require_once('acs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }
global $hostname_pendientes, $database_pendientes, $username_pendientes, $password_pendientes, $pendientes;
	
  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes,$theValue) : mysqli_escape_string($mximg7,$theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
} 

$mmIDPENeval="-1";
if (isset($_GET['ID'])) {
  $mmIDPENeval = $_GET['ID'];
}
mysqli_query($pendientes,"SET NAMES 'utf8'");
////////////////// evalua /////
mysqli_select_db($pendientes,$database_pendientes);
$query_eval = "SELECT d.* FROM EVAL d WHERE d.IDP =".$mmIDPENeval;
$eval= mysqli_query($pendientes,$query_eval) or die(mysqli_error($pendientes));
$row_eval = mysqli_fetch_array($eval);
$totalRows_eval = mysqli_num_rows($eval);
///////// termian evalua

////////////////// evalua /////
mysqli_select_db($pendientes,$database_pendientes);
$query_psico = "SELECT c.NOMBRE FROM usuarios c WHERE c.ID =".$row_eval['IDPSICOLOGO'];
$psico= mysqli_query($pendientes,$query_psico) or die(mysqli_error($pendientes));
$row_psico = mysqli_fetch_array($psico);
$totalRows_psico = mysqli_num_rows($psico);
///////// termian evalua

$ArraymmPA[0]="No selecciono";
$ArraymmPA[1]="Autoestima";
$ArraymmPA[2]="Duelo y pérdidas";
$ArraymmPA[3]="Pensamientos de muerte";
$ArraymmPA[4]="Ideación suicida";
$ArraymmPA[5]="Intento suicida";
$ArraymmPA[6]="Autolesión";
$ArraymmPA[7]="Problemas de relación";
$ArraymmPA[8]="Relación conflictiva con el cónyuge o la pareja";
$ArraymmPA[9]="Ruptura familiar por separación o divorcio";
$ArraymmPA[10]="Maltrato";
$ArraymmPA[11]="Abuso sexual";
$ArraymmPA[12]=" Violencia por parte del cónyuge o pareja (física, sexual, psicológica, económica)";
$ArraymmPA[13]="Maltrato por parte de una persona distinta al cónyuge";
$ArraymmPA[14]="Problemas educativos";
$ArraymmPA[15]="Problemas laborales";
$ArraymmPA[16]="Alojamiento inadecuado";
$ArraymmPA[17]="Discordia con vecino o inquilino";
$ArraymmPA[18]="Vida en una residencia";
$ArraymmPA[19]="Ingresos bajos";
$ArraymmPA[20]="Seguro social o asistencia pública insuficiente";
$ArraymmPA[21]="Problema o crisis de vida vinculado con la etapa de desarrollo";
$ArraymmPA[22]="Problema relacionado con vivir solo";
$ArraymmPA[23]="Dificultad de aculturación";
$ArraymmPA[24]="Exclusión o rechazo social";
$ArraymmPA[25]="Blanco de discriminación";
$ArraymmPA[26]="Víctima de delincuencia";
$ArraymmPA[27]="Sentencia penal sin encarcelamiento";
$ArraymmPA[28]="Encarcelamiento";
$ArraymmPA[29]="Problema relacionado con excarcelación";
$ArraymmPA[30]="Problemas relacionados con otras circunstancias legales";
$ArraymmPA[31]="Asesoramiento sexual";
$ArraymmPA[32]="Otro asesoramiento o consulta";
$ArraymmPA[33]="Problema religioso o espiritual";
$ArraymmPA[34]="Embarazo no deseado";
$ArraymmPA[35]="Multiparidad";
$ArraymmPA[36]="Víctima de tortura";
$ArraymmPA[37]="Otro problema relacionado con circunstancias psicosociales";
$ArraymmPA[38]="Historia personal con trauma psicológico";
$ArraymmPA[39]="Historia de autolesión";
$ArraymmPA[40]="Problema relacionado con el estilo de vida";
$ArraymmPA[41]="Víctima de acoso y/o ciberacoso";
$ArraymmPA[42]="Manejo de emociones (problemas de ira, estado de ánimo cambiante, etc.)";
$ArraymmPA[43]="Psicoeducación, orientación, acompañamiento";

$ArraymmPS[0]="No selecciono";
$ArraymmPS[1]="Discriminación";
$ArraymmPS[2]="Sentimiento de aislamiento";
$ArraymmPS[3]="Poco acompañamiento por parte de las autoridades";
$ArraymmPS[4]="Sentimientos de Frustración";
$ArraymmPS[5]="Falta del equipo e insumos necesarios para el desempeño adecuado";
$ArraymmPS[6]="Catársis (desahogo emocional)";

$ArraymmPD[0]="No selecciono";
$ArraymmPD[1]="Esquizofrenia y otros trastornos psicóticos";
$ArraymmPD[2]="Trastorno bipolar y trastornos relacionados";
$ArraymmPD[3]="Trastornos depresivos";
$ArraymmPD[4]="Trastornos de ansiedad";
$ArraymmPD[5]="Trastornos obsesivos-compulsivos y trastornos relacionados";
$ArraymmPD[6]="Trastornos relacionados con traumas y factores de estrés";
$ArraymmPD[7]="Trastornos de síntomas somáticos y trastornos relacionados";
$ArraymmPD[8]="Trastornos alimentarios y de la ingestión de alimentos";
$ArraymmPD[9]="Trastornos destructivos, del control de los impulsos y de la conducta";
$ArraymmPD[10]="Trastornos relacionados con sustancias y trastornos adictivos";
$ArraymmPD[11]="Trastornos de la personalidad";


?>
<strong>Descripción de la intervención:</strong><br>
<?php echo $row_eval['EVALDESCRIPCION']; ?><br><br>


<strong>Conductas de riesgo detectadas:</strong><br>
<?php echo $row_eval['EVACONDUCTAS']; ?><br><br>


<strong>Acciones o acuerdos tomados:</strong><br>
<?php echo $row_eval['EVALACCIONES']; ?><br><br>


<strong>Derivación:</strong><br>
<?php echo $row_eval['EVALDERIVACION']; ?><br><br><br>
<hr>
<strong>PROBLEMAS DE ATENCIÓN:</strong><br>
<?php
 //los valores estan en el array $ArraymmPA
if ($row_eval['PA']!=NULL){
$datarPA= explode(",", $row_eval['PA']); // convertimos el campoa en array

$mmCUANTOSPA = count($datarPA); // contamos cuantos datos son
if($mmCUANTOSPA>1) {
for ($i = 0; $i < $mmCUANTOSPA; $i++) {
  echo $ArraymmPA[$datarPA[$i]].", ";
} 
} else {
echo $ArraymmPA[$row_eval['PA']];
}
} else { echo "no selecciono"; }
?>
<br><br>

<strong>SEGMENTACIÓN EXCLUSIVA PARA PROFESIONALES DE LA SALUD:</strong><br>
<?php
 //los valores estan en el array $ArraymmPA
if ($row_eval['PS']!=NULL){
$datarPS= explode(",", $row_eval['PS']); // convertimos el campoa en array

$mmCUANTOSPS = count($datarPS); // contamos cuantos datos son
if($mmCUANTOSPS>1) {
for ($i = 0; $i < $mmCUANTOSPS; $i++) {
  echo $ArraymmPS[$datarPS[$i]].", ";
} 
} else {
echo $ArraymmPS[$row_eval['PS']];
}
} else { echo "no selecciono"; }
?>
<br>
<br>

<strong>PRE DIAGNÓSTICO DE TRASTORNOS:</strong><br>
<?php
 //los valores estan en el array $ArraymmPA
if ($row_eval['PD']!=NULL){
$datarPD= explode(",", $row_eval['PD']); // convertimos el campoa en array

$mmCUANTOSPD = count($datarPD); // contamos cuantos datos son
if($mmCUANTOSPD>1) {
for ($i = 0; $i < $mmCUANTOSPD; $i++) {
  echo $ArraymmPD[$datarPD[$i]].", ";
} 
} else {
echo $ArraymmPD[$row_eval['PD']];
}
} else { echo "no selecciono"; }
?>
<br>
<br>

<strong>Psicólogo:</strong> <?php echo $row_psico['NOMBRE']; ?>


<?php
mysqli_free_result($eval);
mysqli_free_result($psico);
mysqli_close($pendientes);
?>