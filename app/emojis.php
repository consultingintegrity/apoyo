<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>

    <h1>Emoticones y personas</h1>

    <p style="font-size:18px">
        1&#128512;2&#128513;3&#128514;4&#128515;5&#128516;6&#128517;7&#128518;8&#128519;9&#128520;10&#128521;11&#128522;12&#128523;
        13&#128524;14&#128525;15&#128526;16&#128527;17&#128528;18&#128529;19&#128530;20&#128531;21&#128532;22&#128533;23&#128534;
        24&#128535;25&#128536;26&#128537;27&#128538;28&#128539;29&#128540;30&#128541;31&#128542;32&#128543;33&#128544;34&#128545;
        35&#128546;36&#128547;37&#128548;38&#128549;39&#128550;40&#128551;41&#128552;42&#128553;43&#128554;44&#128555;45&#128556;
        46&#128557;47&#128558;48&#128559;49&#128560;50&#128561;51&#128562;52&#128563;53&#128564;54&#128566;55&#128567;56&#128568;
        57&#128569;58&#128570;59&#128571;60&#128572;61&#128573;62&#128574;63&#128575;64&#128576;65&#128577;66&#128578;67&#128579;
        68&#128580;69&#128581;70&#128582;71&#128583;72&#128584;73&#128586;74&#128587;75&#128588;76&#128589;77&#128590;78&#128591;
        79&#127744;80&#127745;81&#127746;82&#127747;83&#127748;84&#127750;85&#127751;86&#127752;87&#127753;88&#127754;89&#127755;
        90&#127756;91&#127757;92&#127758;93&#127759;94&#127760;95&#127761;96&#127762;97&#127763;98&#127764;99&#127765;100&#127766;
        101&#127767;102&#127768;103&#127769;104&#127770;105&#127771;106&#127772;107&#127773;108&#127774;109&#127775;110&#127776;
        111&#127777;112&#127778;113&#127779;114&#127780;115&#127781;116&#127782;117&#127783;118&#127784;119&#127785;120&#127787;
        121&#127788;122&#127789;123&#127790;124&#127791;125&#127792;126&#127793;127&#127794;128&#127795;129&#127796;130&#127797;
        131&#127798;132&#127799;133&#127800;134&#127801;135&#127802;136&#127803;137&#127804;138&#127805;139&#127806;140&#127807;
        141&#127808;142&#127809;143&#127810;144&#127811;145&#127812;146&#127813;147&#127814;148&#127815;149&#127816;150&#127817;
        151&#127818;152&#127819;153&#127820;154&#127821;155&#127822;156&#127823;157&#127824;158&#127825;159&#127826;160&#127827;
        161&#127828;162&#127829;163&#127830;164&#127831;165&#127832;166&#127833;167&#127834;168&#127835;169&#127836;170&#127837;
        171&#127838;172&#127839;173&#127840;174&#127841;175&#127842;176&#127843;177&#127844;178&#127845;179&#127846;180&#127847;
        181&#127848;182&#127849;183&#127850;184&#127851;185&#127852;186&#127853;187&#127854;188&#127855;189&#127856;190&#127857;
        191&#127858;192&#127859;193&#127860;194&#127861;195&#127862;196&#127863;197&#127864;198&#127865;199&#127866;200&#127867;
        201&#127868;202&#127869;203&#127870;204&#127871;205&#127872;206&#127873;207&#127874;208&#127875;209&#127876;210&#127877;
        211&#127878;212&#127879;213&#127880;214&#127881;215&#127882;216&#127883;217&#127884;218&#127885;219&#127886;220&#127887;
        221&#127888;222&#127889;223&#127890;224&#127891;225&#127892;226&#127893;227&#127894;228&#127895;229&#127896;230&#127897;
        231&#127898;232&#127899;233&#127900;234&#127901;235&#127902;236&#127903;237&#127904;238&#127905;239&#127906;240&#127907;
        241&#127908;242&#127909;243&#127910;244&#127911;245&#127912;246&#127913;247&#127914;248&#127915;249&#127916;250&#127917;
        251&#127918;252&#127919;253&#127920;254&#127921;255&#127922;256&#127923;257&#127924;258&#127925;259&#127926;260&#127927;
        261&#127928;262&#127929;263&#127930;264&#127931;265&#127932;266&#127933;267&#127934;268&#127935;269&#127936;270&#127937;
        281&#127948;282&#127949;283&#127950;284&#127951;285&#127952;286&#127953;287&#127954;288&#127955;289&#127956;300&#127957;
        301&#127958;302&#127959;303&#127960;304&#127961;305&#127962;306&#127963;307&#127964;308&#127965;309&#127966;310&#127967;
        311&#127968;312&#127969;313&#127970;314&#127970;315&#127971;316&#127972;317&#127973;318&#127974;319&#127975;320&#127976;
        321&#127977;322&#127978;323&#127979;324&#127980;325&#127981;326&#127982;327&#127983;328&#127984;329&#127985;330&#127986;
        331&#127987;332&#127988;333&#127989;334&#127990;35&#127991;336&#127992;337&#127993;338&#127994;339&#127995;340&#127996;
        341&#127997;342&#127998;343&#127999;344&#128000;345&#128001;346&#128002;347&#128003;348&#128004;349&#128005;350&#128006;
        351&#128007;352&#128008;353&#128009;354&#128010;355&#128011;356&#128012;357&#128013;358&#128014;359&#128015;360&#128016;
        361&#128017;362&#128018;363&#128019;364&#128020;365&#128021;366&#128022;367&#128023;368&#128024;369&#128025;370&#128026;
        371&#128027;372&#128028;373&#128029;374&#128030;375&#128031;376&#128032;377&#128033;378&#128034;379&#128036;380&#128037;
        381&#128038;382&#128039;382&#128040;383&#128041;384&#128042;385&#128043;386&#128044;387&#128045;388&#128046;389&#128047;
        390&#128048;391&#128049;392&#128050;393&#128051;394&#128052;395&#128053;396&#128054;397&#128055;398&#128056;399&#128057;
        400&#128058;401&#128059;402&#128060;403&#128061;404&#128062;405&#128063;406&#128064;407&#128065;408&#128066;410&#128067;
        411&#128068;412&#128069;413&#128070;414&#128071;415&#128072;416&#128073;417&#128074;418&#128075;419&#128076;420&#128077;
        421&#128078;422&#128079;423&#128080;424&#128081;425&#128082;426&#128083;427&#128084;428&#128085;429&#128086;430&#128087;
        431&#128088;432&#128089;433&#128090;434&#128091;435&#128092;436&#128093;437&#128094;438&#128095;439&#128096;450&#128097;
        451&#128098;452&#128099;453&#128100;454&#128101;455&#128102;456&#128103;457&#128104;458&#128105;459&#128106;460&#128107;
        461&#128108;462&#128109;463&#128110;464&#128111;465&#128112;466&#128113;467&#128114;468&#128115;469&#128116;470&#128117;
        471&#128118;472&#128119;472&#128120;473&#128121;474&#128122;475&#128123;476&#128124;477&#128125;478&#128126;479&#128127;
        480&#128128;481&#128129;482&#128130;483&#128131;484&#128132;485&#128133;486&#128134;487&#128135;488&#128136;489&#128137;
        490&#128138;491&#128139;492&#128140;493&#128141;494&#128142;495&#128143;496&#128144;497&#128145;498&#128146;499&#128147;
        500&#128148;501&#128149;502&#128150;503&#128151;



    </p>
    <div class="emocat" data-name="Smileys">
        <div><input type="text" value="😀" readonly> <span>Grinning Face</span></div>
        <div><input type="text" value="😁" readonly> <span>Beaming Face With Smiling Eyes</span></div>
        <div><input type="text" value="😂" readonly> <span>Face With Tears of Joy</span></div>
        <div><input type="text" value="🤣" readonly> <span>Rolling on the Floor Laughing</span></div>
        <div><input type="text" value="😃" readonly> <span>Grinning Face With Big Eyes</span></div>
        <div><input type="text" value="😄" readonly> <span>Grinning Face With Smiling Eyes</span></div>
        <div><input type="text" value="😅" readonly> <span>Grinning Face With Sweat</span></div>
        <div><input type="text" value="😆" readonly> <span>Grinning Squinting Face</span></div>
        <div><input type="text" value="😉" readonly> <span>Winking Face</span></div>
        <div><input type="text" value="😊" readonly> <span>Smiling Face With Smiling Eyes</span></div>
        <div><input type="text" value="😋" readonly> <span>Face Savoring Food</span></div>
        <div><input type="text" value="😎" readonly> <span>Smiling Face With Sunglasses</span></div>
        <div><input type="text" value="😍" readonly> <span>Smiling Face With Heart-Eyes</span></div>
        <div><input type="text" value="😘" readonly> <span>Face Blowing a Kiss</span></div>
        <div><input type="text" value="😗" readonly> <span>Kissing Face</span></div>
        <div><input type="text" value="😙" readonly> <span>Kissing Face With Smiling Eyes</span></div>
        <div><input type="text" value="😚" readonly> <span>Kissing Face With Closed Eyes</span></div>
        <div><input type="text" value="🙂" readonly> <span>Slightly Smiling Face</span></div>
        <div><input type="text" value="🤗" readonly> <span>Hugging Face</span></div>
        <div><input type="text" value="🤩" readonly> <span>Star-Struck</span></div>
        <div><input type="text" value="🤔" readonly> <span>Thinking Face</span></div>
        <div><input type="text" value="🤨" readonly> <span>Face With Raised Eyebrow</span></div>
        <div><input type="text" value="😐" readonly> <span>Neutral Face</span></div>
        <div><input type="text" value="😑" readonly> <span>Expressionless Face</span></div>
        <div><input type="text" value="😶" readonly> <span>Face Without Mouth</span></div>
        <div><input type="text" value="🙄" readonly> <span>Face With Rolling Eyes</span></div>
        <div><input type="text" value="😏" readonly> <span>Smirking Face</span></div>
        <div><input type="text" value="😣" readonly> <span>Persevering Face</span></div>
        <div><input type="text" value="😥" readonly> <span>Sad but Relieved Face</span></div>
        <div><input type="text" value="😮" readonly> <span>Face With Open Mouth</span></div>
        <div><input type="text" value="🤐" readonly> <span>Zipper-Mouth Face</span></div>
        <div><input type="text" value="😯" readonly> <span>Hushed Face</span></div>
        <div><input type="text" value="😪" readonly> <span>Sleepy Face</span></div>
        <div><input type="text" value="😫" readonly> <span>Tired Face</span></div>
        <div><input type="text" value="😴" readonly> <span>Sleeping Face</span></div>
        <div><input type="text" value="😌" readonly> <span>Relieved Face</span></div>
        <div><input type="text" value="😛" readonly> <span>Face With Tongue</span></div>
        <div><input type="text" value="😜" readonly> <span>Winking Face With Tongue</span></div>
        <div><input type="text" value="😝" readonly> <span>Squinting Face With Tongue</span></div>
        <div><input type="text" value="🤤" readonly> <span>Drooling Face</span></div>
        <div><input type="text" value="😒" readonly> <span>Unamused Face</span></div>
        <div><input type="text" value="😓" readonly> <span>Downcast Face With Sweat</span></div>
        <div><input type="text" value="😔" readonly> <span>Pensive Face</span></div>
        <div><input type="text" value="😕" readonly> <span>Confused Face</span></div>
        <div><input type="text" value="🙃" readonly> <span>Upside-Down Face</span></div>
        <div><input type="text" value="🤑" readonly> <span>Money-Mouth Face</span></div>
        <div><input type="text" value="😲" readonly> <span>Astonished Face</span></div>
        <div><input type="text" value="☹" readonly> <span>Frowning Face</span></div>
        <div><input type="text" value="🙁" readonly> <span>Slightly Frowning Face</span></div>
        <div><input type="text" value="😖" readonly> <span>Confounded Face</span></div>
        <div><input type="text" value="😞" readonly> <span>Disappointed Face</span></div>
        <div><input type="text" value="😟" readonly> <span>Worried Face</span></div>
        <div><input type="text" value="😤" readonly> <span>Face With Steam From Nose</span></div>
        <div><input type="text" value="😢" readonly> <span>Crying Face</span></div>
        <div><input type="text" value="😭" readonly> <span>Loudly Crying Face</span></div>
        <div><input type="text" value="😦" readonly> <span>Frowning Face With Open Mouth</span></div>
        <div><input type="text" value="😧" readonly> <span>Anguished Face</span></div>
        <div><input type="text" value="😨" readonly> <span>Fearful Face</span></div>
        <div><input type="text" value="😩" readonly> <span>Weary Face</span></div>
        <div><input type="text" value="🤯" readonly> <span>Exploding Head</span></div>
        <div><input type="text" value="😬" readonly> <span>Grimacing Face</span></div>
        <div><input type="text" value="😰" readonly> <span>Anxious Face With Sweat</span></div>
        <div><input type="text" value="😱" readonly> <span>Face Screaming in Fear</span></div>
        <div><input type="text" value="😳" readonly> <span>Flushed Face</span></div>
        <div><input type="text" value="🤪" readonly> <span>Zany Face</span></div>
        <div><input type="text" value="😵" readonly> <span>Dizzy Face</span></div>
        <div><input type="text" value="😡" readonly> <span>Pouting Face</span></div>
        <div><input type="text" value="😠" readonly> <span>Angry Face</span></div>
        <div><input type="text" value="🤬" readonly> <span>Face With Symbols on Mouth</span></div>
        <div><input type="text" value="😷" readonly> <span>Face With Medical Mask</span></div>
        <div><input type="text" value="🤒" readonly> <span>Face With Thermometer</span></div>
        <div><input type="text" value="🤕" readonly> <span>Face With Head-Bandage</span></div>
        <div><input type="text" value="🤢" readonly> <span>Nauseated Face</span></div>
        <div><input type="text" value="🤮" readonly> <span>Face Vomiting</span></div>
        <div><input type="text" value="🤧" readonly> <span>Sneezing Face</span></div>
        <div><input type="text" value="😇" readonly> <span>Smiling Face With Halo</span></div>
        <div><input type="text" value="🤠" readonly> <span>Cowboy Hat Face</span></div>
        <div><input type="text" value="🤡" readonly> <span>Clown Face</span></div>
        <div><input type="text" value="🤥" readonly> <span>Lying Face</span></div>
        <div><input type="text" value="🤫" readonly> <span>Shushing Face</span></div>
        <div><input type="text" value="🤭" readonly> <span>Face With Hand Over Mouth</span></div>
        <div><input type="text" value="🧐" readonly> <span>Face With Monocle</span></div>
        <div><input type="text" value="🤓" readonly> <span>Nerd Face</span></div>
        <div><input type="text" value="😈" readonly> <span>Red Devil With Horns</span></div>
        <div><input type="text" value="👿" readonly> <span>Red Devil Face With Horns</span></div>
        <div><input type="text" value="👹" readonly> <span>Ogre</span></div>
        <div><input type="text" value="👺" readonly> <span>Goblin</span></div>
        <div><input type="text" value="💀" readonly> <span>Skull</span></div>
        <div><input type="text" value="👻" readonly> <span>Ghost</span></div>
        <div><input type="text" value="👽" readonly> <span>Alien</span></div>
        <div><input type="text" value="🤖" readonly> <span>Robot Face</span></div>
        <div><input type="text" value="💩" readonly> <span>Pile of Poo</span></div>
    </div>
    <div class="emocat" data-name="People">
        <div><input type="text" value="👦" readonly> <span>Boy</span></div>
        <div><input type="text" value="👶" readonly> <span>Baby</span></div>
        <div><input type="text" value="👧" readonly> <span>Girl</span></div>
        <div><input type="text" value="👨" readonly> <span>Man</span></div>
        <div><input type="text" value="👩" readonly> <span>Woman</span></div>
        <div><input type="text" value="👴" readonly> <span>Old Man</span></div>
        <div><input type="text" value="👵" readonly> <span>Old Woman</span></div>
        <div><input type="text" value="👾" readonly> <span>Alien Monster</span></div>
        <div><input type="text" value="👨&zwj;⚕️" readonly> <span>Man Health Worker</span></div>
        <div><input type="text" value="👩&zwj;⚕️" readonly> <span>Woman Health Worker</span></div>
        <div><input type="text" value="👨&zwj;🎓" readonly> <span>Man Student</span></div>
        <div><input type="text" value="👩&zwj;🎓" readonly> <span>Woman Student</span></div>
        <div><input type="text" value="👨&zwj;⚖️" readonly> <span>Man Judge</span></div>
        <div><input type="text" value="👩&zwj;⚖️" readonly> <span>Woman Judge</span></div>
        <div><input type="text" value="👨&zwj;🌾" readonly> <span>Man Farmer</span></div>
        <div><input type="text" value="👩&zwj;🌾" readonly> <span>Woman Farmer</span></div>
        <div><input type="text" value="👨&zwj;🍳" readonly> <span>Man Cook</span></div>
        <div><input type="text" value="👩&zwj;🍳" readonly> <span>Woman Cook</span></div>
        <div><input type="text" value="👨&zwj;🔧" readonly> <span>Man Mechanic</span></div>
        <div><input type="text" value="👩&zwj;🔧" readonly> <span>Woman Mechanic</span></div>
        <div><input type="text" value="👨&zwj;🏭" readonly> <span>Man Factory Worker</span></div>
        <div><input type="text" value="👩&zwj;🏭" readonly> <span>Woman Factory Worker</span></div>
        <div><input type="text" value="👨&zwj;💼" readonly> <span>Man Office Worker</span></div>
        <div><input type="text" value="👩&zwj;💼" readonly> <span>Woman Office Worker</span></div>
        <div><input type="text" value="👨&zwj;🔬" readonly> <span>Man Scientist</span></div>
        <div><input type="text" value="👩&zwj;🔬" readonly> <span>Woman Scientist</span></div>
        <div><input type="text" value="👨&zwj;💻" readonly> <span>Man Technologist</span></div>
        <div><input type="text" value="👩&zwj;💻" readonly> <span>Woman Technologist</span></div>
        <div><input type="text" value="👨&zwj;🎤" readonly> <span>Man Singer</span></div>
        <div><input type="text" value="👩&zwj;🎤" readonly> <span>Woman Singer</span></div>
        <div><input type="text" value="👨&zwj;🎨" readonly> <span>Man Artist</span></div>
        <div><input type="text" value="👩&zwj;🎨" readonly> <span>Woman Artist</span></div>
        <div><input type="text" value="👨&zwj;✈️" readonly> <span>Man Pilot</span></div>
        <div><input type="text" value="👩&zwj;✈️" readonly> <span>Woman Pilot</span></div>
        <div><input type="text" value="👨&zwj;🚀" readonly> <span>Man Astronaut</span></div>
        <div><input type="text" value="👩&zwj;🚀" readonly> <span>Woman Astronaut</span></div>
        <div><input type="text" value="👨&zwj;🚒" readonly> <span>Man Firefighter</span></div>
        <div><input type="text" value="👩&zwj;🚒" readonly> <span>Woman Firefighter</span></div>
        <div><input type="text" value="👮" readonly> <span>Police Officer</span></div>
        <div><input type="text" value="👮&zwj;♂️" readonly> <span>Man Police Officer</span></div>
        <div><input type="text" value="👮&zwj;♀️" readonly> <span>Woman Police Officer</span></div>
        <div><input type="text" value="🕵" readonly> <span>Detective</span></div>
        <div><input type="text" value="🕵️&zwj;♂️" readonly> <span>Man Detective</span></div>
        <div><input type="text" value="🕵️&zwj;♀️" readonly> <span>Woman Detective</span></div>
        <div><input type="text" value="💂" readonly> <span>Guard</span></div>
        <div><input type="text" value="💂&zwj;♂️" readonly> <span>Man Guard</span></div>
        <div><input type="text" value="💂&zwj;♀️" readonly> <span>Woman Guard</span></div>
        <div><input type="text" value="👷" readonly> <span>Construction Worker</span></div>
        <div><input type="text" value="👷&zwj;♂️" readonly> <span>Man Construction Worker</span></div>
        <div><input type="text" value="👷&zwj;♀️" readonly> <span>Woman Construction Worker</span></div>
        <div><input type="text" value="🤴" readonly> <span>Prince</span></div>
        <div><input type="text" value="👸" readonly> <span>Princess</span></div>
        <div><input type="text" value="👳" readonly> <span>Person Wearing Turban</span></div>
        <div><input type="text" value="👳&zwj;♂️" readonly> <span>Man Wearing Turban</span></div>
        <div><input type="text" value="👳&zwj;♀️" readonly> <span>Woman Wearing Turban</span></div>
        <div><input type="text" value="👲" readonly> <span>Man With Chinese Cap</span></div>
        <div><input type="text" value="🧕" readonly> <span>Woman With Headscarf</span></div>
        <div><input type="text" value="🧔" readonly> <span>Bearded Person</span></div>
        <div><input type="text" value="👱" readonly> <span>Blond-Haired Person</span></div>
        <div><input type="text" value="👱&zwj;♂️" readonly> <span>Blond-Haired Man</span></div>
        <div><input type="text" value="👱&zwj;♀️" readonly> <span>Blond-Haired Woman</span></div>
        <div><input type="text" value="🤵" readonly> <span>Man in Tuxedo</span></div>
        <div><input type="text" value="👰" readonly> <span>Bride With Veil</span></div>
        <div><input type="text" value="🤰" readonly> <span>Pregnant Woman</span></div>
        <div><input type="text" value="🤱" readonly> <span>Breast-Feeding</span></div>
        <div><input type="text" value="👼" readonly> <span>Baby Angel</span></div>
        <div><input type="text" value="🎅" readonly> <span>Santa Claus</span></div>
        <div><input type="text" value="🤶" readonly> <span>Mrs. Claus</span></div>
        <div><input type="text" value="🧙&zwj;♀️" readonly> <span>Woman Mage</span></div>
        <div><input type="text" value="🧙&zwj;♂️" readonly> <span>Man Mage</span></div>
        <div><input type="text" value="🧚&zwj;♀️" readonly> <span>Woman Fairy</span></div>
        <div><input type="text" value="🧚&zwj;♂️" readonly> <span>Man Fairy</span></div>
        <div><input type="text" value="🧛&zwj;♀️" readonly> <span>Woman Vampire</span></div>
        <div><input type="text" value="🧛&zwj;♂️" readonly> <span>Man Vampire</span></div>
        <div><input type="text" value="🧜&zwj;♀️" readonly> <span>Mermaid</span></div>
        <div><input type="text" value="🧜&zwj;♂️" readonly> <span>Merman</span></div>
        <div><input type="text" value="🧝&zwj;♀️" readonly> <span>Woman Elf</span></div>
        <div><input type="text" value="🧝&zwj;♂️" readonly> <span>Man Elf</span></div>
        <div><input type="text" value="🧞&zwj;♀️" readonly> <span>Woman Genie</span></div>
        <div><input type="text" value="🧞&zwj;♂️" readonly> <span>Man Genie</span></div>
        <div><input type="text" value="🧟&zwj;♀️" readonly> <span>Woman Zombie</span></div>
        <div><input type="text" value="🧟&zwj;♂️" readonly> <span>Man Zombie</span></div>
        <div><input type="text" value="🙍" readonly> <span>Person Frowning</span></div>
        <div><input type="text" value="🙍&zwj;♂️" readonly> <span>Man Frowning</span></div>
        <div><input type="text" value="🙍&zwj;♀️" readonly> <span>Woman Frowning</span></div>
        <div><input type="text" value="🙎" readonly> <span>Person Pouting</span></div>
        <div><input type="text" value="🙎&zwj;♂️" readonly> <span>Man Pouting</span></div>
        <div><input type="text" value="🙎&zwj;♀️" readonly> <span>Woman Pouting</span></div>
        <div><input type="text" value="🙅" readonly> <span>Person Gesturing No</span></div>
        <div><input type="text" value="🙅&zwj;♂️" readonly> <span>Man Gesturing No</span></div>
        <div><input type="text" value="🙅&zwj;♀️" readonly> <span>Woman Gesturing No</span></div>
        <div><input type="text" value="🙆" readonly> <span>Person Gesturing OK</span></div>
        <div><input type="text" value="🙆&zwj;♂️" readonly> <span>Man Gesturing OK</span></div>
        <div><input type="text" value="🙆&zwj;♀️" readonly> <span>Woman Gesturing OK</span></div>
        <div><input type="text" value="💁" readonly> <span>Person Tipping Hand</span></div>
        <div><input type="text" value="💁&zwj;♂️" readonly> <span>Man Tipping Hand</span></div>
        <div><input type="text" value="💁&zwj;♀️" readonly> <span>Woman Tipping Hand</span></div>
        <div><input type="text" value="🙋" readonly> <span>Person Raising Hand</span></div>
        <div><input type="text" value="🙋&zwj;♂️" readonly> <span>Man Raising Hand</span></div>
        <div><input type="text" value="🙋&zwj;♀️" readonly> <span>Woman Raising Hand</span></div>
        <div><input type="text" value="🙇" readonly> <span>Person Bowing</span></div>
        <div><input type="text" value="🙇&zwj;♂️" readonly> <span>Man Bowing</span></div>
        <div><input type="text" value="🙇&zwj;♀️" readonly> <span>Woman Bowing</span></div>
        <div><input type="text" value="🤦" readonly> <span>Person Facepalming</span></div>
        <div><input type="text" value="🤦&zwj;♂️" readonly> <span>Man Facepalming</span></div>
        <div><input type="text" value="🤦&zwj;♀️" readonly> <span>Woman Facepalming</span></div>
        <div><input type="text" value="🤷" readonly> <span>Person Shrugging</span></div>
        <div><input type="text" value="🤷&zwj;♂️" readonly> <span>Man Shrugging</span></div>
        <div><input type="text" value="🤷&zwj;♀️" readonly> <span>Woman Shrugging</span></div>
        <div><input type="text" value="💆" readonly> <span>Person Getting Massage</span></div>
        <div><input type="text" value="💆&zwj;♂️" readonly> <span>Man Getting Massage</span></div>
        <div><input type="text" value="💆&zwj;♀️" readonly> <span>Woman Getting Massage</span></div>
        <div><input type="text" value="💇" readonly> <span>Person Getting Haircut</span></div>
        <div><input type="text" value="💇&zwj;♂️" readonly> <span>Man Getting Haircut</span></div>
        <div><input type="text" value="💇&zwj;♀️" readonly> <span>Woman Getting Haircut</span></div>
        <div><input type="text" value="🚶" readonly> <span>Person Walking</span></div>
        <div><input type="text" value="🚶&zwj;♂️" readonly> <span>Man Walking</span></div>
        <div><input type="text" value="🚶&zwj;♀️" readonly> <span>Woman Walking</span></div>
        <div><input type="text" value="🏃" readonly> <span>Person Running</span></div>
        <div><input type="text" value="🏃&zwj;♂️" readonly> <span>Man Running</span></div>
        <div><input type="text" value="🏃&zwj;♀️" readonly> <span>Woman Running</span></div>
        <div><input type="text" value="💃" readonly> <span>Woman Dancing</span></div>
        <div><input type="text" value="🕺" readonly> <span>Man Dancing</span></div>
        <div><input type="text" value="👯" readonly> <span>People With Bunny Ears</span></div>
        <div><input type="text" value="👯&zwj;♂️" readonly> <span>Men With Bunny Ears</span></div>
        <div><input type="text" value="👯&zwj;♀️" readonly> <span>Women With Bunny Ears</span></div>
        <div><input type="text" value="🧖&zwj;♀️" readonly> <span>Woman in Steamy Room</span></div>
        <div><input type="text" value="🧖&zwj;♂️" readonly> <span>Man in Steamy Room</span></div>
        <div><input type="text" value="🕴" readonly> <span>Man in Suit Levitating</span></div>
        <div><input type="text" value="🗣" readonly> <span>Speaking Head</span></div>
        <div><input type="text" value="👤" readonly> <span>Bust in Silhouette</span></div>
        <div><input type="text" value="👥" readonly> <span>Busts in Silhouette</span></div>
        <div><input type="text" value="👫" readonly> <span>Man and Woman Holding Hands</span></div>
        <div><input type="text" value="👬" readonly> <span>Two Men Holding Hands</span></div>
        <div><input type="text" value="👭" readonly> <span>Two Women Holding Hands</span></div>
        <div><input type="text" value="💏" readonly> <span>Kiss</span></div>
        <div><input type="text" value="👨&zwj;❤️&zwj;💋&zwj;👨" readonly> <span>Kiss: Man, Man</span></div>
        <div><input type="text" value="👩&zwj;❤️&zwj;💋&zwj;👩" readonly> <span>Kiss: Woman, Woman</span></div>
        <div><input type="text" value="💑" readonly> <span>Couple With Heart</span></div>
        <div><input type="text" value="👨&zwj;❤️&zwj;👨" readonly> <span>Couple With Heart: Man, Man</span></div>
        <div><input type="text" value="👩&zwj;❤️&zwj;👩" readonly> <span>Couple With Heart: Woman, Woman</span></div>
        <div><input type="text" value="👪" readonly> <span>Family</span></div>
        <div><input type="text" value="👨&zwj;👩&zwj;👦" readonly> <span>Family: Man, Woman, Boy</span></div>
        <div><input type="text" value="👨&zwj;👩&zwj;👧" readonly> <span>Family: Man, Woman, Girl</span></div>
        <div><input type="text" value="👨&zwj;👩&zwj;👧&zwj;👦" readonly> <span>Family: Man, Woman, Girl, Boy</span>
        </div>
        <div><input type="text" value="👨&zwj;👩&zwj;👦&zwj;👦" readonly> <span>Family: Man, Woman, Boy, Boy</span>
        </div>
        <div><input type="text" value="👨&zwj;👩&zwj;👧&zwj;👧" readonly> <span>Family: Man, Woman, Girl, Girl</span>
        </div>
        <div><input type="text" value="👨&zwj;👨&zwj;👦" readonly> <span>Family: Man, Man, Boy</span></div>
        <div><input type="text" value="👨&zwj;👨&zwj;👧" readonly> <span>Family: Man, Man, Girl</span></div>
        <div><input type="text" value="👨&zwj;👨&zwj;👧&zwj;👦" readonly> <span>Family: Man, Man, Girl, Boy</span></div>
        <div><input type="text" value="👨&zwj;👨&zwj;👦&zwj;👦" readonly> <span>Family: Man, Man, Boy, Boy</span></div>
        <div><input type="text" value="👨&zwj;👨&zwj;👧&zwj;👧" readonly> <span>Family: Man, Man, Girl, Girl</span>
        </div>
        <div><input type="text" value="👩&zwj;👩&zwj;👦" readonly> <span>Family: Woman, Woman, Boy</span></div>
        <div><input type="text" value="👩&zwj;👩&zwj;👧" readonly> <span>Family: Woman, Woman, Girl</span></div>
        <div><input type="text" value="👩&zwj;👩&zwj;👧&zwj;👦" readonly> <span>Family: Woman, Woman, Girl, Boy</span>
        </div>
        <div><input type="text" value="👩&zwj;👩&zwj;👦&zwj;👦" readonly> <span>Family: Woman, Woman, Boy, Boy</span>
        </div>
        <div><input type="text" value="👩&zwj;👩&zwj;👧&zwj;👧" readonly> <span>Family: Woman, Woman, Girl, Girl</span>
        </div>
        <div><input type="text" value="👨&zwj;👦" readonly> <span>Family: Man, Boy</span></div>
        <div><input type="text" value="👨&zwj;👦&zwj;👦" readonly> <span>Family: Man, Boy, Boy</span></div>
        <div><input type="text" value="👨&zwj;👧" readonly> <span>Family: Man, Girl</span></div>
        <div><input type="text" value="👨&zwj;👧&zwj;👦" readonly> <span>Family: Man, Girl, Boy</span></div>
        <div><input type="text" value="👨&zwj;👧&zwj;👧" readonly> <span>Family: Man, Girl, Girl</span></div>
        <div><input type="text" value="👩&zwj;👦" readonly> <span>Family: Woman, Boy</span></div>
        <div><input type="text" value="👩&zwj;👦&zwj;👦" readonly> <span>Family: Woman, Boy, Boy</span></div>
        <div><input type="text" value="👩&zwj;👧" readonly> <span>Family: Woman, Girl</span></div>
        <div><input type="text" value="👩&zwj;👧&zwj;👦" readonly> <span>Family: Woman, Girl, Boy</span></div>
        <div><input type="text" value="👩&zwj;👧&zwj;👧" readonly> <span>Family: Woman, Girl, Girl</span></div>
    </div>
    <div class="emocat" data-name="Animals">
        <div><input type="text" value="😺" readonly> <span>Grinning Cat Face</span></div>
        <div><input type="text" value="😸" readonly> <span>Grinning Cat Face With Smiling Eyes</span></div>
        <div><input type="text" value="😹" readonly> <span>Cat Face With Tears of Joy</span></div>
        <div><input type="text" value="😻" readonly> <span>Smiling Cat Face With Heart-Eyes</span></div>
        <div><input type="text" value="😼" readonly> <span>Cat Face With Wry Smile</span></div>
        <div><input type="text" value="😽" readonly> <span>Kissing Cat Face</span></div>
        <div><input type="text" value="🙀" readonly> <span>Weary Cat Face</span></div>
        <div><input type="text" value="😿" readonly> <span>Crying Cat Face</span></div>
        <div><input type="text" value="😾" readonly> <span>Pouting Cat Face</span></div>
        <div><input type="text" value="🙈" readonly> <span>See-No-Evil Monkey</span></div>
        <div><input type="text" value="🙉" readonly> <span>Hear-No-Evil Monkey</span></div>
        <div><input type="text" value="🙊" readonly> <span>Speak-No-Evil Monkey</span></div>
        <div><input type="text" value="💥" readonly> <span>Collision</span></div>
        <div><input type="text" value="🐵" readonly> <span>Monkey Face</span></div>
        <div><input type="text" value="🐒" readonly> <span>Monkey</span></div>
        <div><input type="text" value="🦍" readonly> <span>Gorilla</span></div>
        <div><input type="text" value="🐶" readonly> <span>Dog Face</span></div>
        <div><input type="text" value="🐕" readonly> <span>Dog</span></div>
        <div><input type="text" value="🐩" readonly> <span>Poodle</span></div>
        <div><input type="text" value="🐺" readonly> <span>Wolf Face</span></div>
        <div><input type="text" value="🦊" readonly> <span>Fox Face</span></div>
        <div><input type="text" value="🐱" readonly> <span>Cat Face</span></div>
        <div><input type="text" value="🐈" readonly> <span>Cat</span></div>
        <div><input type="text" value="🦁" readonly> <span>Lion Face</span></div>
        <div><input type="text" value="🐯" readonly> <span>Tiger Face</span></div>
        <div><input type="text" value="🐅" readonly> <span>Tiger</span></div>
        <div><input type="text" value="🐆" readonly> <span>Leopard</span></div>
        <div><input type="text" value="🐴" readonly> <span>Horse Face</span></div>
        <div><input type="text" value="🐎" readonly> <span>Horse</span></div>
        <div><input type="text" value="🦄" readonly> <span>Unicorn Face</span></div>
        <div><input type="text" value="🦓" readonly> <span>Zebra</span></div>
        <div><input type="text" value="🐮" readonly> <span>Cow Face</span></div>
        <div><input type="text" value="🐂" readonly> <span>Ox</span></div>
        <div><input type="text" value="🐃" readonly> <span>Water Buffalo</span></div>
        <div><input type="text" value="🐄" readonly> <span>Cow</span></div>
        <div><input type="text" value="🐷" readonly> <span>Pig Face</span></div>
        <div><input type="text" value="🐖" readonly> <span>Pig</span></div>
        <div><input type="text" value="🐗" readonly> <span>Boar</span></div>
        <div><input type="text" value="🐽" readonly> <span>Pig Nose</span></div>
        <div><input type="text" value="🐏" readonly> <span>Ram</span></div>
        <div><input type="text" value="🐑" readonly> <span>Ewe</span></div>
        <div><input type="text" value="🐐" readonly> <span>Goat</span></div>
        <div><input type="text" value="🐪" readonly> <span>Camel</span></div>
        <div><input type="text" value="🐫" readonly> <span>Two-Hump Camel</span></div>
        <div><input type="text" value="🦒" readonly> <span>Giraffe</span></div>
        <div><input type="text" value="🐘" readonly> <span>Elephant</span></div>
        <div><input type="text" value="🦏" readonly> <span>Rhinoceros</span></div>
        <div><input type="text" value="🐭" readonly> <span>Mouse Face</span></div>
        <div><input type="text" value="🐁" readonly> <span>Mouse</span></div>
        <div><input type="text" value="🐀" readonly> <span>Rat</span></div>
        <div><input type="text" value="🐹" readonly> <span>Hamster Face</span></div>
        <div><input type="text" value="🐰" readonly> <span>Rabbit Face</span></div>
        <div><input type="text" value="🐇" readonly> <span>Rabbit</span></div>
        <div><input type="text" value="🐿" readonly> <span>Chipmunk</span></div>
        <div><input type="text" value="🦔" readonly> <span>Hedgehog</span></div>
        <div><input type="text" value="🦇" readonly> <span>Bat</span></div>
        <div><input type="text" value="🐻" readonly> <span>Bear Face</span></div>
        <div><input type="text" value="🐨" readonly> <span>Koala</span></div>
        <div><input type="text" value="🐼" readonly> <span>Panda Face</span></div>
        <div><input type="text" value="🐾" readonly> <span>Paw Prints</span></div>
        <div><input type="text" value="🦃" readonly> <span>Turkey</span></div>
        <div><input type="text" value="🐔" readonly> <span>Chicken</span></div>
        <div><input type="text" value="🐓" readonly> <span>Rooster</span></div>
        <div><input type="text" value="🐣" readonly> <span>Hatching Chick</span></div>
        <div><input type="text" value="🐤" readonly> <span>Baby Chick</span></div>
        <div><input type="text" value="🐥" readonly> <span>Front-Facing Baby Chick</span></div>
        <div><input type="text" value="🐦" readonly> <span>Bird</span></div>
        <div><input type="text" value="🐧" readonly> <span>Penguin</span></div>
        <div><input type="text" value="🕊" readonly> <span>White Dove</span></div>
        <div><input type="text" value="🦅" readonly> <span>Eagle</span></div>
        <div><input type="text" value="🦆" readonly> <span>Duck</span></div>
        <div><input type="text" value="🦉" readonly> <span>Owl</span></div>
        <div><input type="text" value="🐸" readonly> <span>Frog Face</span></div>
        <div><input type="text" value="🐊" readonly> <span>Crocodile</span></div>
        <div><input type="text" value="🐢" readonly> <span>Turtle</span></div>
        <div><input type="text" value="🦎" readonly> <span>Lizard</span></div>
        <div><input type="text" value="🐍" readonly> <span>Snake</span></div>
        <div><input type="text" value="🐲" readonly> <span>Dragon Face</span></div>
        <div><input type="text" value="🐉" readonly> <span>Dragon</span></div>
        <div><input type="text" value="🦕" readonly> <span>Sauropod</span></div>
        <div><input type="text" value="🦖" readonly> <span>T-Rex</span></div>
        <div><input type="text" value="🐳" readonly> <span>Spouting Whale</span></div>
        <div><input type="text" value="🐋" readonly> <span>Whale</span></div>
        <div><input type="text" value="🐬" readonly> <span>Dolphin</span></div>
        <div><input type="text" value="🐟" readonly> <span>Fish</span></div>
        <div><input type="text" value="🐠" readonly> <span>Tropical Fish</span></div>
        <div><input type="text" value="🐡" readonly> <span>Blowfish</span></div>
        <div><input type="text" value="🦈" readonly> <span>Shark</span></div>
        <div><input type="text" value="🐙" readonly> <span>Octopus</span></div>
        <div><input type="text" value="🐚" readonly> <span>Spiral Shell</span></div>
        <div><input type="text" value="🦀" readonly> <span>Crab</span></div>
        <div><input type="text" value="🦐" readonly> <span>Shrimp</span></div>
        <div><input type="text" value="🦑" readonly> <span>Squid</span></div>
        <div><input type="text" value="🐌" readonly> <span>Snail</span></div>
        <div><input type="text" value="🦋" readonly> <span>Butterfly</span></div>
        <div><input type="text" value="🐛" readonly> <span>Bug</span></div>
        <div><input type="text" value="🐜" readonly> <span>Ant</span></div>
        <div><input type="text" value="🐝" readonly> <span>Honeybee</span></div>
        <div><input type="text" value="🐞" readonly> <span>Lady Beetle</span></div>
        <div><input type="text" value="🦗" readonly> <span>Cricket</span></div>
        <div><input type="text" value="🕷" readonly> <span>Spider</span></div>
        <div><input type="text" value="🕸" readonly> <span>Spider Web</span></div>
        <div><input type="text" value="🦂" readonly> <span>Scorpion</span></div>
    </div>
    <div class="emocat" data-name="Plants">
        <div><input type="text" value="💐" readonly> <span>Bouquet</span></div>
        <div><input type="text" value="🌸" readonly> <span>Cherry Blossom</span></div>
        <div><input type="text" value="💮" readonly> <span>White Flower</span></div>
        <div><input type="text" value="🏵" readonly> <span>Rosette</span></div>
        <div><input type="text" value="🌹" readonly> <span>Rose</span></div>
        <div><input type="text" value="🥀" readonly> <span>Wilted Flower</span></div>
        <div><input type="text" value="🌺" readonly> <span>Hibiscus</span></div>
        <div><input type="text" value="🌻" readonly> <span>Sunflower</span></div>
        <div><input type="text" value="🌼" readonly> <span>Blossom</span></div>
        <div><input type="text" value="🌷" readonly> <span>Tulip</span></div>
        <div><input type="text" value="🌱" readonly> <span>Seedling</span></div>
        <div><input type="text" value="🌲" readonly> <span>Evergreen Tree</span></div>
        <div><input type="text" value="🌳" readonly> <span>Deciduous Tree</span></div>
        <div><input type="text" value="🌴" readonly> <span>Palm Tree</span></div>
        <div><input type="text" value="🌵" readonly> <span>Cactus</span></div>
        <div><input type="text" value="🌾" readonly> <span>Sheaf of Rice</span></div>
        <div><input type="text" value="🌿" readonly> <span>Herb</span></div>
        <div><input type="text" value="☘" readonly> <span>Shamrock</span></div>
        <div><input type="text" value="🍀" readonly> <span>Four Leaf Clover</span></div>
        <div><input type="text" value="🍁" readonly> <span>Maple Leaf</span></div>
        <div><input type="text" value="🍂" readonly> <span>Fallen Leaf</span></div>
        <div><input type="text" value="🍃" readonly> <span>Leaf Fluttering in Wind</span></div>
        <div><input type="text" value="🍄" readonly> <span>Mushroom</span></div>
        <div><input type="text" value="🌰" readonly> <span>Chestnut</span></div>
    </div>
    <div class="emocat" data-name="Nature">
        <div><input type="text" value="🌍" readonly> <span>Globe Showing Europe-Africa</span></div>
        <div><input type="text" value="🌎" readonly> <span>Globe Showing Americas</span></div>
        <div><input type="text" value="🌏" readonly> <span>Globe Showing Asia-Australia</span></div>
        <div><input type="text" value="🌐" readonly> <span>Globe With Meridians</span></div>
        <div><input type="text" value="🌑" readonly> <span>New Moon</span></div>
        <div><input type="text" value="🌒" readonly> <span>Waxing Crescent Moon</span></div>
        <div><input type="text" value="🌓" readonly> <span>First Quarter Moon</span></div>
        <div><input type="text" value="🌔" readonly> <span>Waxing Gibbous Moon</span></div>
        <div><input type="text" value="🌕" readonly> <span>Full Moon</span></div>
        <div><input type="text" value="🌖" readonly> <span>Waning Gibbous Moon</span></div>
        <div><input type="text" value="🌗" readonly> <span>Last Quarter Moon</span></div>
        <div><input type="text" value="🌘" readonly> <span>Waning Crescent Moon</span></div>
        <div><input type="text" value="🌙" readonly> <span>Crescent Moon</span></div>
        <div><input type="text" value="🌚" readonly> <span>New Moon Face</span></div>
        <div><input type="text" value="🌛" readonly> <span>First Quarter Moon Face</span></div>
        <div><input type="text" value="🌜" readonly> <span>Last Quarter Moon Face</span></div>
        <div><input type="text" value="☀" readonly> <span>Sun</span></div>
        <div><input type="text" value="🌝" readonly> <span>Full Moon Face</span></div>
        <div><input type="text" value="🌞" readonly> <span>Sun With Face</span></div>
        <div><input type="text" value="⭐" readonly> <span>White Medium Star</span></div>
        <div><input type="text" value="🌟" readonly> <span>Glowing Star</span></div>
        <div><input type="text" value="🌠" readonly> <span>Shooting Star</span></div>
        <div><input type="text" value="☁" readonly> <span>Cloud</span></div>
        <div><input type="text" value="⛅" readonly> <span>Sun Behind Cloud</span></div>
        <div><input type="text" value="⛈" readonly> <span>Cloud With Lightning and Rain</span></div>
        <div><input type="text" value="🌤" readonly> <span>Sun Behind Small Cloud</span></div>
        <div><input type="text" value="🌥" readonly> <span>Sun Behind Large Cloud</span></div>
        <div><input type="text" value="🌦" readonly> <span>Sun Behind Rain Cloud</span></div>
        <div><input type="text" value="🌧" readonly> <span>Cloud With Rain</span></div>
        <div><input type="text" value="🌨" readonly> <span>Cloud With Snow</span></div>
        <div><input type="text" value="🌩" readonly> <span>Cloud With Lightning</span></div>
        <div><input type="text" value="🌪" readonly> <span>Tornado</span></div>
        <div><input type="text" value="🌫" readonly> <span>Fog</span></div>
        <div><input type="text" value="🌬" readonly> <span>Wind Face</span></div>
        <div><input type="text" value="🌈" readonly> <span>Rainbow</span></div>
        <div><input type="text" value="☔" readonly> <span>Umbrella With Rain Drops</span></div>
        <div><input type="text" value="⚡" readonly> <span>High Voltage</span></div>
        <div><input type="text" value="❄" readonly> <span>Snowflake</span></div>
        <div><input type="text" value="☃" readonly> <span>Snowman</span></div>
        <div><input type="text" value="⛄" readonly> <span>Snowman Without Snow</span></div>
        <div><input type="text" value="☄" readonly> <span>Comet</span></div>
        <div><input type="text" value="🔥" readonly> <span>Fire</span></div>
        <div><input type="text" value="💧" readonly> <span>Droplet</span></div>
        <div><input type="text" value="🌊" readonly> <span>Water Wave</span></div>
        <div><input type="text" value="🎄" readonly> <span>Christmas Tree</span></div>
        <div><input type="text" value="✨" readonly> <span>Sparkles</span></div>
        <div><input type="text" value="🎋" readonly> <span>Tanabata Tree</span></div>
        <div><input type="text" value="🎍" readonly> <span>Pine Decoration</span></div>
        </span>
    </div>
    <div class="emocat" data-name="Food">
        <div><input type="text" value="🍇" readonly> <span>Grapes</span></div>
        <div><input type="text" value="🍈" readonly> <span>Melon</span></div>
        <div><input type="text" value="🍉" readonly> <span>Watermelon</span></div>
        <div><input type="text" value="🍊" readonly> <span>Tangerine</span></div>
        <div><input type="text" value="🍋" readonly> <span>Lemon</span></div>
        <div><input type="text" value="🍌" readonly> <span>Banana</span></div>
        <div><input type="text" value="🍍" readonly> <span>Pineapple</span></div>
        <div><input type="text" value="🍎" readonly> <span>Red Apple</span></div>
        <div><input type="text" value="🍏" readonly> <span>Green Apple</span></div>
        <div><input type="text" value="🍐" readonly> <span>Pear</span></div>
        <div><input type="text" value="🍑" readonly> <span>Peach</span></div>
        <div><input type="text" value="🍒" readonly> <span>Cherries</span></div>
        <div><input type="text" value="🍓" readonly> <span>Strawberry</span></div>
        <div><input type="text" value="🥝" readonly> <span>Kiwi Fruit</span></div>
        <div><input type="text" value="🍅" readonly> <span>Tomato</span></div>
        <div><input type="text" value="🥥" readonly> <span>Coconut</span></div>
        <div><input type="text" value="🥑" readonly> <span>Avocado</span></div>
        <div><input type="text" value="🍆" readonly> <span>Eggplant</span></div>
        <div><input type="text" value="🥔" readonly> <span>Potato</span></div>
        <div><input type="text" value="🥕" readonly> <span>Carrot</span></div>
        <div><input type="text" value="🌽" readonly> <span>Ear of Corn</span></div>
        <div><input type="text" value="🌶" readonly> <span>Hot Pepper</span></div>
        <div><input type="text" value="🥒" readonly> <span>Cucumber</span></div>
        <div><input type="text" value="🥦" readonly> <span>Broccoli</span></div>
        <div><input type="text" value="🥜" readonly> <span>Peanuts</span></div>
        <div><input type="text" value="🍞" readonly> <span>Bread</span></div>
        <div><input type="text" value="🥐" readonly> <span>Croissant</span></div>
        <div><input type="text" value="🥖" readonly> <span>Baguette Bread</span></div>
        <div><input type="text" value="🥨" readonly> <span>Pretzel</span></div>
        <div><input type="text" value="🥞" readonly> <span>Pancakes</span></div>
        <div><input type="text" value="🧀" readonly> <span>Cheese Wedge</span></div>
        <div><input type="text" value="🍖" readonly> <span>Meat on Bone</span></div>
        <div><input type="text" value="🍗" readonly> <span>Poultry Leg</span></div>
        <div><input type="text" value="🥩" readonly> <span>Cut of Meat</span></div>
        <div><input type="text" value="🥓" readonly> <span>Bacon</span></div>
        <div><input type="text" value="🍔" readonly> <span>Hamburger</span></div>
        <div><input type="text" value="🍟" readonly> <span>French Fries</span></div>
        <div><input type="text" value="🍕" readonly> <span>Pizza</span></div>
        <div><input type="text" value="🌭" readonly> <span>Hot Dog</span></div>
        <div><input type="text" value="🥪" readonly> <span>Sandwich</span></div>
        <div><input type="text" value="🌮" readonly> <span>Taco</span></div>
        <div><input type="text" value="🌯" readonly> <span>Burrito</span></div>
        <div><input type="text" value="🍳" readonly> <span>Cooking</span></div>
        <div><input type="text" value="🍲" readonly> <span>Pot of Food</span></div>
        <div><input type="text" value="🥣" readonly> <span>Bowl With Spoon</span></div>
        <div><input type="text" value="🥗" readonly> <span>Green Salad</span></div>
        <div><input type="text" value="🍿" readonly> <span>Popcorn</span></div>
        <div><input type="text" value="🥫" readonly> <span>Canned Food</span></div>
        <div><input type="text" value="🍱" readonly> <span>Bento Box</span></div>
        <div><input type="text" value="🍘" readonly> <span>Rice Cracker</span></div>
        <div><input type="text" value="🍙" readonly> <span>Rice Ball</span></div>
        <div><input type="text" value="🍚" readonly> <span>Cooked Rice</span></div>
        <div><input type="text" value="🍛" readonly> <span>Curry Rice</span></div>
        <div><input type="text" value="🍜" readonly> <span>Steaming Bowl</span></div>
        <div><input type="text" value="🍝" readonly> <span>Spaghetti</span></div>
        <div><input type="text" value="🍠" readonly> <span>Roasted Sweet Potato</span></div>
        <div><input type="text" value="🍢" readonly> <span>Oden</span></div>
        <div><input type="text" value="🍣" readonly> <span>Sushi</span></div>
        <div><input type="text" value="🍤" readonly> <span>Fried Shrimp</span></div>
        <div><input type="text" value="🍥" readonly> <span>Fish Cake With Swirl</span></div>
        <div><input type="text" value="🍡" readonly> <span>Dango</span></div>
        <div><input type="text" value="🥟" readonly> <span>Dumpling</span></div>
        <div><input type="text" value="🥠" readonly> <span>Fortune Cookie</span></div>
        <div><input type="text" value="🥡" readonly> <span>Takeout Box</span></div>
        <div><input type="text" value="🍦" readonly> <span>Soft Ice Cream</span></div>
        <div><input type="text" value="🍧" readonly> <span>Shaved Ice</span></div>
        <div><input type="text" value="🍨" readonly> <span>Ice Cream</span></div>
        <div><input type="text" value="🍩" readonly> <span>Doughnut</span></div>
        <div><input type="text" value="🍪" readonly> <span>Cookie</span></div>
        <div><input type="text" value="🎂" readonly> <span>Birthday Cake</span></div>
        <div><input type="text" value="🍰" readonly> <span>Shortcake</span></div>
        <div><input type="text" value="🥧" readonly> <span>Pie</span></div>
        <div><input type="text" value="🍫" readonly> <span>Chocolate Bar</span></div>
        <div><input type="text" value="🍬" readonly> <span>Candy</span></div>
        <div><input type="text" value="🍭" readonly> <span>Lollipop</span></div>
        <div><input type="text" value="🍮" readonly> <span>Custard</span></div>
        <div><input type="text" value="🍯" readonly> <span>Honey Pot</span></div>
        <div><input type="text" value="🍼" readonly> <span>Baby Bottle</span></div>
        <div><input type="text" value="🥛" readonly> <span>Glass of Milk</span></div>
        <div><input type="text" value="☕" readonly> <span>Cup of Hot Beverage</span></div>
        <div><input type="text" value="🍵" readonly> <span>Teacup Without Handle</span></div>
        <div><input type="text" value="🍶" readonly> <span>Sake</span></div>
        <div><input type="text" value="🍾" readonly> <span>Bottle With Popping Cork</span></div>
        <div><input type="text" value="🍷" readonly> <span>Wine Glass</span></div>
        <div><input type="text" value="🍸" readonly> <span>Cocktail Glass</span></div>
        <div><input type="text" value="🍹" readonly> <span>Tropical Drink</span></div>
        <div><input type="text" value="🍺" readonly> <span>Beer Mug</span></div>
        <div><input type="text" value="🍻" readonly> <span>Clinking Beer Mugs</span></div>
        <div><input type="text" value="🥂" readonly> <span>Clinking Glasses</span></div>
        <div><input type="text" value="🥃" readonly> <span>Tumbler Glass</span></div>
        <div><input type="text" value="🥤" readonly> <span>Cup With Straw</span></div>
        <div><input type="text" value="🥢" readonly> <span>Chopsticks</span></div>
        <div><input type="text" value="🍽" readonly> <span>Fork and Knife With Plate</span></div>
        <div><input type="text" value="🍴" readonly> <span>Fork and Knife</span></div>
        <div><input type="text" value="🥄" readonly> <span>Spoon</span></div>
    </div>
    <div class="emocat" data-name="Activity">
        <div><input type="text" value="🏇" readonly> <span>Horse Racing</span></div>
        <div><input type="text" value="⛷" readonly> <span>Skier</span></div>
        <div><input type="text" value="🏂" readonly> <span>Snowboarder</span></div>
        <div><input type="text" value="🧗&zwj;♀️" readonly> <span>Woman Climbing</span></div>
        <div><input type="text" value="🧗&zwj;♂️" readonly> <span>Man Climbing</span></div>
        <div><input type="text" value="🧘&zwj;♀️" readonly> <span>Woman in Lotus Position</span></div>
        <div><input type="text" value="🧘&zwj;♂️" readonly> <span>Man in Lotus Position</span></div>
        <div><input type="text" value="🏌" readonly> <span>Person Golfing</span></div>
        <div><input type="text" value="🏌️&zwj;♂️" readonly> <span>Man Golfing</span></div>
        <div><input type="text" value="🏌️&zwj;♀️" readonly> <span>Woman Golfing</span></div>
        <div><input type="text" value="🏄" readonly> <span>Person Surfing</span></div>
        <div><input type="text" value="🏄&zwj;♂️" readonly> <span>Man Surfing</span></div>
        <div><input type="text" value="🏄&zwj;♀️" readonly> <span>Woman Surfing</span></div>
        <div><input type="text" value="🚣" readonly> <span>Person Rowing Boat</span></div>
        <div><input type="text" value="🚣&zwj;♂️" readonly> <span>Man Rowing Boat</span></div>
        <div><input type="text" value="🚣&zwj;♀️" readonly> <span>Woman Rowing Boat</span></div>
        <div><input type="text" value="🏊" readonly> <span>Person Swimming</span></div>
        <div><input type="text" value="🏊&zwj;♂️" readonly> <span>Man Swimming</span></div>
        <div><input type="text" value="🏊&zwj;♀️" readonly> <span>Woman Swimming</span></div>
        <div><input type="text" value="⛹" readonly> <span>Person Bouncing Ball</span></div>
        <div><input type="text" value="⛹️&zwj;♂️" readonly> <span>Man Bouncing Ball</span></div>
        <div><input type="text" value="⛹️&zwj;♀️" readonly> <span>Woman Bouncing Ball</span></div>
        <div><input type="text" value="🏋" readonly> <span>Person Lifting Weights</span></div>
        <div><input type="text" value="🏋️&zwj;♂️" readonly> <span>Man Lifting Weights</span></div>
        <div><input type="text" value="🏋️&zwj;♀️" readonly> <span>Woman Lifting Weights</span></div>
        <div><input type="text" value="🚴" readonly> <span>Person Biking</span></div>
        <div><input type="text" value="🚴&zwj;♂️" readonly> <span>Man Biking</span></div>
        <div><input type="text" value="🚴&zwj;♀️" readonly> <span>Woman Biking</span></div>
        <div><input type="text" value="🚵" readonly> <span>Person Mountain Biking</span></div>
        <div><input type="text" value="🚵&zwj;♂️" readonly> <span>Man Mountain Biking</span></div>
        <div><input type="text" value="🚵&zwj;♀️" readonly> <span>Woman Mountain Biking</span></div>
        <div><input type="text" value="🤸" readonly> <span>Person Cartwheeling</span></div>
        <div><input type="text" value="🤸&zwj;♂️" readonly> <span>Man Cartwheeling</span></div>
        <div><input type="text" value="🤸&zwj;♀️" readonly> <span>Woman Cartwheeling</span></div>
        <div><input type="text" value="🤼" readonly> <span>People Wrestling</span></div>
        <div><input type="text" value="🤼&zwj;♂️" readonly> <span>Men Wrestling</span></div>
        <div><input type="text" value="🤼&zwj;♀️" readonly> <span>Women Wrestling</span></div>
        <div><input type="text" value="🤽" readonly> <span>Person Playing Water Polo</span></div>
        <div><input type="text" value="🤽&zwj;♂️" readonly> <span>Man Playing Water Polo</span></div>
        <div><input type="text" value="🤽&zwj;♀️" readonly> <span>Woman Playing Water Polo</span></div>
        <div><input type="text" value="🤾" readonly> <span>Person Playing Handball</span></div>
        <div><input type="text" value="🤾&zwj;♂️" readonly> <span>Man Playing Handball</span></div>
        <div><input type="text" value="🤾&zwj;♀️" readonly> <span>Woman Playing Handball</span></div>
        <div><input type="text" value="🤹" readonly> <span>Person Juggling</span></div>
        <div><input type="text" value="🤹&zwj;♂️" readonly> <span>Man Juggling</span></div>
        <div><input type="text" value="🤹&zwj;♀️" readonly> <span>Woman Juggling</span></div>
        <div><input type="text" value="🎪" readonly> <span>Circus Tent</span></div>
        <div><input type="text" value="🎗" readonly> <span>Reminder Ribbon</span></div>
        <div><input type="text" value="🎟" readonly> <span>Admission Tickets</span></div>
        <div><input type="text" value="🎫" readonly> <span>Ticket</span></div>
        <div><input type="text" value="🎖" readonly> <span>Military Medal</span></div>
        <div><input type="text" value="🏆" readonly> <span>Trophy</span></div>
        <div><input type="text" value="🏅" readonly> <span>Sports Medal</span></div>
        <div><input type="text" value="🥇" readonly> <span>1st Place Medal</span></div>
        <div><input type="text" value="🥈" readonly> <span>2nd Place Medal</span></div>
        <div><input type="text" value="🥉" readonly> <span>3rd Place Medal</span></div>
        <div><input type="text" value="⚽" readonly> <span>Soccer Ball</span></div>
        <div><input type="text" value="⚾" readonly> <span>Baseball</span></div>
        <div><input type="text" value="🏀" readonly> <span>Basketball</span></div>
        <div><input type="text" value="🏐" readonly> <span>Volleyball</span></div>
        <div><input type="text" value="🏈" readonly> <span>American Football</span></div>
        <div><input type="text" value="🏉" readonly> <span>Rugby Football</span></div>
        <div><input type="text" value="🎾" readonly> <span>Tennis</span></div>
        <div><input type="text" value="🎳" readonly> <span>Bowling</span></div>
        <div><input type="text" value="🏏" readonly> <span>Cricket Game</span></div>
        <div><input type="text" value="🏑" readonly> <span>Field Hockey</span></div>
        <div><input type="text" value="🏒" readonly> <span>Ice Hockey</span></div>
        <div><input type="text" value="🏓" readonly> <span>Ping Pong</span></div>
        <div><input type="text" value="🏸" readonly> <span>Badminton</span></div>
        <div><input type="text" value="🥊" readonly> <span>Boxing Glove</span></div>
        <div><input type="text" value="🥋" readonly> <span>Martial Arts Uniform</span></div>
        <div><input type="text" value="⛳" readonly> <span>Flag in Hole</span></div>
        <div><input type="text" value="⛸" readonly> <span>Ice Skate</span></div>
        <div><input type="text" value="🎣" readonly> <span>Fishing Pole</span></div>
        <div><input type="text" value="🎽" readonly> <span>Running Shirt</span></div>
        <div><input type="text" value="🎿" readonly> <span>Skis</span></div>
        <div><input type="text" value="🛷" readonly> <span>Sled</span></div>
        <div><input type="text" value="🥌" readonly> <span>Curling Stone</span></div>
        <div><input type="text" value="🎯" readonly> <span>Direct Hit</span></div>
        <div><input type="text" value="🎱" readonly> <span>Pool 8 Ball</span></div>
        <div><input type="text" value="🎮" readonly> <span>Video Game</span></div>
        <div><input type="text" value="🎰" readonly> <span>Slot Machine</span></div>
        <div><input type="text" value="🎲" readonly> <span>Game Die</span></div>
        <div><input type="text" value="🎭" readonly> <span>Performing Arts</span></div>
        <div><input type="text" value="🎨" readonly> <span>Artist Palette</span></div>
        <div><input type="text" value="🎼" readonly> <span>Musical Score</span></div>
        <div><input type="text" value="🎤" readonly> <span>Microphone</span></div>
        <div><input type="text" value="🎧" readonly> <span>Headphone</span></div>
        <div><input type="text" value="🎷" readonly> <span>Saxophone</span></div>
        <div><input type="text" value="🎸" readonly> <span>Guitar</span></div>
        <div><input type="text" value="🎹" readonly> <span>Musical Keyboard</span></div>
        <div><input type="text" value="🎺" readonly> <span>Trumpet</span></div>
        <div><input type="text" value="🎻" readonly> <span>Violin</span></div>
        <div><input type="text" value="🥁" readonly> <span>Drum</span></div>
        <div><input type="text" value="🎬" readonly> <span>Clapper Board</span></div>
        <div><input type="text" value="🏹" readonly> <span>Bow and Arrow</span></div>
    </div>
    <div class="emocat" data-name="Travel">
        <div><input type="text" value="🏖" readonly> <span>Beach With Umbrella</span></div>
        <div><input type="text" value="🏎" readonly> <span>Racing Car</span></div>
        <div><input type="text" value="🏍" readonly> <span>Motorcycle</span></div>
        <div><input type="text" value="🗾" readonly> <span>Map of Japan</span></div>
        <div><input type="text" value="🏔" readonly> <span>Snow-Capped Mountain</span></div>
        <div><input type="text" value="⛰" readonly> <span>Mountain</span></div>
        <div><input type="text" value="🌋" readonly> <span>Volcano</span></div>
        <div><input type="text" value="🗻" readonly> <span>Mount Fuji</span></div>
        <div><input type="text" value="🏕" readonly> <span>Camping</span></div>
        <div><input type="text" value="🏜" readonly> <span>Desert</span></div>
        <div><input type="text" value="🏝" readonly> <span>Desert Island</span></div>
        <div><input type="text" value="🏞" readonly> <span>National Park</span></div>
        <div><input type="text" value="🏟" readonly> <span>Stadium</span></div>
        <div><input type="text" value="🏛" readonly> <span>Classical Building</span></div>
        <div><input type="text" value="🏗" readonly> <span>Building Construction</span></div>
        <div><input type="text" value="🏘" readonly> <span>Houses</span></div>
        <div><input type="text" value="🏚" readonly> <span>Derelict House</span></div>
        <div><input type="text" value="🏠" readonly> <span>House</span></div>
        <div><input type="text" value="🏡" readonly> <span>House With Garden</span></div>
        <div><input type="text" value="🏢" readonly> <span>Office Building</span></div>
        <div><input type="text" value="🏣" readonly> <span>Japanese Post Office</span></div>
        <div><input type="text" value="🏤" readonly> <span>Post Office</span></div>
        <div><input type="text" value="🏥" readonly> <span>Hospital</span></div>
        <div><input type="text" value="🏦" readonly> <span>Bank</span></div>
        <div><input type="text" value="🏨" readonly> <span>Hotel</span></div>
        <div><input type="text" value="🏩" readonly> <span>Love Hotel</span></div>
        <div><input type="text" value="🏪" readonly> <span>Convenience Store</span></div>
        <div><input type="text" value="🏫" readonly> <span>School</span></div>
        <div><input type="text" value="🏬" readonly> <span>Department Store</span></div>
        <div><input type="text" value="🏭" readonly> <span>Factory</span></div>
        <div><input type="text" value="🏯" readonly> <span>Japanese Castle</span></div>
        <div><input type="text" value="🏰" readonly> <span>Castle</span></div>
        <div><input type="text" value="💒" readonly> <span>Wedding</span></div>
        <div><input type="text" value="🗼" readonly> <span>Tokyo Tower</span></div>
        <div><input type="text" value="🗽" readonly> <span>Statue of Liberty</span></div>
        <div><input type="text" value="⛪" readonly> <span>Church</span></div>
        <div><input type="text" value="🕌" readonly> <span>Mosque</span></div>
        <div><input type="text" value="🕍" readonly> <span>Synagogue</span></div>
        <div><input type="text" value="⛩" readonly> <span>Shinto Shrine</span></div>
        <div><input type="text" value="🕋" readonly> <span>Kaaba</span></div>
        <div><input type="text" value="⛲" readonly> <span>Fountain</span></div>
        <div><input type="text" value="⛺" readonly> <span>Tent</span></div>
        <div><input type="text" value="🌁" readonly> <span>Foggy</span></div>
        <div><input type="text" value="🌃" readonly> <span>Night With Stars</span></div>
        <div><input type="text" value="🏙" readonly> <span>Cityscape</span></div>
        <div><input type="text" value="🌄" readonly> <span>Sunrise Over Mountains</span></div>
        <div><input type="text" value="🌅" readonly> <span>Sunrise</span></div>
        <div><input type="text" value="🌆" readonly> <span>Cityscape at Dusk</span></div>
        <div><input type="text" value="🌇" readonly> <span>Sunset</span></div>
        <div><input type="text" value="🌉" readonly> <span>Bridge at Night</span></div>
        <div><input type="text" value="🌌" readonly> <span>Milky Way</span></div>
        <div><input type="text" value="🎠" readonly> <span>Carousel Horse</span></div>
        <div><input type="text" value="🎡" readonly> <span>Ferris Wheel</span></div>
        <div><input type="text" value="🎢" readonly> <span>Roller Coaster</span></div>
        <div><input type="text" value="🚂" readonly> <span>Locomotive</span></div>
        <div><input type="text" value="🚃" readonly> <span>Railway Car</span></div>
        <div><input type="text" value="🚄" readonly> <span>High-Speed Train</span></div>
        <div><input type="text" value="🚅" readonly> <span>Bullet Train</span></div>
        <div><input type="text" value="🚆" readonly> <span>Train</span></div>
        <div><input type="text" value="🚇" readonly> <span>Metro</span></div>
        <div><input type="text" value="🚈" readonly> <span>Light Rail</span></div>
        <div><input type="text" value="🚉" readonly> <span>Station</span></div>
        <div><input type="text" value="🚊" readonly> <span>Tram</span></div>
        <div><input type="text" value="🚝" readonly> <span>Monorail</span></div>
        <div><input type="text" value="🚞" readonly> <span>Mountain Railway</span></div>
        <div><input type="text" value="🚋" readonly> <span>Tram Car</span></div>
        <div><input type="text" value="🚌" readonly> <span>Bus</span></div>
        <div><input type="text" value="🚍" readonly> <span>Oncoming Bus</span></div>
        <div><input type="text" value="🚎" readonly> <span>Trolleybus</span></div>
        <div><input type="text" value="🚐" readonly> <span>Minibus</span></div>
        <div><input type="text" value="🚑" readonly> <span>Ambulance</span></div>
        <div><input type="text" value="🚒" readonly> <span>Fire Engine</span></div>
        <div><input type="text" value="🚓" readonly> <span>Police Car</span></div>
        <div><input type="text" value="🚔" readonly> <span>Oncoming Police Car</span></div>
        <div><input type="text" value="🚕" readonly> <span>Taxi</span></div>
        <div><input type="text" value="🚖" readonly> <span>Oncoming Taxi</span></div>
        <div><input type="text" value="🚗" readonly> <span>Automobile</span></div>
        <div><input type="text" value="🚘" readonly> <span>Oncoming Automobile</span></div>
        <div><input type="text" value="🚚" readonly> <span>Delivery Truck</span></div>
        <div><input type="text" value="🚛" readonly> <span>Articulated Lorry</span></div>
        <div><input type="text" value="🚜" readonly> <span>Tractor</span></div>
        <div><input type="text" value="🚲" readonly> <span>Bicycle</span></div>
        <div><input type="text" value="🛴" readonly> <span>Kick Scooter</span></div>
        <div><input type="text" value="🛵" readonly> <span>Motor Scooter</span></div>
        <div><input type="text" value="🚏" readonly> <span>Bus Stop</span></div>
        <div><input type="text" value="🛤" readonly> <span>Railway Track</span></div>
        <div><input type="text" value="⛽" readonly> <span>Fuel Pump</span></div>
        <div><input type="text" value="🚨" readonly> <span>Police Car Light</span></div>
        <div><input type="text" value="⛵" readonly> <span>Sailboat</span></div>
        <div><input type="text" value="🚤" readonly> <span>Speedboat</span></div>
        <div><input type="text" value="🛳" readonly> <span>Passenger Ship</span></div>
        <div><input type="text" value="⛴" readonly> <span>Ferry</span></div>
        <div><input type="text" value="🛥" readonly> <span>Motor Boat</span></div>
        <div><input type="text" value="🚢" readonly> <span>Ship</span></div>
        <div><input type="text" value="✈" readonly> <span>Airplane</span></div>
        <div><input type="text" value="🛩" readonly> <span>Small Airplane</span></div>
        <div><input type="text" value="🛫" readonly> <span>Airplane Departure</span></div>
        <div><input type="text" value="🛬" readonly> <span>Airplane Arrival</span></div>
        <div><input type="text" value="💺" readonly> <span>Seat</span></div>
        <div><input type="text" value="🚁" readonly> <span>Helicopter</span></div>
        <div><input type="text" value="🚟" readonly> <span>Suspension Railway</span></div>
        <div><input type="text" value="🚠" readonly> <span>Mountain Cableway</span></div>
        <div><input type="text" value="🚡" readonly> <span>Aerial Tramway</span></div>
        <div><input type="text" value="🛰" readonly> <span>Satellite</span></div>
        <div><input type="text" value="🚀" readonly> <span>Rocket</span></div>
        <div><input type="text" value="🛸" readonly> <span>Flying Saucer</span></div>
        <div><input type="text" value="⛱" readonly> <span>Umbrella on the Beach</span></div>
        <div><input type="text" value="🎆" readonly> <span>Fireworks</span></div>
        <div><input type="text" value="🎇" readonly> <span>Sparkler</span></div>
        <div><input type="text" value="🎑" readonly> <span>Moon Viewing Ceremony</span></div>
        <div><input type="text" value="🗿" readonly> <span>Moai Statue</span></div>
        <div><input type="text" value="🛂" readonly> <span>Passport Control</span></div>
        <div><input type="text" value="🛃" readonly> <span>Customs</span></div>
        <div><input type="text" value="🛄" readonly> <span>Baggage Claim</span></div>
        <div><input type="text" value="🛅" readonly> <span>Left Luggage</span></div>
    </div>
    <div class="emocat" data-name="Objects">
        <div><input type="text" value="💎" readonly> <span>Gem Stone</span></div>
        <div><input type="text" value="👓" readonly> <span>Glasses</span></div>
        <div><input type="text" value="🕶" readonly> <span>Sunglasses</span></div>
        <div><input type="text" value="👔" readonly> <span>Necktie</span></div>
        <div><input type="text" value="👕" readonly> <span>T-Shirt</span></div>
        <div><input type="text" value="👖" readonly> <span>Jeans</span></div>
        <div><input type="text" value="🧣" readonly> <span>Scarf</span></div>
        <div><input type="text" value="🧤" readonly> <span>Gloves</span></div>
        <div><input type="text" value="🧥" readonly> <span>Coat</span></div>
        <div><input type="text" value="🧦" readonly> <span>Socks</span></div>
        <div><input type="text" value="👗" readonly> <span>Dress</span></div>
        <div><input type="text" value="👘" readonly> <span>Kimono</span></div>
        <div><input type="text" value="👙" readonly> <span>Bikini</span></div>
        <div><input type="text" value="👚" readonly> <span>Woman&rsquo;s Clothes</span></div>
        <div><input type="text" value="👛" readonly> <span>Purse</span></div>
        <div><input type="text" value="👜" readonly> <span>Handbag</span></div>
        <div><input type="text" value="👝" readonly> <span>Clutch Bag</span></div>
        <div><input type="text" value="🎒" readonly> <span>School Backpack</span></div>
        <div><input type="text" value="👞" readonly> <span>Man&rsquo;s Shoe</span></div>
        <div><input type="text" value="👟" readonly> <span>Running Shoe</span></div>
        <div><input type="text" value="👠" readonly> <span>High-Heeled Shoe</span></div>
        <div><input type="text" value="👡" readonly> <span>Woman&rsquo;s Sandal</span></div>
        <div><input type="text" value="👢" readonly> <span>Woman&rsquo;s Boot</span></div>
        <div><input type="text" value="👑" readonly> <span>Crown</span></div>
        <div><input type="text" value="👒" readonly> <span>Woman&rsquo;s Hat</span></div>
        <div><input type="text" value="🎩" readonly> <span>Top Hat</span></div>
        <div><input type="text" value="🎓" readonly> <span>Graduation Cap</span></div>
        <div><input type="text" value="🧢" readonly> <span>Billed Cap</span></div>
        <div><input type="text" value="⛑" readonly> <span>Rescue Worker&rsquo;s Helmet</span></div>
        <div><input type="text" value="💄" readonly> <span>Lipstick</span></div>
        <div><input type="text" value="💍" readonly> <span>Ring</span></div>
        <div><input type="text" value="🌂" readonly> <span>Closed Umbrella</span></div>
        <div><input type="text" value="☂" readonly> <span>Umbrella</span></div>
        <div><input type="text" value="💼" readonly> <span>Briefcase</span></div>
        <div><input type="text" value="☠" readonly> <span>Skull and Crossbones</span></div>
        <div><input type="text" value="🛀" readonly> <span>Person Taking Bath</span></div>
        <div><input type="text" value="🛌" readonly> <span>Person in Bed</span></div>
        <div><input type="text" value="💌" readonly> <span>Love Letter</span></div>
        <div><input type="text" value="💣" readonly> <span>Bomb</span></div>
        <div><input type="text" value="🚥" readonly> <span>Horizontal Traffic Light</span></div>
        <div><input type="text" value="🚦" readonly> <span>Vertical Traffic Light</span></div>
        <div><input type="text" value="🚧" readonly> <span>Construction</span></div>
        <div><input type="text" value="⚓" readonly> <span>Anchor</span></div>
        <div><input type="text" value="🕳" readonly> <span>Hole</span></div>
        <div><input type="text" value="🛍" readonly> <span>Shopping Bags</span></div>
        <div><input type="text" value="📿" readonly> <span>Prayer Beads</span></div>
        <div><input type="text" value="🔪" readonly> <span>Kitchen Knife</span></div>
        <div><input type="text" value="🏺" readonly> <span>Amphora</span></div>
        <div><input type="text" value="🗺" readonly> <span>World Map</span></div>
        <div><input type="text" value="💈" readonly> <span>Barber Pole</span></div>
        <div><input type="text" value="🛢" readonly> <span>Oil Drum</span></div>
        <div><input type="text" value="🛎" readonly> <span>Bellhop Bell</span></div>
        <div><input type="text" value="⌛" readonly> <span>Hourglass Done</span></div>
        <div><input type="text" value="⏳" readonly> <span>Hourglass Not Done</span></div>
        <div><input type="text" value="⌚" readonly> <span>Watch</span></div>
        <div><input type="text" value="⏰" readonly> <span>Alarm Clock</span></div>
        <div><input type="text" value="⏱" readonly> <span>Stopwatch</span></div>
        <div><input type="text" value="⏲" readonly> <span>Timer Clock</span></div>
        <div><input type="text" value="🕰" readonly> <span>Mantelpiece Clock</span></div>
        <div><input type="text" value="🌡" readonly> <span>Thermometer</span></div>
        <div><input type="text" value="🎈" readonly> <span>Balloon</span></div>
        <div><input type="text" value="🎉" readonly> <span>Party Popper</span></div>
        <div><input type="text" value="🎊" readonly> <span>Confetti Ball</span></div>
        <div><input type="text" value="🎎" readonly> <span>Japanese Dolls</span></div>
        <div><input type="text" value="🎏" readonly> <span>Carp Streamer</span></div>
        <div><input type="text" value="🎐" readonly> <span>Wind Chime</span></div>
        <div><input type="text" value="🎀" readonly> <span>Ribbon</span></div>
        <div><input type="text" value="🎁" readonly> <span>Wrapped Gift</span></div>
        <div><input type="text" value="🔮" readonly> <span>Crystal Ball</span></div>
        <div><input type="text" value="🕹" readonly> <span>Joystick</span></div>
        <div><input type="text" value="🖼" readonly> <span>Framed Picture</span></div>
        <div><input type="text" value="🎙" readonly> <span>Studio Microphone</span></div>
        <div><input type="text" value="🎚" readonly> <span>Level Slider</span></div>
        <div><input type="text" value="🎛" readonly> <span>Control Knobs</span></div>
        <div><input type="text" value="📻" readonly> <span>Radio</span></div>
        <div><input type="text" value="📱" readonly> <span>Mobile Phone</span></div>
        <div><input type="text" value="📲" readonly> <span>Mobile Phone With Arrow</span></div>
        <div><input type="text" value="☎" readonly> <span>Telephone</span></div>
        <div><input type="text" value="📞" readonly> <span>Telephone Receiver</span></div>
        <div><input type="text" value="📟" readonly> <span>Pager</span></div>
        <div><input type="text" value="📠" readonly> <span>Fax Machine</span></div>
        <div><input type="text" value="🔋" readonly> <span>Battery</span></div>
        <div><input type="text" value="🔌" readonly> <span>Electric Plug</span></div>
        <div><input type="text" value="💻" readonly> <span>Laptop Computer</span></div>
        <div><input type="text" value="🖥" readonly> <span>Desktop Computer</span></div>
        <div><input type="text" value="🖨" readonly> <span>Printer</span></div>
        <div><input type="text" value="⌨" readonly> <span>Keyboard</span></div>
        <div><input type="text" value="🖱" readonly> <span>Computer Mouse</span></div>
        <div><input type="text" value="🖲" readonly> <span>Trackball</span></div>
        <div><input type="text" value="💽" readonly> <span>Computer Disk</span></div>
        <div><input type="text" value="💾" readonly> <span>Floppy Disk</span></div>
        <div><input type="text" value="💿" readonly> <span>Optical Disk</span></div>
        <div><input type="text" value="📀" readonly> <span>DVD</span></div>
        <div><input type="text" value="🎥" readonly> <span>Movie Camera</span></div>
        <div><input type="text" value="🎞" readonly> <span>Film Frames</span></div>
        <div><input type="text" value="📽" readonly> <span>Film Projector</span></div>
        <div><input type="text" value="📺" readonly> <span>Television</span></div>
        <div><input type="text" value="📷" readonly> <span>Camera</span></div>
        <div><input type="text" value="📸" readonly> <span>Camera With Flash</span></div>
        <div><input type="text" value="📹" readonly> <span>Video Camera</span></div>
        <div><input type="text" value="📼" readonly> <span>Videocassette</span></div>
        <div><input type="text" value="🔍" readonly> <span>Magnifying Glass Tilted Left</span></div>
        <div><input type="text" value="🔎" readonly> <span>Magnifying Glass Tilted Right</span></div>
        <div><input type="text" value="🕯" readonly> <span>Candle</span></div>
        <div><input type="text" value="💡" readonly> <span>Light Bulb</span></div>
        <div><input type="text" value="🔦" readonly> <span>Flashlight</span></div>
        <div><input type="text" value="🏮" readonly> <span>Red Paper Lantern</span></div>
        <div><input type="text" value="📔" readonly> <span>Notebook With Decorative Cover</span></div>
        <div><input type="text" value="📕" readonly> <span>Closed Book</span></div>
        <div><input type="text" value="📖" readonly> <span>Open Book</span></div>
        <div><input type="text" value="📗" readonly> <span>Green Book</span></div>
        <div><input type="text" value="📘" readonly> <span>Blue Book</span></div>
        <div><input type="text" value="📙" readonly> <span>Orange Book</span></div>
        <div><input type="text" value="📚" readonly> <span>Books</span></div>
        <div><input type="text" value="📓" readonly> <span>Notebook</span></div>
        <div><input type="text" value="📃" readonly> <span>Page With Curl</span></div>
        <div><input type="text" value="📜" readonly> <span>Scroll</span></div>
        <div><input type="text" value="📄" readonly> <span>Page Facing Up</span></div>
        <div><input type="text" value="📰" readonly> <span>Newspaper</span></div>
        <div><input type="text" value="🗞" readonly> <span>Rolled-Up Newspaper</span></div>
        <div><input type="text" value="📑" readonly> <span>Bookmark Tabs</span></div>
        <div><input type="text" value="🔖" readonly> <span>Bookmark</span></div>
        <div><input type="text" value="🏷" readonly> <span>Label</span></div>
        <div><input type="text" value="💰" readonly> <span>Money Bag</span></div>
        <div><input type="text" value="💸" readonly> <span>Money With Wings</span></div>
        <div><input type="text" value="💳" readonly> <span>Credit Card</span></div>
        <div><input type="text" value="✉" readonly> <span>Envelope</span></div>
        <div><input type="text" value="📧" readonly> <span>E-Mail</span></div>
        <div><input type="text" value="📨" readonly> <span>Incoming Envelope</span></div>
        <div><input type="text" value="📩" readonly> <span>Envelope With Arrow</span></div>
        <div><input type="text" value="📤" readonly> <span>Outbox Tray</span></div>
        <div><input type="text" value="📥" readonly> <span>Inbox Tray</span></div>
        <div><input type="text" value="📦" readonly> <span>Package</span></div>
        <div><input type="text" value="📫" readonly> <span>Closed Mailbox With Raised Flag</span></div>
        <div><input type="text" value="📪" readonly> <span>Closed Mailbox With Lowered Flag</span></div>
        <div><input type="text" value="📬" readonly> <span>Open Mailbox With Raised Flag</span></div>
        <div><input type="text" value="📭" readonly> <span>Open Mailbox With Lowered Flag</span></div>
        <div><input type="text" value="📮" readonly> <span>Postbox</span></div>
        <div><input type="text" value="🗳" readonly> <span>Ballot Box With Ballot</span></div>
        <div><input type="text" value="✏" readonly> <span>Pencil</span></div>
        <div><input type="text" value="✎" readonly> <span>Pencil</span></div>
        <div><input type="text" value="🖉" readonly> <span>Pencil</span></div>
        <div><input type="text" value="✒" readonly> <span>Black Nib</span></div>
        <div><input type="text" value="🖋" readonly> <span>Fountain Pen</span></div>
        <div><input type="text" value="🖊" readonly> <span>Pen</span></div>
        <div><input type="text" value="🖌" readonly> <span>Paintbrush</span></div>
        <div><input type="text" value="🖍" readonly> <span>Crayon</span></div>
        <div><input type="text" value="📝" readonly> <span>Memo</span></div>
        <div><input type="text" value="📁" readonly> <span>File Folder</span></div>
        <div><input type="text" value="📂" readonly> <span>Open File Folder</span></div>
        <div><input type="text" value="🗂" readonly> <span>Card Index Dividers</span></div>
        <div><input type="text" value="📅" readonly> <span>Calendar</span></div>
        <div><input type="text" value="📆" readonly> <span>Tear-Off Calendar</span></div>
        <div><input type="text" value="🗒" readonly> <span>Spiral Notepad</span></div>
        <div><input type="text" value="🗓" readonly> <span>Spiral Calendar</span></div>
        <div><input type="text" value="📇" readonly> <span>Card Index</span></div>
        <div><input type="text" value="📈" readonly> <span>Chart Increasing</span></div>
        <div><input type="text" value="📉" readonly> <span>Chart Decreasing</span></div>
        <div><input type="text" value="📊" readonly> <span>Bar Chart</span></div>
        <div><input type="text" value="📋" readonly> <span>Clipboard</span></div>
        <div><input type="text" value="📌" readonly> <span>Pushpin</span></div>
        <div><input type="text" value="📍" readonly> <span>Round Pushpin</span></div>
        <div><input type="text" value="📎" readonly> <span>Paperclip</span></div>
        <div><input type="text" value="🖇" readonly> <span>Linked Paperclips</span></div>
        <div><input type="text" value="📏" readonly> <span>Straight Ruler</span></div>
        <div><input type="text" value="📐" readonly> <span>Triangular Ruler</span></div>
        <div><input type="text" value="✂" readonly> <span>Scissors</span></div>
        <div><input type="text" value="🗃" readonly> <span>Card File Box</span></div>
        <div><input type="text" value="🗄" readonly> <span>File Cabinet</span></div>
        <div><input type="text" value="🗑" readonly> <span>Wastebasket</span></div>
        <div><input type="text" value="🔒" readonly> <span>Locked</span></div>
        <div><input type="text" value="🔓" readonly> <span>Unlocked</span></div>
        <div><input type="text" value="🔏" readonly> <span>Locked With Pen</span></div>
        <div><input type="text" value="🔐" readonly> <span>Locked With Key</span></div>
        <div><input type="text" value="🔑" readonly> <span>Key</span></div>
        <div><input type="text" value="🗝" readonly> <span>Old Key</span></div>
        <div><input type="text" value="🔨" readonly> <span>Hammer</span></div>
        <div><input type="text" value="⛏" readonly> <span>Pick</span></div>
        <div><input type="text" value="⚒" readonly> <span>Hammer and Pick</span></div>
        <div><input type="text" value="🛠" readonly> <span>Hammer and Wrench</span></div>
        <div><input type="text" value="🗡" readonly> <span>Dagger</span></div>
        <div><input type="text" value="⚔" readonly> <span>Crossed Swords</span></div>
        <div><input type="text" value="🔫" readonly> <span>Pistol</span></div>
        <div><input type="text" value="🛡" readonly> <span>Shield</span></div>
        <div><input type="text" value="🔧" readonly> <span>Wrench</span></div>
        <div><input type="text" value="🔩" readonly> <span>Nut and Bolt</span></div>
        <div><input type="text" value="⚙" readonly> <span>Gear</span></div>
        <div><input type="text" value="🗜" readonly> <span>Clamp</span></div>
        <div><input type="text" value="⚖" readonly> <span>Balance Scale</span></div>
        <div><input type="text" value="🔗" readonly> <span>Link</span></div>
        <div><input type="text" value="⛓" readonly> <span>Chains</span></div>
        <div><input type="text" value="⚗" readonly> <span>Alembic</span></div>
        <div><input type="text" value="🔬" readonly> <span>Microscope</span></div>
        <div><input type="text" value="🔭" readonly> <span>Telescope</span></div>
        <div><input type="text" value="📡" readonly> <span>Satellite Antenna</span></div>
        <div><input type="text" value="💉" readonly> <span>Syringe</span></div>
        <div><input type="text" value="💊" readonly> <span>Pill</span></div>
        <div><input type="text" value="🚪" readonly> <span>Door</span></div>
        <div><input type="text" value="🛏" readonly> <span>Bed</span></div>
        <div><input type="text" value="🛋" readonly> <span>Couch and Lamp</span></div>
        <div><input type="text" value="🚽" readonly> <span>Toilet</span></div>
        <div><input type="text" value="🚿" readonly> <span>Shower</span></div>
        <div><input type="text" value="🛁" readonly> <span>Bathtub</span></div>
        <div><input type="text" value="🚬" readonly> <span>Cigarette</span></div>
        <div><input type="text" value="⚰" readonly> <span>Coffin</span></div>
        <div><input type="text" value="⚱" readonly> <span>Funeral Urn</span></div>
        <div><input type="text" value="💘" readonly> <span>Heart With Arrow</span></div>
        <div><input type="text" value="❤" readonly> <span>Red Heart</span></div>
        <div><input type="text" value="💓" readonly> <span>Beating Heart</span></div>
        <div><input type="text" value="💔" readonly> <span>Broken Heart</span></div>
        <div><input type="text" value="💕" readonly> <span>Two Hearts</span></div>
        <div><input type="text" value="💖" readonly> <span>Sparkling Heart</span></div>
        <div><input type="text" value="💗" readonly> <span>Growing Heart</span></div>
        <div><input type="text" value="💙" readonly> <span>Blue Heart</span></div>
        <div><input type="text" value="💚" readonly> <span>Green Heart</span></div>
        <div><input type="text" value="💛" readonly> <span>Yellow Heart</span></div>
        <div><input type="text" value="🧡" readonly> <span>Orange Heart</span></div>
        <div><input type="text" value="💜" readonly> <span>Purple Heart</span></div>
        <div><input type="text" value="🖤" readonly> <span>Black Heart</span></div>
        <div><input type="text" value="💝" readonly> <span>Heart With Ribbon</span></div>
        <div><input type="text" value="💞" readonly> <span>Revolving Hearts</span></div>
        <div><input type="text" value="💟" readonly> <span>Heart Decoration</span></div>
        <div><input type="text" value="❣" readonly> <span>Heavy Heart Exclamation</span></div>
        <div><input type="text" value="💦" readonly> <span>Sweat Droplets</span></div>
        <div><input type="text" value="💨" readonly> <span>Dashing Away</span></div>
        <div><input type="text" value="💫" readonly> <span>Dizzy Stars</span></div>
        <div><input type="text" value="🏁" readonly> <span>Chequered Flag</span></div>
        <div><input type="text" value="🚩" readonly> <span>Triangular Flag</span></div>
        <div><input type="text" value="🎌" readonly> <span>Crossed Flags</span></div>
        <div><input type="text" value="🏴" readonly> <span>Black Flag</span></div>
        <div><input type="text" value="🏳" readonly> <span>White Flag</span></div>
        <div><input type="text" value="🏳️&zwj;🌈" readonly> <span>Rainbow Flag</span></div>
        <div><input type="text" value="🏴&zwj;☠️" readonly> <span>Pirate Flag</span></div>
    </div>
    <div class="emocat" data-name="Symbols">
        <div><input type="text" value="👍" readonly> <span>Thumbs Up</span></div>
        <div><input type="text" value="👎" readonly> <span>Thumbs Down</span></div>
        <div><input type="text" value="💪" readonly> <span>Flexed Biceps</span></div>
        <div><input type="text" value="🤳" readonly> <span>Selfie</span></div>
        <div><input type="text" value="👈" readonly> <span>Backhand Index Pointing Left</span></div>
        <div><input type="text" value="👉" readonly> <span>Backhand Index Pointing Right</span></div>
        <div><input type="text" value="☝" readonly> <span>Index Pointing Up</span></div>
        <div><input type="text" value="👆" readonly> <span>Backhand Index Pointing Up</span></div>
        <div><input type="text" value="🖕" readonly> <span>Middle Finger</span></div>
        <div><input type="text" value="👇" readonly> <span>Backhand Index Pointing Down</span></div>
        <div><input type="text" value="✌" readonly> <span>Victory Hand</span></div>
        <div><input type="text" value="🤞" readonly> <span>Crossed Fingers</span></div>
        <div><input type="text" value="🖖" readonly> <span>Vulcan Salute</span></div>
        <div><input type="text" value="🤘" readonly> <span>Sign of the Horns</span></div>
        <div><input type="text" value="🖐" readonly> <span>Hand With Fingers Splayed</span></div>
        <div><input type="text" value="✋" readonly> <span>Raised Hand</span></div>
        <div><input type="text" value="👌" readonly> <span>OK Hand</span></div>
        <div><input type="text" value="✊" readonly> <span>Raised Fist</span></div>
        <div><input type="text" value="👊" readonly> <span>Oncoming Fist</span></div>
        <div><input type="text" value="🤛" readonly> <span>Left-Facing Fist</span></div>
        <div><input type="text" value="🤜" readonly> <span>Right-Facing Fist</span></div>
        <div><input type="text" value="🤚" readonly> <span>Raised Back of Hand</span></div>
        <div><input type="text" value="👋" readonly> <span>Waving Hand</span></div>
        <div><input type="text" value="🤟" readonly> <span>Love-You Gesture</span></div>
        <div><input type="text" value="✍" readonly> <span>Writing Hand</span></div>
        <div><input type="text" value="👏" readonly> <span>Clapping Hands</span></div>
        <div><input type="text" value="👐" readonly> <span>Open Hands</span></div>
        <div><input type="text" value="🙌" readonly> <span>Raising Hands</span></div>
        <div><input type="text" value="🤲" readonly> <span>Palms Up Together</span></div>
        <div><input type="text" value="🙏" readonly> <span>Folded Hands</span></div>
        <div><input type="text" value="🤝" readonly> <span>Handshake</span></div>
        <div><input type="text" value="💅" readonly> <span>Nail Polish</span></div>
        <div><input type="text" value="👂" readonly> <span>Ear</span></div>
        <div><input type="text" value="👃" readonly> <span>Nose</span></div>
        <div><input type="text" value="⚕️" readonly> <span>Aesculapius: pharmacy, medical</span></div>
        <div><input type="text" value="👣" readonly> <span>Footprints</span></div>
        <div><input type="text" value="👀" readonly> <span>Eyes</span></div>
        <div><input type="text" value="👁" readonly> <span>Eye</span></div>
        <div><input type="text" value="🧠" readonly> <span>Brain</span></div>
        <div><input type="text" value="👅" readonly> <span>Tongue</span></div>
        <div><input type="text" value="👄" readonly> <span>Mouth</span></div>
        <div><input type="text" value="💋" readonly> <span>Kiss Mark</span></div>
        <div><input type="text" value="👁️&zwj;🗨️" readonly> <span>Eye in Speech Bubble</span></div>
        <div><input type="text" value="💤" readonly> <span>Zzz</span></div>
        <div><input type="text" value="💢" readonly> <span>Anger Symbol</span></div>
        <div><input type="text" value="💬" readonly> <span>Speech Balloon</span></div>
        <div><input type="text" value="🗯" readonly> <span>Right Anger Bubble</span></div>
        <div><input type="text" value="💭" readonly> <span>Thought Balloon</span></div>
        <div><input type="text" value="♨" readonly> <span>Hot Springs</span></div>
        <div><input type="text" value="🛑" readonly> <span>Stop Sign</span></div>
        <div><input type="text" value="🕛" readonly> <span>Twelve O&rsquo;clock</span></div>
        <div><input type="text" value="🕧" readonly> <span>Twelve-Thirty</span></div>
        <div><input type="text" value="🕐" readonly> <span>One O&rsquo;clock</span></div>
        <div><input type="text" value="🕜" readonly> <span>One-Thirty</span></div>
        <div><input type="text" value="🕑" readonly> <span>Two O&rsquo;clock</span></div>
        <div><input type="text" value="🕝" readonly> <span>Two-Thirty</span></div>
        <div><input type="text" value="🕒" readonly> <span>Three O&rsquo;clock</span></div>
        <div><input type="text" value="🕞" readonly> <span>Three-Thirty</span></div>
        <div><input type="text" value="🕓" readonly> <span>Four O&rsquo;clock</span></div>
        <div><input type="text" value="🕟" readonly> <span>Four-Thirty</span></div>
        <div><input type="text" value="🕔" readonly> <span>Five O&rsquo;clock</span></div>
        <div><input type="text" value="🕠" readonly> <span>Five-Thirty</span></div>
        <div><input type="text" value="🕕" readonly> <span>Six O&rsquo;clock</span></div>
        <div><input type="text" value="🕡" readonly> <span>Six-Thirty</span></div>
        <div><input type="text" value="🕖" readonly> <span>Seven O&rsquo;clock</span></div>
        <div><input type="text" value="🕢" readonly> <span>Seven-Thirty</span></div>
        <div><input type="text" value="🕗" readonly> <span>Eight O&rsquo;clock</span></div>
        <div><input type="text" value="🕣" readonly> <span>Eight-Thirty</span></div>
        <div><input type="text" value="🕘" readonly> <span>Nine O&rsquo;clock</span></div>
        <div><input type="text" value="🕤" readonly> <span>Nine-Thirty</span></div>
        <div><input type="text" value="🕙" readonly> <span>Ten O&rsquo;clock</span></div>
        <div><input type="text" value="🕥" readonly> <span>Ten-Thirty</span></div>
        <div><input type="text" value="🕚" readonly> <span>Eleven O&rsquo;clock</span></div>
        <div><input type="text" value="🕦" readonly> <span>Eleven-Thirty</span></div>
        <div><input type="text" value="🌀" readonly> <span>Cyclone</span></div>
        <div><input type="text" value="🃏" readonly> <span>Joker</span></div>
        <div><input type="text" value="🀄" readonly> <span>Mahjong Red Dragon</span></div>
        <div><input type="text" value="🎴" readonly> <span>Flower Playing Cards</span></div>
        <div><input type="text" value="🔇" readonly> <span>Muted Speaker</span></div>
        <div><input type="text" value="🔈" readonly> <span>Speaker Low Volume</span></div>
        <div><input type="text" value="🔉" readonly> <span>Speaker Medium Volume</span></div>
        <div><input type="text" value="🔊" readonly> <span>Speaker High Volume</span></div>
        <div><input type="text" value="📢" readonly> <span>Loudspeaker</span></div>
        <div><input type="text" value="📣" readonly> <span>Megaphone</span></div>
        <div><input type="text" value="📯" readonly> <span>Postal Horn</span></div>
        <div><input type="text" value="🔔" readonly> <span>Bell</span></div>
        <div><input type="text" value="🔕" readonly> <span>Bell With Slash</span></div>
        <div><input type="text" value="🎵" readonly> <span>Musical Note</span></div>
        <div><input type="text" value="🎶" readonly> <span>Musical Notes</span></div>
        <div><input type="text" value="🏧" readonly> <span>Atm Sign</span></div>
        <div><input type="text" value="🚮" readonly> <span>Litter in Bin Sign</span></div>
        <div><input type="text" value="🚰" readonly> <span>Potable Water</span></div>
        <div><input type="text" value="♿" readonly> <span>Wheelchair Symbol</span></div>
        <div><input type="text" value="🚹" readonly> <span>Men&rsquo;s Room</span></div>
        <div><input type="text" value="🚺" readonly> <span>Women&rsquo;s Room</span></div>
        <div><input type="text" value="🚻" readonly> <span>Restroom</span></div>
        <div><input type="text" value="🚼" readonly> <span>Baby Symbol</span></div>
        <div><input type="text" value="🚾" readonly> <span>Water Closet</span></div>
        <div><input type="text" value="⚠" readonly> <span>Warning</span></div>
        <div><input type="text" value="🚸" readonly> <span>Children Crossing</span></div>
        <div><input type="text" value="⛔" readonly> <span>No Entry</span></div>
        <div><input type="text" value="🚫" readonly> <span>Prohibited</span></div>
        <div><input type="text" value="🚳" readonly> <span>No Bicycles</span></div>
        <div><input type="text" value="🚭" readonly> <span>No Smoking</span></div>
        <div><input type="text" value="🚯" readonly> <span>No Littering</span></div>
        <div><input type="text" value="🚱" readonly> <span>Non-Potable Water</span></div>
        <div><input type="text" value="🚷" readonly> <span>No Pedestrians</span></div>
        <div><input type="text" value="🔞" readonly> <span>No One Under Eighteen</span></div>
        <div><input type="text" value="☢" readonly> <span>Radioactive</span></div>
        <div><input type="text" value="☣" readonly> <span>Biohazard</span></div>
        <div><input type="text" value="🛐" readonly> <span>Place of Worship</span></div>
        <div><input type="text" value="⚛" readonly> <span>Atom Symbol</span></div>
        <div><input type="text" value="🕉" readonly> <span>Om</span></div>
        <div><input type="text" value="✡" readonly> <span>Star of David</span></div>
        <div><input type="text" value="☸" readonly> <span>Wheel of Dharma</span></div>
        <div><input type="text" value="☯" readonly> <span>Yin Yang</span></div>
        <div><input type="text" value="✝" readonly> <span>Latin Cross</span></div>
        <div><input type="text" value="☦" readonly> <span>Orthodox Cross</span></div>
        <div><input type="text" value="☪" readonly> <span>Star and Crescent</span></div>
        <div><input type="text" value="☮" readonly> <span>Peace Symbol</span></div>
        <div><input type="text" value="🕎" readonly> <span>Menorah</span></div>
        <div><input type="text" value="🔯" readonly> <span>Dotted Six-Pointed Star</span></div>
        <div><input type="text" value="♈" readonly> <span>Aries</span></div>
        <div><input type="text" value="♉" readonly> <span>Taurus</span></div>
        <div><input type="text" value="♊" readonly> <span>Gemini</span></div>
        <div><input type="text" value="♋" readonly> <span>Cancer</span></div>
        <div><input type="text" value="♌" readonly> <span>Leo</span></div>
        <div><input type="text" value="♍" readonly> <span>Virgo</span></div>
        <div><input type="text" value="♎" readonly> <span>Libra</span></div>
        <div><input type="text" value="♏" readonly> <span>Scorpio</span></div>
        <div><input type="text" value="♐" readonly> <span>Sagittarius</span></div>
        <div><input type="text" value="♑" readonly> <span>Capricorn</span></div>
        <div><input type="text" value="♒" readonly> <span>Aquarius</span></div>
        <div><input type="text" value="♓" readonly> <span>Pisces</span></div>
        <div><input type="text" value="⛎" readonly> <span>Ophiuchus</span></div>
        <div><input type="text" value="🔀" readonly> <span>Shuffle Tracks Button</span></div>
        <div><input type="text" value="🔁" readonly> <span>Repeat Button</span></div>
        <div><input type="text" value="🔂" readonly> <span>Repeat Single Button</span></div>
        <div><input type="text" value="▶" readonly> <span>Play Button</span></div>
        <div><input type="text" value="⏩" readonly> <span>Fast-Forward Button</span></div>
        <div><input type="text" value="◀" readonly> <span>Reverse Button</span></div>
        <div><input type="text" value="⏪" readonly> <span>Fast Reverse Button</span></div>
        <div><input type="text" value="🔼" readonly> <span>Upwards Button</span></div>
        <div><input type="text" value="⏫" readonly> <span>Fast Up Button</span></div>
        <div><input type="text" value="🔽" readonly> <span>Downwards Button</span></div>
        <div><input type="text" value="⏬" readonly> <span>Fast Down Button</span></div>
        <div><input type="text" value="⏹" readonly> <span>Stop Button</span></div>
        <div><input type="text" value="⏏" readonly> <span>Eject Button</span></div>
        <div><input type="text" value="🎦" readonly> <span>Cinema</span></div>
        <div><input type="text" value="🔅" readonly> <span>Dim Button</span></div>
        <div><input type="text" value="🔆" readonly> <span>Bright Button</span></div>
        <div><input type="text" value="📶" readonly> <span>Antenna Bars</span></div>
        <div><input type="text" value="📳" readonly> <span>Vibration Mode</span></div>
        <div><input type="text" value="📴" readonly> <span>Mobile Phone Off</span></div>
        <div><input type="text" value="♻" readonly> <span>Recycling Symbol</span></div>
        <div><input type="text" value="🔱" readonly> <span>Trident Emblem</span></div>
        <div><input type="text" value="📛" readonly> <span>Name Badge</span></div>
        <div><input type="text" value="🔰" readonly> <span>Japanese Symbol for Beginner</span></div>
        <div><input type="text" value="⭕" readonly> <span>Heavy Large Circle</span></div>
        <div><input type="text" value="✅" readonly> <span>White Heavy Check Mark</span></div>
        <div><input type="text" value="☑" readonly> <span>Ballot Box With Check</span></div>
        <div><input type="text" value="✔" readonly> <span>Heavy Check Mark</span></div>
        <div><input type="text" value="✖" readonly> <span>Heavy Multiplication X</span></div>
        <div><input type="text" value="❌" readonly> <span>Cross Mark</span></div>
        <div><input type="text" value="❎" readonly> <span>Cross Mark Button</span></div>
        <div><input type="text" value="➕" readonly> <span>Heavy Plus Sign</span></div>
        <div><input type="text" value="➖" readonly> <span>Heavy Minus Sign</span></div>
        <div><input type="text" value="➗" readonly> <span>Heavy Division Sign</span></div>
        <div><input type="text" value="➰" readonly> <span>Curly Loop</span></div>
        <div><input type="text" value="➿" readonly> <span>Double Curly Loop</span></div>
        <div><input type="text" value="〽" readonly> <span>Part Alternation Mark</span></div>
        <div><input type="text" value="✳" readonly> <span>Eight-Spoked Asterisk</span></div>
        <div><input type="text" value="✴" readonly> <span>Eight-Pointed Star</span></div>
        <div><input type="text" value="❇" readonly> <span>Sparkle</span></div>
        <div><input type="text" value="‼" readonly> <span>Double Exclamation Mark</span></div>
        <div><input type="text" value="⁉" readonly> <span>Exclamation Question Mark</span></div>
        <div><input type="text" value="❓" readonly> <span>Question Mark</span></div>
        <div><input type="text" value="❔" readonly> <span>White Question Mark</span></div>
        <div><input type="text" value="❕" readonly> <span>White Exclamation Mark</span></div>
        <div><input type="text" value="❗" readonly> <span>Exclamation Mark</span></div>
        <div><input type="text" value="#️⃣" readonly> <span>Keycap Number Sign</span></div>
        <div><input type="text" value="0️⃣" readonly> <span>Keycap Digit Zero</span></div>
        <div><input type="text" value="1️⃣" readonly> <span>Keycap Digit One</span></div>
        <div><input type="text" value="2️⃣" readonly> <span>Keycap Digit Two</span></div>
        <div><input type="text" value="3️⃣" readonly> <span>Keycap Digit Three</span></div>
        <div><input type="text" value="4️⃣" readonly> <span>Keycap Digit Four</span></div>
        <div><input type="text" value="5️⃣" readonly> <span>Keycap Digit Five</span></div>
        <div><input type="text" value="6️⃣" readonly> <span>Keycap Digit Six</span></div>
        <div><input type="text" value="7️⃣" readonly> <span>Keycap Digit Seven</span></div>
        <div><input type="text" value="8️⃣" readonly> <span>Keycap Digit Eight</span></div>
        <div><input type="text" value="9️⃣" readonly> <span>Keycap Digit Nine</span></div>
        <div><input type="text" value="🔟" readonly> <span>Keycap 10</span></div>
        <div><input type="text" value="💯" readonly> <span>Hundred Points</span></div>
        <div><input type="text" value="🔠" readonly> <span>Input Latin Uppercase</span></div>
        <div><input type="text" value="🔡" readonly> <span>Input Latin Lowercase</span></div>
        <div><input type="text" value="🔢" readonly> <span>Input Numbers</span></div>
        <div><input type="text" value="🔣" readonly> <span>Input Symbols</span></div>
        <div><input type="text" value="🔤" readonly> <span>Input Latin Letters</span></div>
        <div><input type="text" value="🅰" readonly> <span>A Button (blood Type)</span></div>
        <div><input type="text" value="🆎" readonly> <span>Ab Button (blood Type)</span></div>
        <div><input type="text" value="🅱" readonly> <span>B Button (blood Type)</span></div>
        <div><input type="text" value="🆑" readonly> <span>CL Button</span></div>
        <div><input type="text" value="🆒" readonly> <span>Cool Button</span></div>
        <div><input type="text" value="🆓" readonly> <span>Free Button</span></div>
        <div><input type="text" value="ℹ" readonly> <span>Information</span></div>
        <div><input type="text" value="🆔" readonly> <span>ID Button</span></div>
        <div><input type="text" value="Ⓜ" readonly> <span>Circled M</span></div>
        <div><input type="text" value="🆕" readonly> <span>New Button</span></div>
        <div><input type="text" value="🆖" readonly> <span>NG Button</span></div>
        <div><input type="text" value="🅾" readonly> <span>O Button (blood Type)</span></div>
        <div><input type="text" value="🆗" readonly> <span>OK Button</span></div>
        <div><input type="text" value="🅿" readonly> <span>P Button</span></div>
        <div><input type="text" value="🆘" readonly> <span>SOS Button</span></div>
        <div><input type="text" value="🆙" readonly> <span>Up! Button</span></div>
        <div><input type="text" value="🆚" readonly> <span>Vs Button</span></div>
        <div><input type="text" value="🈁" readonly> <span>Japanese &ldquo;here&rdquo; Button</span></div>
        <div><input type="text" value="🈂" readonly> <span>Japanese &ldquo;service Charge&rdquo; Button</span></div>
        <div><input type="text" value="🈷" readonly> <span>Japanese &ldquo;monthly Amount&rdquo; Button</span></div>
        <div><input type="text" value="🈶" readonly> <span>Japanese &ldquo;not Free of Charge&rdquo; Button</span></div>
        <div><input type="text" value="🈯" readonly> <span>Japanese &ldquo;reserved&rdquo; Button</span></div>
        <div><input type="text" value="🉐" readonly> <span>Japanese &ldquo;bargain&rdquo; Button</span></div>
        <div><input type="text" value="🈹" readonly> <span>Japanese &ldquo;discount&rdquo; Button</span></div>
        <div><input type="text" value="🈚" readonly> <span>Japanese &ldquo;free of Charge&rdquo; Button</span></div>
        <div><input type="text" value="🈲" readonly> <span>Japanese &ldquo;prohibited&rdquo; Button</span></div>
        <div><input type="text" value="🉑" readonly> <span>Japanese &ldquo;acceptable&rdquo; Button</span></div>
        <div><input type="text" value="🈸" readonly> <span>Japanese &ldquo;application&rdquo; Button</span></div>
        <div><input type="text" value="🈴" readonly> <span>Japanese &ldquo;passing Grade&rdquo; Button</span></div>
        <div><input type="text" value="🈳" readonly> <span>Japanese &ldquo;vacancy&rdquo; Button</span></div>
        <div><input type="text" value="㊗" readonly> <span>Japanese &ldquo;congratulations&rdquo; Button</span></div>
        <div><input type="text" value="㊙" readonly> <span>Japanese &ldquo;secret&rdquo; Button</span></div>
        <div><input type="text" value="🈺" readonly> <span>Japanese &ldquo;open for Business&rdquo; Button</span></div>
        <div><input type="text" value="🈵" readonly> <span>Japanese &ldquo;no Vacancy&rdquo; Button</span></div>
        <div><input type="text" value="▪" readonly> <span>Black Small Square</span></div>
        <div><input type="text" value="▫" readonly> <span>White Small Square</span></div>
        <div><input type="text" value="◻" readonly> <span>White Medium Square</span></div>
        <div><input type="text" value="◼" readonly> <span>Black Medium Square</span></div>
        <div><input type="text" value="◽" readonly> <span>White Medium-Small Square</span></div>
        <div><input type="text" value="◾" readonly> <span>Black Medium-Small Square</span></div>
        <div><input type="text" value="⬛" readonly> <span>Black Large Square</span></div>
        <div><input type="text" value="⬜" readonly> <span>White Large Square</span></div>
        <div><input type="text" value="🔶" readonly> <span>Large Orange Diamond</span></div>
        <div><input type="text" value="🔷" readonly> <span>Large Blue Diamond</span></div>
        <div><input type="text" value="🔸" readonly> <span>Small Orange Diamond</span></div>
        <div><input type="text" value="🔹" readonly> <span>Small Blue Diamond</span></div>
        <div><input type="text" value="🔺" readonly> <span>Red Triangle Pointed Up</span></div>
        <div><input type="text" value="🔻" readonly> <span>Red Triangle Pointed Down</span></div>
        <div><input type="text" value="💠" readonly> <span>Diamond With a Dot</span></div>
        <div><input type="text" value="🔲" readonly> <span>Black Square Button</span></div>
        <div><input type="text" value="🔳" readonly> <span>White Square Button</span></div>
        <div><input type="text" value="⚪" readonly> <span>White Circle</span></div>
        <div><input type="text" value="⚫" readonly> <span>Black Circle</span></div>
        <div><input type="text" value="🔴" readonly> <span>Red Circle</span></div>
        <div><input type="text" value="🔵" readonly> <span>Blue Circle</span></div>
    </div>
    <div class="emocat" data-name="Arrows">
        <div><input type="text" value="🔃" readonly> <span>Clockwise Vertical Arrows</span></div>
        <div><input type="text" value="🔄" readonly> <span>Counterclockwise Arrows Button</span></div>
        <div><input type="text" value="🔙" readonly> <span>Back Arrow</span></div>
        <div><input type="text" value="🔚" readonly> <span>End Arrow</span></div>
        <div><input type="text" value="🔛" readonly> <span>On! Arrow</span></div>
        <div><input type="text" value="🔜" readonly> <span>Soon Arrow</span></div>
        <div><input type="text" value="🔝" readonly> <span>Top Arrow</span></div>
        <div><input type="text" value="➤" readonly> <span>Right Arrowhead</span></div>
        <div><input type="text" value="➧" readonly> <span>Squat Black Right Arrow</span></div>
    </div>
    <div class="emocat" data-name="Currency">
        <div><input type="text" value="💴" readonly> <span>Yen Banknote</span></div>
        <div><input type="text" value="💵" readonly> <span>Dollar Banknote</span></div>
        <div><input type="text" value="💶" readonly> <span>Euro Banknote</span></div>
        <div><input type="text" value="💷" readonly> <span>Pound Banknote</span></div>
    </div>
    <div class="emocat" data-name="HTML4">

        <div><input type="text" value="◊" readonly> <span>Lozenge</span></div>
        <div><input type="text" value="♠" readonly> <span>Black Spade Suit</span></div>
        <div><input type="text" value="♣" readonly> <span>Black Club Suit / Shamrock</span></div>
        <div><input type="text" value="♥" readonly> <span>Black Heart Suit / Valentine</span></div>
        <div><input type="text" value="♦" readonly> <span>Black Diamond Suit</span></div>
    </div>

</body>

</html>