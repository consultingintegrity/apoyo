<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  $_SESSION['MM_Nombre'] = NULL;
  $_SESSION['MM_SEUDONIMO'] = NULL;
  $_SESSION['MM_IdQuien'] = NULL;
  
  
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
  unset($_SESSION['MM_Nombre']);
  unset($_SESSION['MM_SEUDONIMO']);
  unset($_SESSION['MM_IdQuien']); 

  	
  $logoutGoTo = "inicio.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "1,4,5";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "signin.php?A=1";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
$MMestatus=array(    1 => "SIN ASIGNAR",    2 => "ASIGNADO",    3 => "EN REVISION",	4 => "EN ESPERA",	5 => "CANCELADO",	6 => "ATENDIDO" );

$MMcolor= array ( 1 => "label-success",    2 => "label-warning",    3 => "label-default",	4 => "label-default",	5 => "label-danger",	6 => "label-default" );

$mmMES= array("01"=>"Enero","02"=>"Febrero","03"=>"Marzo","04"=>"Abril","05"=>"Mayo","06"=>"Junio","07"=>"Julio","08"=>"Agosto","09"=>"Septiembre","10"=>"Octubre","11"=>"Noviembre","12"=>"Diciembre");

$mmEDO= array("1"=>"AGUASCALIENTES","2"=>"BAJA CALIFORNIA NORTE","3"=>"BAJA CALIFORNIA SUR","4"=>"CAMPECHE","5"=>"COAHUILA","6"=>"CHIAPAS","8"=>"CHIHUAHUA","9"=>"CDMX","10"=>"DURANGO","11"=>"GUANAJUATO","12"=>"GUERRERO","13"=>"HIDALGO","14"=>"JALISCO","15"=>"EDOMEX","16"=>"MICHOACAN","17"=>"MORELOS","18"=>"NAYARIT","19"=>"NUEVO LEON",
"20"=>"OAXACA","21"=>"PUEBLA","22"=>"QUERETARO","23"=>"QUINTANA ROO","24"=>"SAN LUIS POTOSI","25"=>"SINALOA","26"=>"SONORA","27"=>"TABASCO","28"=>"TAMAULIPAS","29"=>"TLAXCALA","30"=>"VERACRUZ","31"=>"YUCATAN","32"=>"ZACATECAS","33"=>"COLIMA");

$PA=array(1=>"Autoestima",2=>"Duelo y perdidas",3=>"Pensamientos de muerte",4=>"Ideación suicida",
 5=>"Intento suicida",6=>"Autolesión",7=>"Problemas de relación",
 8=>"Relación conflictiva con el cónyuge o la pareja",9=>"Ruptura familiar por separación o divorcio",
 10=>"Maltrato",11=>"Abuso sexual",12=>"Violencia por parte del cónyuge o pareja (física,sexual,psicológica,económica)",
 13=>"Maltrato por parte de una persona distinta al cónyuge",14=>"Problemas educativos",
 15=>"Problemas laborales",16=>"Alojamiento inadecuado",17=>"Discordia con vecino o inquilino",
 18=>"Vida en una residencia",19=>"Ingresos bajos",20=>"Seguro social o asistencia pública insuficiente",
 21=>"Problema o crisis de vida vinculado con la etapa de desarrollo",22=>"Problema relacionado con vivir solo",
 23=>"Dificultad de aculturación",24=>"Exclusión o rechazo social",25=>"Blanco de discriminación",
 26=>"Víctima de delincuencia",27=>"Sentencia penal sin encarcelamiento",28=>"Encarcelamiento",
 29=>"Problema relacionado con excarcelación",30=>"Problemas relacionados con otras circunstancias legales",
 31=>"Asesoramiento sexual",32=>"Otro asesoramiento o consulta",33=>"Problema religioso o espiritual",
 34=>"Embarazo no deseado",35=>"Multiparidad",36=>"Víctima de tortura",
 37=>"Otro problema relacionado con circunstancias psicosociales",
 38=>"Historia personal con trauma psicológico",39=>"Historia de autolesión",
 40=>"Problema relacionado con el estilo de vida",41=>"Víctima de acoso y/o ciberacoso",
 42=>"Manejo de emociones (problemas de ira,estado de ánimo cambiante,etc.)",
 43=>"Psicoeducación,orientación,acompañamiento");

$PS=array(1=>"Discriminación",2=>"Sentimiento de aislamiento",3=>"Poco acompañamiento por parte de las autoridades",
 4=>"Sentimientos de Frustración",5=>"Falta del equipo e insumos necesarios para el desempeño adecuado",
 6=>"Catársis (desahogo emocional)");

$PD=array(1=>"Esquizofrenia y otros trastornos psicóticos",
2=>"Trastorno bipolar y trastornos relacionados",
3=>"Trastornos depresivos",
4=>"Trastornos de ansiedad",
5=>"Trastornos obsesivos-compulsivos y trastornos relacionados",
6=>"Trastornos relacionados con traumas y factores de estrés",
7=>"Trastornos de síntomas somáticos y trastornos relacionados",
8=>"Trastornos alimentarios y de la ingestión de alimentos",
9=>"Trastornos destructivos, del control de los impulsos y de la conducta",
10=>"Trastornos relacionados con sustancias y trastornos adictivos",
11=>"Trastornos de la personalidad");

?>