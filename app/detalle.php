<?php
date_default_timezone_set('America/Mexico_City');
require_once('../Connections/asesorias.php'); ?><?php require_once('acs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $hostname_pendientes = "localhost";
    $database_pendientes = "psicologo";
    $username_pendientes = "root";
    $password_pendientes = "";
    $pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(), E_USER_ERROR);


    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}

$mmIDP = "";
if (isset($_GET['ID'])) {
  $mmIDP = $_GET['ID'];
}

$maxRows_pen = 500;
$pageNum_pen = 0;
if (isset($_GET['pageNum_pen'])) {
  $pageNum_pen = $_GET['pageNum_pen'];
}
$startRow_pen = $pageNum_pen * $maxRows_pen;

mysqli_query($pendientes, "SET NAMES 'utf8'");
mysqli_select_db($pendientes, $database_pendientes);
$query_pen = sprintf("SELECT a.*, b.EMAIL, b.TEL, b.SEUDONIMO, b.ID as IDPACIENTE FROM pendiente a, usuarios b WHERE a.IDQUIEN=b.ID and a.ID =%s ", $mmIDP); // GRS pasar por el scapetring IMPORTANTE
$query_limit_pen = sprintf("%s LIMIT %d, %d", $query_pen, $startRow_pen, $maxRows_pen);
$pen = mysqli_query($pendientes, $query_limit_pen) or die(mysqli_error($pendientes));
$row_pen = mysqli_fetch_array($pen);

if (isset($_GET['totalRows_pen'])) {
  $totalRows_pen = $_GET['totalRows_pen'];
} else {
  $all_pen = mysqli_query($pendientes, $query_pen);
  $totalRows_pen = mysqli_num_rows($all_pen);
}
$totalPages_pen = ceil($totalRows_pen / $maxRows_pen) - 1;

$queryString_pen = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (
      stristr($param, "pageNum_pen") == false &&
      stristr($param, "totalRows_pen") == false
    ) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_pen = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_pen = sprintf("&totalRows_pen=%d%s", $totalRows_pen, $queryString_pen);


$loginFormAction = $_SERVER['PHP_SELF'];
//$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $loginFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["mm_cnuevo"])) && ($_POST["mm_cnuevo"] == "form2")) {

  /////// inicia insert comentario  
  if ($_POST['COMENT'] != null) {
    $insertSQL2 = sprintf(
      "INSERT INTO coment (QUIEN, FECHA, IDPENDIENTE, ESTATUS, COMENT, TIPO) VALUES (%s, %s, %s, %s, %s, %s)",
      GetSQLValueString($_SESSION['MM_SEUDONIMO'], "text"),
      GetSQLValueString(date('Y-m-d H:i:s'), "date"),
      GetSQLValueString($_POST['IDPEN'], "int"),
      GetSQLValueString(1, "int"),
      GetSQLValueString($_POST['COMENT'], "text"),
      GetSQLValueString($_SESSION['MM_UserGroup'], "int")
    );
    mysqli_query($pendientes, "SET NAMES 'utf8mb4'");
    mysqli_select_db($pendientes, $database_pendientes);
    $Result2 = mysqli_query($pendientes, $insertSQL2) or die(mysql_error($pendientes));
  } //// fin de insert de comentarios

  $insertGoTo = "detalle.php?ID=" . $_POST['IDPEN'];
  /* if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }*/

  header(sprintf("Location: %s", $insertGoTo));
}
////////////////// PERTNECE trae la lista de psicologos/////
mysqli_select_db($pendientes, $database_pendientes);
$query_usuario = "SELECT a.ID, a. NOMBRE FROM usuarios a WHERE a.TIPO!=5 and a.ID>1";

$usuario = mysqli_query($pendientes, $query_usuario) or die(mysqli_error($pendientes));
$row_usuario = mysqli_fetch_array($usuario);

$totalRows_usuario = mysqli_num_rows($usuario);
///////// termina pertenece 


//// UPDATE/////
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf(
    "UPDATE pendiente SET ESTATUS=%s,  IDASIGNADO=%s WHERE ID=%s",
    GetSQLValueString(2, "int"),
    GetSQLValueString($_POST['IDASIGNADO'], "int"),
    GetSQLValueString($_POST['ID'], "int")
  );

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $updateSQL) or die(mysqli_error($pendientes));

  /////// inicia insert comentario  SEGUNDO TEXTO
  $mmTXTinicial = "Gracias por contactarnos ¿En qué te puedo servir? ¿Me podrías compartir la situación en la que te encuentras?";
  $insertSQL = sprintf(
    "INSERT INTO coment (QUIEN, FECHA, IDPENDIENTE, ESTATUS,TIPO, COMENT) VALUES (%s, %s, %s, %s, %s, %s)",
    GetSQLValueString($_SESSION['MM_Nombre'], "text"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date"),
    GetSQLValueString($_POST['ID'], "int"),
    GetSQLValueString(1, "int"),
    GetSQLValueString(1, "int"),
    GetSQLValueString($mmTXTinicial, "text")
  );
  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result2 = mysqli_query($pendientes, $insertSQL) or die(mysqli_error($pendientes));
  //// fin de insert de comentarios

  $updateGoTo = "inicio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }

  header(sprintf("Location: %s", $updateGoTo));
  
  
}


///////////////////// TERMINA UPDATE

$mmP = "d";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("google.php"); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Contacto Nikon">
    <meta name="author" content="aaltaan.com">
    <link rel="shortcut icon" href="/favicon.png">

    <title>apoyo-psicologico.mx - Inicio</title>

    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css">
    <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css">
    <link rel="stylesheet" href="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../lib/select2/select2.css">
    <link rel="stylesheet" href="../css/quirk.css">
    <script src="../lib/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../css/chat.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/emojionearea.min.css" rel="stylesheet">

</head>

<body>
    <header>
        <?php include("header.php"); ?>
    </header>

    <section>
        <div class="leftpanel">
            <?php include("leftpanel.php"); ?>
            <!-- leftpanelinner -->
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="contentpanel">
                <div>
                    <ol class="breadcrumb breadcrumb-quirk">
                        <li>
                            <i class="fa fa-comment mr5"></i> Usuario:
                            <strong><?php echo $row_pen['QUIEN']; ?></strong>
                        </li>
                        <?php if ($row_pen['ESTATUS'] == 1) { ?><li></li><?php } ?>
                    </ol>
                </div>

                <div class="row">
                    <!-- <div class="col-sm-6">
            <?php if ($_SESSION['MM_UserGroup'] < 5 and $row_pen['ESTATUS'] == 2) {
              include("reloj.php");
            } ?>
          </div>-->
                    <div class="col-sm-6" style="text-align:right;margin-right:auto; width:100%;float:left;">
                        <?php if ($_SESSION['MM_UserGroup'] < 5 and  $row_pen['ESTATUS'] == 2) { ?>
                        <a href="detalle_cierre.php?ID=<?php echo $row_pen['ID']; ?>"><i class="fa fa-cog"></i> Terminar
                            consulta</a><?php } ?>
                        <?php if ($_SESSION['MM_UserGroup'] < 5) { ?> |
                        <a href="inicio.php?EST=2" target="_new"><i class="fa fa-sign-out"></i> Abrir nueva ventana
                        </a><?php } ?>
                    </div>
                </div>
                <?php if ($row_pen['ESTATUS'] == 1 and $_SESSION['MM_UserGroup'] < 3) { ?>
                <form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1">
                    <div class="form-group mb15">
                        <div class="form-group">
                            <select class="form-control" style="width: 50%" data-placeholder="Solicitante"
                                name="IDASIGNADO" required>
                                <option value="2">Selecciona un psicólogo</option>
                                <?php do { ?>
                                <option value="<?php echo $row_usuario['ID']; ?>"><?php echo $row_usuario['NOMBRE']; ?>
                                    / <?php cusuario($row_usuario['ID']); ?></option>
                                <?php } while ($row_usuario = mysqli_fetch_array($usuario)); ?>
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-success btn-quirk btn-block" style="width: 50%" type="submit" name="submit"
                        id="submit"><i class="fa fa-user-plus"></i> Asignar</button>

                    <input type="hidden" name="ID" value="<?php echo $row_pen['ID']; ?>" />
                    <input type="hidden" name="MM_update" value="form1" />
                </form>
                <?php } ?>
                <hr class="darken">
                <!-- row -->
                <!-- fin row -->

                <?php if ($_SESSION['MM_UserGroup'] < 5 and  $row_pen['ESTATUS'] == 6) { ?>
                <div class="panel panel-announcement">
                    <ul class="panel-options">
                        <li><a href="inicio.php" title="recargar"><i class="fa fa-refresh"></i></a></li>
                        <li><a class="panel-remove"><i class="fa fa-remove"></i></a></li>
                    </ul>
                    <div class="panel-heading">
                        <h4 class="panel-title">Cierre de consulta</h4>
                    </div>
                    <div class="panel-body">
                        <?php include("evaluacion.php"); ?>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="messaging">
                        <div class="inbox_msg">
                            <div class="mesgs" id="mmCHAT">
                                <div class="msg_history" id="live_table">
                                    <!-- contenido coment -->
                                </div>
                            </div>
                            <div class="mesgs2">
                                <div class="type_msg">
                                    <div class="input_msg_write">
                                        <form action="detalle.php?ID=<?php echo $_GET['ID']; ?>" method="POST"
                                            name="form2" id="form2">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <input id="content" type="text"class="write_msg"
                                                        placeholder="Escribe tu mensaje" name="COMENT"/>
                                                </div>
                                                <div class="col-md-0.05">
                                                    <button                                                        
                                                        class="msg_send_btn" type="button"
                                                        onClick="document.getElementById('form2').submit();"><i
                                                            class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="IDPEN" value="<?php echo $row_pen['ID']; ?>" />
                                            <input type="hidden" name="mm_cnuevo" value="form2" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- contentpanel -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </section>




    <script src="../lib/jquery/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="../js/emojionearea.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../lib/jquery-toggles/toggles.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="../lib/select2/select2.js"></script>
    <script src="../js/quirk.js"></script>

    <script>
    $(document).ready(function() {

        'use strict';

        $('#dataTable1').DataTable();

        var exRowTable = $('#exRowTable').DataTable({
            responsive: true,
            'fnDrawCallback': function(oSettings) {
                $('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            'ajax': 'ajax/objects.txt',
            'columns': [{
                    'class': 'details-control',
                    'orderable': false,
                    'data': null,
                    'defaultContent': ''
                },
                {
                    'data': 'name'
                },
                {
                    'data': 'position'
                },
                {
                    'data': 'office'
                },
                {
                    'data': 'start_date'
                },
                {
                    'data': 'salary'
                }
            ],
            'order': [
                [1, 'asc']
            ]
        });

        // Add event listener for opening and closing details
        $('#exRowTable tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        function format(d) {
            // `d` is the original data object for the row
            return '<h4>' + d.name + '<small>' + d.position + '</small></h4>' +
                '<p class="nomargin">txt</p>';
        }

        // Select2
        $('select').select2({
            minimumResultsForSearch: Infinity
        });

    });

    var table = $('#dataTable1').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },

    });

    $(document).ready(function() {

        function cargaDATOS() {
            $('#live_table').load('coment.php?ID=<?php echo $_GET['ID']; ?>',

            );

            $.ajaxSetup({
                cache: false
            });

        }
        <?php if ($row_pen['ESTATUS'] != 6) { ?>
        setInterval(cargaDATOS, 9000);
        <?php } ?>
        cargaDATOS();
    });

    function scrollSmoothToBottom(id) {
        var div = document.getElementById(id);
        div.scrollTop = div.scrollHeight - div.clientHeight;
    }
    </script>
    <script>
    $("#content").emojioneArea({
        pickerPosition: "top",
    });
    </script>


    <!-- <?php // echo $query_limit_pen; 
        ?> -->
</body>

</html>
<?php
mysqli_free_result($pen);
mysqli_free_result($usuario);
//mysqli_close($pendientes);
?>