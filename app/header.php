     <div class="headerpanel">
         <div class="logopanel">
             <h4><a href="inicio.php"><i class="tooltips fa fa-home"> </i></a><span style="color:grey;">Inicio</span></h4>
         </div><!-- logopanel -->
         <div class="headerbar">
             <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
             <div class="header-right">
                 <ul class="headermenu">
                     <li>
                         <div class="btn-group">
                             <button type="button" class="btn btn-logged" data-toggle="dropdown">
                                 <!--<img src="../images/photos/loggeduser.png" alt="" /> Codigo que muestra una imagen oculta en el boton de salir -->
                                 <i class="glyphicon glyphicon-log-out"></i> Salir
                                 <span class="caret"></span>
                             </button>
                             <ul class="dropdown-menu pull-right">
                                 <!--lista que se despliega cuando se da click en el boton de salir-->
                                 <!-- <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Preguntas Frecuentes</a></li>-->
                                 <li><a href="<?php echo $logoutAction ?>"><i class="glyphicon glyphicon-log-out"></i>
                                         Salir</a></li>
                             </ul>
                         </div>
                     </li>
                 </ul>
             </div><!-- header-right -->
         </div><!-- headerbar -->
     </div><!-- header-->