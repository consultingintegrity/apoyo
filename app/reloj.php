<?php 
date_default_timezone_set('America/Mexico_City');
?><form name="formulario">
    <input type="text" name="reloj" value="00:00:00"
        style="border : 0px; text-align : center; background-color:transparent;font-size:20px;">
</form>
<?php 
	$mmHORA = date('YYYY-mm-jj H:i:s');
	$mmHORAnva = strtotime( '+30 minute', strtotime ($mmHORA));
	$mmHORAmin = strtotime( '+30 minute', strtotime ($mmHORA));
   // $mmdia=strtotime('+1 day',strtotime($mmHORA));
    //$mmMes=strtotime('+1 day',strtotime($mmHORA));
	//$mmHORAnva= date('H:i:s',$mmHORAnva);
    
	$mmHOR= date('H',$mmHORAnva);
	$mmMIN= date('i',$mmHORAmin);
	?>
<script>
//reloj contador
var futuro = new Date(2022, 05, 18, <?php echo $mmHOR; ?>, <?php echo $mmMIN; ?>);
//actualiza el contador cada 1 segundos ( = 1000 milisegundos)
var actualiza = 1000;
// función que calcula y escribe el tiempo en días, horas, minutos y segundos
// que faltan para la variable futuro
function faltan() {
    var ahora = new Date();
    var faltan = futuro - ahora;
    // si todavís no es futuro
    if (faltan > 0) {
        var segundos = Math.round(faltan / 1000);
        var minutos = Math.floor(segundos / 60);
        var segundos_s = segundos % 60;
        var horas = Math.floor(faltan / 60);
        var minutos_s = minutos % 60;
        var dias = Math.floor(horas / 24);
        var horas_s = horas % 24;
        // escribe los resultados
        (segundos_s < 10) ? segundos_s = "0" + segundos_s: segundos_s = segundos_s;
        (minutos_s < 10) ? minutos_s = "0" + minutos_s: minutos_s = minutos_s;
        (horas_s < 10) ? horas_s = "0" + horas_s: horas_s = horas_s;
        (dias < 10) ? dias = "0" + dias: dias = dias;
        var resultado = minutos_s + " : " + segundos_s;
        document.formulario.reloj.value = resultado;
        //actualiza el contador
        setTimeout("faltan()", actualiza);
    }
    // estamos en el futuro
    else {
        document.formulario.reloj.value = " 00 minutos : 00 segundos";
    }
}
faltan();
</script>