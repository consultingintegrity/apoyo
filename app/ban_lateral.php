<div class="col-md-3 col-lg-4 dash-right">
    <div class="row">
        <div class="col-sm-5 col-md-12 col-lg-6">
            <div class="panel panel-inverse panel-weather">
                <div class="panel-heading">
                    <h4 class="panel-title">Telefonos de emergencia</h4>
                </div>
                <div class="panel-body">
                    Línea de atención COVID-19 Querétaro<br>
                    <strong>442 101 5205</strong><br>
                    EMERGENCIAS<br>
                    <strong>911</strong><br>
                    Denuncia Anónima<br>
                    <strong>089</strong><br>
                    TELMUJER:<br>
                    <strong>01 800 00 83568 o 442 216 4757 </strong><br>
                    Protección Civil del Estado<br>
                    <strong>442 3091430</strong><br>
                    LOCATEL Querétaro<br>
                    <strong>442 229 1111</strong><br>
                    Atención ciudadana del Municipio de Querétaro<br>
                    <strong>070</strong><br>
                </div>
            </div>

            <?php if ($_SESSION['MM_UserGroup'] < 3) { ?>
            <div class="panel panel-inverse panel-weather">
                <div class="panel-heading">
                    <h4 class="panel-title">Documentos</h4>
                </div>
                <div class="panel-body">
                    <a href="docs/flujograma_queretaro.pdf" target="new"><i class="fa fa-download"></i>
                        Flujograma</a><br><br>
                    <a href="docs/protocolo_para_la_atencioon_psicoologica_a_distancia_COVID-2020.pdf"><i
                            class="fa fa-download"></i> Protocolo</a>
                </div>
            </div>
            <?php } ?>
        </div><!-- col-md-12 -->
        <!-- col-md-12 -->
    </div><!-- row -->
    <!-- row -->
</div>