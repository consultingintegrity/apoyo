<?php
////////////////////////////////////////////
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form3")) {
  $updateSQL = sprintf(
    "UPDATE usuarios SET SEUDONIMO=%s,  EMAIL=%s WHERE ID=%s",
    GetSQLValueString($_POST['SEUDONIMO'], "text"),
    GetSQLValueString($_POST['EMAIL'], "text"),
    GetSQLValueString($_POST['IDUS'], "int")
  );

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $updateSQL) or die(mysqli_error($pendientes));

  $updateGoTo = "detalle.php?ID=" . $_POST['IDPEN'];

  header(sprintf("Location: %s", $updateGoTo));
}


///////////////////////////////////////////////////////
?>
<?php
function cusuario($mmIDQUIEN)
{
  global $hostname_pendientes, $database_pendientes, $username_pendientes, $password_pendientes, $pendientes, $cuantosG;

  if ($_SESSION['MM_UserGroup'] == 5) {
    $mmfilto = " and a.IDQUIEN=" . $mmIDQUIEN;
  } else {
    $mmfilto = " and a.IDASIGNADO=" . $mmIDQUIEN;
  }

  mysqli_select_db($pendientes, $database_pendientes);
  $query_cusuario = "SELECT count(a.ID) as CUANTOS FROM pendiente a WHERE a.ESTATUS=2 " . $mmfilto;
  $cusuario = mysqli_query($pendientes, $query_cusuario) or die(mysqli_error($pendientes));
  $row_cusuario = mysqli_fetch_array($cusuario);
  $totalRows_cusuario = mysqli_num_rows($cusuario);
  $cuantosG =  $row_cusuario['CUANTOS'];
  echo $row_cusuario['CUANTOS'];
  mysqli_free_result($cusuario);
}
?>
<?php
/// cuando no es paciente y esta en pagina de detalle
if ($_SESSION['MM_UserGroup'] != 5 and $mmP == "d") {

  mysqli_select_db($pendientes, $database_pendientes);
  $query_historia = "SELECT a.ID, a.ESTATUS, a.IDASIGNADO, b.NOMBRE as PSICO  FROM pendiente a, usuarios b WHERE a.IDQUIEN=" . $row_pen['IDQUIEN'] . "  and  a.IDASIGNADO=b.ID ORDER BY a.ID DESC";
  $historia = mysqli_query($pendientes, $query_historia) or die(mysqli_error($pendientes));
  $row_historia = mysqli_fetch_array($historia);
  $totalRows_historia = mysqli_num_rows($historia);
}
?>
<div class="leftpanelinner">

    <!-- ################## LEFT PANEL PROFILE ################## -->

    <div class="media leftpanel-profile">
        <div class="media-body">
            <h4 class="media-heading"><?php echo "Usuario:" . $_SESSION['MM_SEUDONIMO'] ?> <a data-toggle="collapse"
                    data-target="#loguserinfo" class="pull-right" title="Detalles"><i class="fa fa-angle-down"></i></a>
            </h4>
            <!--. "/" . $_SESSION['MM_UserGroup']; especifica el rol que tiene y lo pinta-->
            <!-- <h4 class="media-heading"><?php echo $_SESSION['MM_SEUDONIMO'] . "/" . $_SESSION['MM_UserGroup']; ?> <a data-toggle="collapse" data-target="#loguserinfo" class="pull-right" title="Detalles"><i class="fa fa-angle-down"></i></a></h4>-->

        </div>
    </div><!-- leftpanel-profile -->

    <div class="leftpanel-userinfo collapse" id="loguserinfo">
        <h5 class="sidebar-title">Estado</h5>
        <address>
            <?php //echo $mmEDO[$row_pen['EDO']]; 
      ?>Querétaro
        </address>
        <h5 class="sidebar-title">Contacto</h5>
        <ul class="list-group">
            <?php if ($_SESSION['MM_UserGroup'] == 5) { // valida que la pagina sea la de detalle y que no sea paciente
      ?>
            <li class="list-group-item">
                <label class="pull-left">Email paciente</label>
                <span class="pull-right"><?php echo $_SESSION['MM_Username']; ?></span>
            </li>
            <li class="list-group-item">
                <label class="pull-left">Celular paciente</label>
                <span class="pull-right"><?php echo $_SESSION['MM_Username']; ?></span>
            </li>
            <?php } // fin pasiente
      ?>

            <?php if ($_SESSION['MM_UserGroup'] != 5 and $mmP == "d") { // valida que la pagina sea la de detalle y que no sea paciente
      ?>
            <li class="list-group-item">
                <label class="pull-left">Email paciente</label>
                <span class="pull-right"><?php echo $row_pen['EMAIL']; ?></span>
            </li>
            <li class="list-group-item">
                <label class="pull-left">Celular paciente</label>
                <span class="pull-right"><?php echo $row_pen['TEL']; ?></span>
            </li>
            <?php } // fin sin detalle
      ?>

            <?php if ($_SESSION['MM_UserGroup'] != 5) { // no lo puede ver el paciente
      ?>
            <li class="list-group-item">
                <label class="pull-left">Violencia (Telmujer):</label>
                <span class="pull-right"><a href="https://api.whatsapp.com/send?phone=+524461390970" target="_new">446
                        1390 970</a></span>
            </li>

            <li class="list-group-item">
                <label class="pull-left">Plan suicida (911):</label>
                <span class="pull-right"><a href="https://api.whatsapp.com/send?phone=+524428199936" target="_new">442
                        8199 936</a></span>
            </li>
            <?php } ?>
        </ul>
    </div><!-- leftpanel-userinfo -->

    <!-- <ul class="nav nav-tabs nav-justified nav-sidebar">
    <li class="tooltips active" data-toggle="tooltip" title="Inicio"><a href="inicio.php" data-toggle="tab" data-target="#mainmenu"><i class="tooltips fa fa-home"></i></a></li>
    <?php if ($_SESSION['MM_UserGroup'] < 5 and $mmP == "d") { // pone pestaña historial de usuario div abajo
    ?>
      <li class="tooltips" data-toggle="tooltip" title="Settings"><a data-toggle="tab" data-target="#contactmenu"><i class="fa fa-user"></i></a></li><?php } ?>

  </ul>-->

    <div class="tab-content">
        <!-- ################# MAIN MENU ################### -->
        <div class="tab-pane active" id="mainmenu">
            <ul class="nav nav-pills nav-stacked nav-quirk">
                <li><a href="inicio.php<?php if ($_SESSION['MM_UserGroup'] < 5) {
                                  echo "?MC=1";
                                } ?>" title="Mis Consultas asignadas">
                        <span class="badge pull-right"
                            id="cuantospen"><?php cusuario($_SESSION['MM_IdQuien']); ?>+</span>
                        <i class="fa fa-check-square"></i>
                        <span>Mis consultas</span></a>
                </li>

                <?php if ($mmP == "d" and $_SESSION['MM_UserGroup'] < 5) { // psicologo en detalle
        ?>
                <li><a href="nuevo.php?IDUS=<?php echo $row_pen['IDQUIEN']; ?>&NOMC=<?php echo $row_pen['QUIEN']; ?>"
                        title="Agregar nuevo consulta"><i class="fa fa-file"></i> <span>Nueva consulta </span></a></li>
                <?php } ?>

                <?php if ($_SESSION['MM_UserGroup'] == 5) { // usuario
        ?>
                <li><a href="nuevo.php?IDUS=<?php echo $_SESSION['MM_IdQuien']; ?>" title="Agregar nuevo consulta"><i
                            class="fa fa-file"></i> <span>Nueva consulta </span></a></li>
                <?php } ?>
            </ul>
            <?php //////////// lista de de estatsu No usuarios 
      ?>
            <?php if ($_SESSION['MM_UserGroup'] != 5) { // NO usuario
      ?>
            <ul class="nav nav-pills nav-stacked nav-quirk">
                <?php $mmEST = "";
          if (isset($_GET['EST'])) {
            $mmEST = $_GET['EST'];
          } ?>
                <li class="nav-parent <?php if ($mmP == "i" or $mmP == "rp") { ?>active<?php } ?>"><a href="#"><i
                            class="fa fa-folder-open"></i> <span>Consultas</span></a>
                    <ul class="children">
                        <li <?php if ($mmEST == 1) { ?>class="active" <?php } ?>><a href="inicio.php?EST=1">Sin
                                Asignar</a></li>
                        <li <?php if ($mmEST == 2) { ?>class="active" <?php } ?>><a
                                href="inicio.php?EST=2">Asignadas</a></li>
                        <li <?php if ($mmEST == 6) { ?>class="active" <?php } ?>><a
                                href="inicio.php?EST=6">Atendidas</a></li>
                        <li <?php if ($mmEST == 5) { ?>class="active" <?php } ?>><a
                                href="inicio.php?EST=5">Canceladas</a></li>
                        <li <?php if ($mmEST == NULL) { ?>class="active" <?php } ?>><a href="inicio.php">Todas</a></li>
                    </ul>
                </li>
            </ul>
            <?php } ?>
            <?php //////////// lista de psicologos ADMIN 
      ?>
            <?php if ($_SESSION['MM_IdQuien'] < 4) { // NO usuario
      ?>
            <ul class="nav nav-pills nav-stacked nav-quirk">
                <li class="nav-parent <?php if ($mmP == "us" or $mmP == "rp") { ?>active<?php } ?>"><a href="#"><i
                            class="glyphicon glyphicon-user"></i> <span>Usuarios</span></a>
                    <ul class="children">
                        <li><a href="usuarios.php">Ver todos</a></li>
                        <li><a href="usuarios_new.php">Dar de alta nuevo</a></li>
                    </ul>
                </li>
            </ul>
            <?php } ?>
            <?php //////////// lista de reportes solo 1 y 3  
      ?>
            <?php if ($_SESSION['MM_IdQuien'] == 2 or $_SESSION['MM_IdQuien'] == 8) { // NO usuario
      ?>
            <ul class="nav nav-pills nav-stacked nav-quirk">
                <li class="nav-parent <?php if ($mmP == "rp") { ?>active<?php } ?>"><a href="#"><i
                            class="glyphicon glyphicon-download-alt"></i> <span>Reportes</span></a>
                    <ul class="children">
                        <li><a href="rep_consultas_xls.php">Consultas</a></li>
                        <li><a href="rep_usuarios_xls.php" >Usuarios</a></li>
                        <li><a href="rep_eval_xls.php" >Evaluaciones</a></li>
                    </ul>
                </li>
            </ul>
            <?php } ?>


            <?php if ($mmP == "d" and $_SESSION['MM_UserGroup'] < 5) 
      { // valida que la pagina sea la de detalle
        ?>
            <div class="panel panel-inverse panel-weather">
                <div class="panel-heading">
                    <h4 class="panel-title">Perfil del usuario</h4>
                </div>

                <div class="panel-body">
                    <form class="form-signin" id="login" name="login" method="POST"
                        action="<?php echo $loginFormAction; ?>">
                        <div class="form-group mb10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" class="form-control" placeholder="Definir nombre" name="SEUDONIMO"
                                    value="<?php echo $row_pen['SEUDONIMO']; ?>">
                            </div>
                        </div>

                        <div class="form-group mb10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-inbox"></i></span>
                                <input type="text" class="form-control" placeholder="Definir correo" name="EMAIL"
                                    value="<?php echo $row_pen['EMAIL']; ?>">
                            </div>
                        </div>

                        <div>&nbsp;</div>
                        <div class="form-group">
                            <button class="btn btn-success btn-quirk btn-block">Guardar</button>
                            <input type="hidden" name="MM_update" value="form3" />
                            <input type="hidden" name="IDUS" value="<?php echo $row_pen['IDPACIENTE']; ?>" />
                            <input type="hidden" name="IDPEN" value="<?php echo $row_pen['ID']; ?>" />
                        </div>
                    </form>
                </div>
            </div>
            <?php } ?>


        </div>
        <?php if ($_SESSION['MM_UserGroup'] < 5 and $mmP == "d") { // solo lo ven psicologos y admin 
    ?>
        <!-- tab-pane -->
        <div class="tab-pane" id="contactmenu">
            <h5 class="sidebar-title">Consultas</h5>
            <ul class="media-list media-list-contacts">
                <?php if ($totalRows_historia >= 1) 
          {
            do { ?>
                <li class="media">
                    <a href="detalle.php?ID=<?php echo $row_historia['ID']; ?>" target="_new"
                        title="Abrira en ventana nueva">
                        <div class="media-body">
                            <span class="media-heading">AYP-10<?php echo $row_historia['ID']; ?></span>
                            <span
                                class="label <?php echo $MMcolor[$row_historia['ESTATUS']]; ?>"><?php echo $MMestatus[$row_historia['ESTATUS']]; ?></span><br>
                            <span> Psicólogo: <?php echo $row_historia['PSICO']; ?></span>
                        </div>
                    </a>
                </li>
                <?php  } while ($row_historia = mysqli_fetch_array($historia));
          } ?>

            </ul>
        </div>
        <?php } ?>
        <!-- tab-pane -->

    </div><!-- tab-content -->

</div><?php if ($_SESSION['MM_UserGroup'] != 5 and $mmP == "d") {
        mysqli_free_result($historia);
      } ?>