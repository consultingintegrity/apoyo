<?php
date_default_timezone_set('America/Mexico_City');
require_once('../Connections/asesorias.php'); ?><?php require_once('acs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $hostname_pendientes = "localhost";
    $database_pendientes = "psicologo";
    $username_pendientes = "root";
    $password_pendientes = "";
    $pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(), E_USER_ERROR);
    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}

mysqli_select_db($pendientes, $database_pendientes);
$query_usuario = sprintf("SELECT a.PERTENECE FROM usuarios a WHERE a.ID=%s ", GetSQLValueString($_SESSION['MM_IdQuien'], "text"));
$usuario = mysqli_query($pendientes, $query_usuario) or die(mysqli_error($pendientes));
$row_usuario = mysqli_fetch_array($usuario);
$totalRows_usuario = mysqli_num_rows($usuario);

/////////// insert nuevo 

$loginFormAction = $_SERVER['PHP_SELF'];
//$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $loginFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}


if ((isset($_POST["mm_cnuevo"])) && ($_POST["mm_cnuevo"] == "form2")) {

  $mmCTIT = $_POST['TITULO'];
  switch ($mmCTIT) {
    case 0:
      $mmTITULO = "No selecciono";
      break;
    case 1:
      $mmTITULO = "Público en general";
      break;
    case 2:
      $mmTITULO = "Familiares o personas adictas (familiar o paciente)";
      break;
    case 3:
      $mmTITULO = "Víctimas de violencia familiar y/o abuso ";
      break;
    case 4:
      $mmTITULO = "Pacientes con COVID-19 y familiares";
      break;
    case 5:
      $mmTITULO = "Pacientes con enfermedades preexistentes y familiares";
      break;
    case 6:
      $mmTITULO = "Profesionales de la salud (médic@s, enfermer@s, personal hospitalario)";
      break;
    case 7:
      $mmTITULO = "Ansiedad por temas relacionados a Covid -19";
      break;
    case 8:
      $mmTITULO = "Aislamiento social por covid";
      break;
    case 9:
      $mmTITULO = "Insomnio ";
      break;
  }

  $insertSQL = sprintf(
    "INSERT INTO pendiente (IDCLINTE, IDASIGNADO, IDQUIEN,  TITULO, ESTATUS, SOLICITUD, QUIEN, PRIORIDAD, ENTREGASOL) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
    GetSQLValueString(1, "int"),
    GetSQLValueString($_POST['IDASIGNADO'], "int"),
    GetSQLValueString($_POST['IDQUIEN'], "int"), // segun si es psicologo o pacinete
    GetSQLValueString($mmTITULO, "text"),
    GetSQLValueString(1, "int"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date"),
    GetSQLValueString($_POST['NOMUS'], "text"), // nombre de quien levanta
    GetSQLValueString(2, "text"), // prioridad
    GetSQLValueString($_POST['ENTREGASOL'], "date")
  );

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $insertSQL) or die(mysqli_error($pendientes));
  $ultimo_id = mysqli_insert_id($pendientes);

  /////// inicia insert comentario  GRS 
  $mmTXTinicial = "¡Bienvenid@! Estás en el chat de atención psicológica.  En un momento uno de nuestros Psicólogos estará contigo.";
  $insertSQL2 = sprintf(
    "INSERT INTO coment (QUIEN, FECHA, IDPENDIENTE, ESTATUS, COMENT) VALUES (%s, %s, %s, %s, %s)",
    GetSQLValueString($_SESSION['MM_Nombre'], "text"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date"),
    GetSQLValueString($ultimo_id, "int"),
    GetSQLValueString(1, "int"),
    GetSQLValueString($mmTXTinicial, "text")
  );
  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result2 = mysqli_query($pendientes, $insertSQL2) or die(mysql_error($pendientes));


  $insertGoTo = "inicio.php?E=1";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }

  if ($_POST['EMAIL'] == "memo") {
    //-- ARMA MAIL
    $dest = "guillermo@aaltaan.com";
    $head = "From: Pendientes <contacto@aaltaan.com>\r\n";
    $head .= "To: guillermo@lohechoenmexico.mx\r\n";
    $head .= "MIME-Version: 1.0\r\n";
    $head .= "Content-type: text/html; charset=utf-8\r\n";
    // cuerpo del mensaje
    $msg = "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
    $msg .= "<title>Aaltaan</title></head><body style=\"font-family:Arial, Helvetica;\">\n";

    $msg .= "<h5>Aaltaan.com</h5><h3>\n";
    $msg .= "<br /><br />Has recibido un pendiente mediante el la herramineta.<br />\n";
    $msg .= "<br />La solicitud de pendiente es la  <strong>" . $ultimo_id . "</strong><br /><br /></h3>\n";
    $msg .= "<br />" . $_POST['SOLICITUD'] . "<br /><br />\n";
    $msg .= "El tiempo de respuesta debe ser no mayor a 48 hrs hábiles. Para conocer el detalle, ingresa al <a href=\"http://www.aaltaan.com/pendientes/\"> panel de administración</a>\n";
    $msg .= "<br>Favor de no responder a este correo.<br /><br />\n";
    $msg .= "<h6>Todos los derechos reservdos Aaltaan.com<br /><br /></h6>\n";
    $msg .= "</body></html>\n";

    // envia mail
    mail($dest, "Solicitud - " . $ultimo_id, $msg, $head);
    //-- TERMINA MAIL	
  }

  header(sprintf("Location: %s", $insertGoTo));
}
//////////////// temina insert

$maxRows_pen = 500;
$pageNum_pen = 0;
if (isset($_GET['pageNum_pen'])) {
  $pageNum_pen = $_GET['pageNum_pen'];
}
$startRow_pen = $pageNum_pen * $maxRows_pen;

mysqli_query($pendientes, "SET NAMES 'utf8'");
mysqli_select_db($pendientes, $database_pendientes);
$query_pen = sprintf("SELECT a.*, c.NOMBRE, c.TEL, c.EDO FROM pendiente a, usuarios c WHERE a.IDQUIEN = c.ID and c.ID =%s GROUP BY a.ID ORDER BY a.IDQUIEN", $_SESSION['MM_IdQuien']);
$query_limit_pen = sprintf("%s LIMIT %d, %d", $query_pen, $startRow_pen, $maxRows_pen);
$pen = mysqli_query($pendientes, $query_limit_pen) or die(mysqli_error($pendientes));
$row_pen = mysqli_fetch_array($pen);

if (isset($_GET['totalRows_pen'])) {
  $totalRows_pen = $_GET['totalRows_pen'];
} else {
  $all_pen = mysqli_query($pendientes, $query_pen);
  $totalRows_pen = mysqli_num_rows($all_pen);
}
$totalPages_pen = ceil($totalRows_pen / $maxRows_pen) - 1;

$queryString_pen = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (
      stristr($param, "pageNum_pen") == false &&
      stristr($param, "totalRows_pen") == false
    ) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_pen = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_pen = sprintf("&totalRows_pen=%d%s", $totalRows_pen, $queryString_pen);

$mmgetIDUS = "";
if (isset($_GET['IDUS'])) {
  $mmgetIDUS = $_GET['IDUS'];
}
$mmgetNOMUS = $_SESSION['MM_Nombre'];
if (isset($_GET['NOMC'])) {
  $mmgetNOMUS = $_GET['NOMC'];
}


$mmP = "n";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("google.php"); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Contacto Nikon">
    <meta name="author" content="aaltaan.com">
    <link rel="shortcut icon" href="../../favicon.png">

    <title>apoyo-psicologico.mx - Inicio</title>

    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css">
    <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css">
    <link rel="stylesheet" href="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../lib/select2/select2.css">
    <link rel="stylesheet" href="../css/quirk.css">
    <script src="../lib/modernizr/modernizr.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body>

    <header><?php include("header.php"); ?></header>

    <section>

        <div class="leftpanel">
            <?php include("leftpanel.php"); ?>
            <!-- leftpanelinner -->
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="contentpanel">
                <div class="row">
                    <div class="col-md-9 col-lg-8 dash-left">
                        <div class="panel panel-announcement">
                            <ul class="panel-options">
                                <li><a><i class="fa fa-refresh"></i></a></li>
                                <li><a class="panel-remove"><i class="fa fa-remove"></i></a></li>
                            </ul>
                            <div class="panel-heading">

                            </div>
                            <div class="panel-body">
                                <h2>Bienvenid@ <?php echo $_SESSION['MM_Nombre']; ?>, es un gusto poder escucharte ...
                                </h2>
                                <p>¿Necesitas una nueva consulta? Dinos el motivo de tu solicitud y déjanos un mensaje
                                    breve y claro de lo que necesitas. En breve te pondremos en contacto con un
                                    especialista.</p>
                            </div>
                        </div><!-- panel -->

                        <!-- panel -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">Solicitar nueva consulta:</h4>
                            </div>
                            <div class="panel-body">
                                <form action="<?php echo $loginFormAction; ?>" method="POST" name="form2" id="form2">
                                    <div class="form-group mb15">
                                        <div class="form-group">
                                            <select class="form-control" style="width: 100%"
                                                data-placeholder="Solicitante" name="TITULO" required>
                                                <option value="0">Selecciona una opción</option>
                                                <option value="1">Público en general</option>
                                                <option value="2">Familiares o personas adictas (familiar o paciente)
                                                </option>
                                                <option value="3">Víctimas de violencia familiar y/o abuso </option>
                                                <option value="4">Pacientes con COVID-19 y familiares</option>
                                                <option value="5">Pacientes con enfermedades preexistentes y familiares
                                                </option>
                                                <option value="6">Profesionales de la salud (médic@s, enfermer@s,
                                                    personal hospitalario)</option>
                                                <option value="7">Ansiedad por temas relacionados a Covid -19</option>
                                                <option value="8">Aislamiento social por covid</option>
                                                <option value="9">Insomnio</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group mb15">
                                        <div class="form-group">
                                            <?php
                      $date = date('Y-m-d');
                      //sua 1 día
                      $diasiguiente = strtotime($date . '+1 day');
                      ?>
                                            <?php if ($_SESSION['MM_UserGroup'] != 5) 
                      { // no lo puede ver el paciente
                      ?>
                                            <input type="datetime-local" id="meeting-time" name="ENTREGASOL"
                                                value="<?php echo date('Y-m-d', $diasiguiente); ?>T09:00"
                                                min="2020-05-07T08:00" max="2020-12-12T00:00">

                                            <input type="hidden" value="<?php echo $mmgetIDUS; // es el usuario del pendiente 
                                                    ?>" name="IDQUIEN">

                                            <?php } 
                      else { ?>
                                            <input type="hidden" value="<?php echo date('Y-m-d\T09:00'); ?>"
                                                name="ENTREGASOL">
                                            <input type="hidden" value="<?php echo $_SESSION['MM_IdQuien']; ?>"
                                                name="IDQUIEN">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <input type="hidden" name="EMAIL" value="<?php echo $_SESSION['MM_Username']; ?>" />
                                    <input type="hidden" name="USTIPO" value="5" />
                                    <input type="hidden" name="IDASIGNADO"
                                        value="<?php echo $row_usuario['PERTENECE']; ?>" />
                                    <input type="hidden" name="NOMUS" value="<?php echo $mmgetNOMUS; ?>" />
                                    <input type="hidden" name="mm_cnuevo" value="form2" />
                                    <div class="form-group">
                                        <button class="btn btn-success btn-quirk btn-block" id="btnenviar">Enviar
                                            SOLICITUD</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- row -->
                        <!-- row -->
                    </div><!-- col-md-9 -->
                    <?php include("ban_lateral.php"); ?>
                </div><!-- row -->
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->

    </section>

    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../lib/jquery-toggles/toggles.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="../lib/select2/select2.js"></script>

    <script src="../js/quirk.js"></script>
    <script>
    $(document).ready(function() {
        'use strict';
        $('#dataTable1').DataTable();
        var exRowTable = $('#exRowTable').DataTable({
            responsive: true,
            'fnDrawCallback': function(oSettings) {
                $('#exRowTable_paginate ul').addClass('pagination-active-success');
            },
            'ajax': 'ajax/objects.txt',
            'columns': [{
                    'class': 'details-control',
                    'orderable': false,
                    'data': null,
                    'defaultContent': ''
                },
                {
                    'data': 'name'
                },
                {
                    'data': 'position'
                },
                {
                    'data': 'office'
                },
                {
                    'data': 'start_date'
                },
                {
                    'data': 'salary'
                }
            ],
            'order': [
                [1, 'asc']
            ]
        });

        // Add event listener for opening and closing details
        $('#exRowTable tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = exRowTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        function format(d) {
            // `d` is the original data object for the row
            return '<h4>' + d.name + '<small>' + d.position + '</small></h4>' +
                '<p class="nomargin">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';
        }

        // Select2
        $('select').select2({
            minimumResultsForSearch: Infinity
        });

    });
    </script>

</body>

</html>
<?php
mysqli_free_result($pen);
mysqli_free_result($usuario);
?>