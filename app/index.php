<?php
date_default_timezone_set('America/Mexico_City');
require_once('../Connections/asesorias.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $hostname_pendientes = "localhost";
    $database_pendientes = "psicologo";
    $username_pendientes = "root";
    $password_pendientes = "";
    $pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(), E_USER_ERROR);


    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $loginFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["mm_nuevo"])) && ($_POST["mm_nuevo"] == "form1")) {

  $mmIDCL = md5($_POST['TEL']); //enmascarilla el telefono
  //inserta usuario
  $insertSQL = sprintf(
    "INSERT INTO usuarios (NO, PSW, TIPO, NOMBRE, IDCLIENTE, EMAIL, IDEMAIL, PERTENECE, EDO, SEXO , TEL, SEUDONIMO, ANAC) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
    
    GetSQLValueString(55555, "int"), // ESTE NUMERO NO TIENE VALOR 
    GetSQLValueString($_POST['PSW'], "text"),
    GetSQLValueString(5, "int"), //5 = usuario 1= admin, 4 psicologo
    GetSQLValueString($_POST['NOMBRE'], "text"),
    GetSQLValueString(1, "int"), /// 1 = Queretaro 
    GetSQLValueString("na", "text"), // va el mail que en el formulario no se pide
    GetSQLValueString($mmIDCL, "text"), // en mascarilao del telefono queva en el campo IDEMAIL
    GetSQLValueString(2, "int"), //// SE ASIGAN SEGUN ESTADO EN EL CASE 
    GetSQLValueString($_POST['EDO'], "int"),
    GetSQLValueString($_POST['SEXO'], "text"),
    GetSQLValueString($_POST['TEL'], "text"),
    GetSQLValueString($_POST['NOMBRE'], "text"),
    GetSQLValueString($_POST['ANAC'], "int")
  ); //año de nacimiento

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $insertSQL) or die(mysqli_error($pendientes));
  $ultimo_id = mysqli_insert_id($pendientes);

  /// FIN insert usuario

  $mmCTIT = $_POST['TITULO'];
  switch ($mmCTIT) {
    case 0:
      $mmTITULO = "No selecciono";
      break;
    case 1:
      $mmTITULO = "Público en general";
      break;
    case 2:
      $mmTITULO = "Familiares o personas adictas (familiar o paciente)";
      break;
    case 3:
      $mmTITULO = "Víctimas de violencia familiar y/o abuso ";
      break;
    case 4:
      $mmTITULO = "Pacientes con COVID-19 y familiares";
      break;
    case 5:
      $mmTITULO = "Pacientes con enfermedades preexistentes y familiares";
      break;
    case 6:
      $mmTITULO = "Profesionales de la salud (médic@s, enfermer@s, personal hospitalario)";
      break;
    case 7:
      $mmTITULO = "Ansiedad por temas relacionados a Covid -19";
      break;
    case 8:
      $mmTITULO = "Aislamiento social por covid";
      break;
    case 9:
      $mmTITULO = "Insomnio ";
      break;
  }

  $insertSQL = sprintf(
    "INSERT INTO pendiente (IDCLINTE, IDASIGNADO, IDQUIEN,  TITULO, ESTATUS, SOLICITUD, QUIEN, PRIORIDAD, ENTREGASOL) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
    GetSQLValueString(1, "int"), // queretaro= 1
    GetSQLValueString(2, "int"), // usaurio admin generico
    GetSQLValueString($ultimo_id, "int"), // el usuario que se registro
    GetSQLValueString($mmTITULO, "text"), // del combo
    GetSQLValueString(1, "int"), // 1= sin asignar
    GetSQLValueString(date('Y-m-d H:i:s'), "date"),
    GetSQLValueString($_POST['NOMBRE'], "text"),
    GetSQLValueString(1, "text"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date")
  );

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $insertSQL) or die(mysql_error($pendientes));
  $ultimo_id2 = mysqli_insert_id($pendientes);


  /////// inicia insert comentario  
  $mmTXTinicial = "¡Bienvenid@! Estás en el chat de atención psicológica.  En un momento uno de nuestros Psicólogos estará contigo.";
  $insertSQL = sprintf(
    "INSERT INTO coment (QUIEN, FECHA, IDPENDIENTE, ESTATUS,TIPO, COMENT) VALUES (%s, %s, %s, %s, %s, %s)",
    GetSQLValueString("Sistema Apoyo", "text"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date"),
    GetSQLValueString($ultimo_id2, "int"),
    GetSQLValueString(1, "int"),
    GetSQLValueString(1, "int"),
    GetSQLValueString($mmTXTinicial, "text")
  );
  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result2 = mysqli_query($pendientes, $insertSQL) or die(mysqli_error($pendientes));
  //// fin de insert de comentarios

  $updateGoTo = "inicio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }



  $insertGoTo = "detalle.php?ID=" . $ultimo_id2;
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  //// ///// ///// ///// ///// ///// ///// ///// ///// GENERA session
  if (isset($_GET['accesscheck'])) {
    $_SESSION['PrevUrl'] = $_GET['accesscheck'];
  }
  $loginUsername = $_POST['TEL'];
  $loginStrGroup = '5';
  $nombreStrGroup  = $_POST['NOMBRE'];
  $idquienStrGroup  = $ultimo_id;
  $mmSEUDONIMO = $_POST['NOMBRE'];

  if (PHP_VERSION >= 5.1) {
    session_regenerate_id(true);
  } else {
    session_regenerate_id();
  }
  //declare two session variables and assign them  

  $_SESSION['MM_Username'] = $loginUsername; // TELEFONO
  $_SESSION['MM_UserGroup'] = $loginStrGroup; // TIPO 5
  $_SESSION['MM_Nombre'] = $nombreStrGroup; // NOMBRE
  $_SESSION['MM_IdQuien'] = $idquienStrGroup; // ID usuario
  $_SESSION['MM_SEUDONIMO'] = $mmSEUDONIMO; // ID SEUDONIMO



  if (isset($_SESSION['PrevUrl']) && false) {
    $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
  }
  ///// FIN DE SESSION  
  ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// 
  header(sprintf("Location: %s", $insertGoTo));
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("google.php"); ?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Contacto Nikon">
    <meta name="author" content="aaltaan.com">
    <link rel="icon" href="/wp-content/uploads/2020/04/cropped-logo_512_ico-32x32.jpg" sizes="32x32" />
    <link rel="icon" href="/wp-content/uploads/2020/04/cropped-logo_512_ico-192x192.jpg" sizes="192x192" />
    <link rel="apple-touch-icon" href="/wp-content/uploads/2020/04/cropped-logo_512_ico-180x180.jpg" />
    <title>Apoyo Psicológico</title>

    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../lib/select2/select2.css">

    <link rel="stylesheet" href="../css/quirk.css">


    <script src="../lib/modernizr/modernizr.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->

</head>

<body class="signwrapper">
    <div class="sign-overlay"></div>
    <div class="signpanel"></div>
    <div class="panel signin">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
            <!-- <button class="btn btn-primary btn-quirk btn-fb btn-block">Sign Up Using Facebook</button>
            <div class="or">or</div>-->
            <p style="color: #D7D5D5; text-align: justify;">&nbsp;</p>
            <h1> Apoyo Psicológico </h1>
            <form action="<?php echo $loginFormAction; ?>" method="POST" name="form1" id="form1">
                <div class="form-group mb15"><br>
                    <input type="text" class="form-control" placeholder="Nombre Completo" name="NOMBRE" minlength="4"
                        maxlength="80" required>
                </div>
                <div class="form-group mb15">
                    <div class="form-group">
                        <select class="form-control" style="width: 100%" data-placeholder="Sexo" name="SEXO" required>
                            <option value="0">Sexo</option>
                            <option value="1">Femenino</option>
                            <option value="2">Masculino</option>
                        </select>
                    </div>
                </div>

                <div class="form-group mb15">
                    <input type="number" class="form-control" placeholder="Año de nacimiento (aaaa)" name="ANAC"
                        min="1920" max="2015" required />
                </div>

                <div class="form-group mb15">Selecciona el dato con el que harás tu usuario:<br>
                    <input id="platform" type="radio" name="tus" value="1" required>Teléfono <input type="radio"
                        name="tus" value="2"> Email
                    <input type="text" class="form-control" placeholder="Usuario" name="TEL" required>
                </div>

                <div class="form-group mb15">
                    <input type="password" class="form-control" placeholder="Contraseña" name="PSW" required>
                </div>

                <div class="form-group mb15"> Estado
                    <!-- estados-->
                    <div class="form-group">
                        <select class="form-control" style="width: 100%" data-placeholder="Estado" name="EDO" required>
                            <option value="0">Selecciona un estado</option>
                            <option value="1">AGUASCALIENTES</option>
                            <option value="2">BAJA CALIFORNIA NORTE</option>
                            <option value="3">BAJA CALIFORNIA SUR</option>
                            <option value="4">CAMPECHE</option>
                            <option value="5">COAHUILA</option>
                            <option value="6">CHIAPAS</option>
                            <option value="8">CHIHUAHUA</option>
                            <option value="9">CDMX</option>
                            <option value="10">DURANGO</option>
                            <option value="11">GUANAJUATO</option>
                            <option value="12">GUERRERO</option>
                            <option value="13">HIDALGO</option>
                            <option value="14">JALISCO</option>
                            <option value="15">EDOMEX</option>
                            <option value="16">MICHOACAN</option>
                            <option value="17">MORELOS</option>
                            <option value="18">NAYARIT</option>
                            <option value="19">NUEVO LEÓN</option>
                            <option value="20">OAXACA</option>
                            <option value="21">PUEBLA</option>
                            <option value="22">QUERETARO</option>
                            <option value="23">QUINTANA ROO</option>
                            <option value="24">SAN LUIS POTOSI</option>
                            <option value="25">SINALOA</option>
                            <option value="26">SONORA</option>
                            <option value="27">TABASCO</option>
                            <option value="28">TAMAULIPAS</option>
                            <option value="29">TLAXCALA</option>
                            <option value="30">VERACRUZ</option>
                            <option value="31">YUCATAN</option>
                            <option value="32">ZACATECAS</option>
                            <option value="33">COLIMA</option>
                        </select>
                    </div>
                </div>

                <div class="form-group mb15">Tipo de solicitud
                    <!--Opcion de solicitud-->
                    <div class="form-group">
                        <select class="form-control" style="width: 100%" data-placeholder="Solicitante" name="TITULO"
                            required>
                            <option value="0">Selecciona una opción</option>
                            <option value="1">Público en general</option>
                            <option value="2">Familiares o personas adictas (familiar o paciente)</option>
                            <option value="3">Víctimas de violencia familiar y/o abuso </option>
                            <option value="4">Pacientes con COVID-19 y familiares</option>
                            <option value="5">Pacientes con enfermedades preexistentes y familiares </option>
                            <option value="6">Profesionales de la salud (médic@s, enfermer@s, personal hospitalario)
                            </option>
                            <option value="7">Ansiedad por temas relacionados a Covid -19</option>
                            <option value="8">Aislamiento social por covid</option>
                            <option value="9">Insomnio</option>
                        </select>
                    </div>
                </div>



                <div class="form-group mb10">
                    <label class="ckbox">
                        <input type="checkbox" name="politicas" required>
                        <span><a href="https://apoyo-psicologico.mx/aviso-de-privacidad/" target="new">Acepto los
                                términos y condiciones</a><br>
                        </span>
                    </label>
                </div>
                <div class="text-center" style="padding: 15px;">Horario de atención de lunes a domingo de 08:00 a 00:00 h.</div>
                <div class="form-group">
                    <button class="btn btn-success btn-quirk btn-block"><i class="fa fa-user-plus"></i>
                        Registrarte</button>
                </div>
                <input type="hidden" name="mm_nuevo" value="form1" />
            </form>
            <div class="form-group">
                <a href="signin.php" class="btn btn-primary btn-quirk btn-block btn-stroke">Iniciar Sesión</a>
                <!--<a href="signin.php" class="btn btn-primary btn-quirk btn-block btn-stroke">Continuidad de consulta</a>-->
            </div>
        </div><!-- panel-body -->
    </div><!-- signup -->
    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../lib/select2/select2.js"></script>

    <script>
    $(function() {

        // Select2 Box
        $("select.form-control").select2({
            minimumResultsForSearch: Infinity
        });

    });
    </script>
    <!-- V1.0.1 -->
</body>

</html>