<?php require_once('../Connections/asesorias.php'); ?>
<?php require_once('acs.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
	{
		if (PHP_VERSION < 6) {
			$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		}
		global $hostname_pendientes, $database_pendientes, $username_pendientes, $password_pendientes, $pendientes;

		$theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

mysqli_query($pendientes, "SET NAMES 'utf8'");
mysqli_select_db($pendientes, $database_pendientes);
$query_pen = "SELECT b.NOMBRE as PSICOLOGO, e.* FROM EVAL e, usuarios b WHERE  e.IDPSICOLOGO=b.ID ORDER BY e.ID DESC";
$pen = mysqli_query($pendientes, $query_pen) or die(mysqli_error($pendientes));
$row_pen = mysqli_fetch_array($pen);
$mmP = "rp";
$rep = 3;
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Contacto Nikon">
    <meta name="author" content="aaltaan.com">
    <link rel="shortcut icon" href="/favicon.png">

    <title>apoyo-psicologico.mx - Inicio</title>

    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css">
    <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css">
    <link rel="stylesheet" href="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../lib/select2/select2.css">
    <link rel="stylesheet" href="../css/quirk.css">
    <script src="../lib/modernizr/modernizr.js"></script>
</head>
<body>

<header><?php include("header.php"); ?></header>
<section>
<div class="leftpanel">
  <?php include("leftpanel.php"); ?><!-- leftpanelinner -->
</div><!-- leftpanel -->

<div class="mainpanel">
  <div class="contentpanel">
    <div class="row">
      <div class="col-md-9 col-lg-8 dash-left"><!-- panel -->
        <div class="panel">
            <ul class="panel-options">
              <li><a><i class="fa fa-refresh"></i></a></li>
              <li><a class="panel-remove"><i class="fa fa-remove"></i></a></li>
            </ul>
          <div class="panel-heading">
            <h4 class="panel-title">Reporte de Evaluaciones</h4>
            En esta sección podrás generar los reportes referentes a las evaluaciones de las consultas.
          </div>
          <div class="panel-body">
            <div class="col-md-6 form-group">
              <div class="col-sm-5">
                <label for="min">Fecha Inicio</label><input type="date" name="min" id="min" class="form-control input-sm">
              </div>
              <div class="col-sm-5">
                <label for="max">Fecha Fin</label><input type="date" name="max" id="max" class="form-control input-sm">
              </div>
            </div>
            <div class="table-responsive col-md-12">
              <table id="tableRep" class="table table-bordered table-striped-col" width="90%" cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
				  	<td>PROBLEMAS DE ATENCIÓN</td>
					<td>PROBLEMAS DE SALUD</td>
					<td>PRE-DIAGNOSTICO DE TRANSTORNOS</td>
					<td>DESCRIPCION DE LA INTERVENCIÓN</td>
					<td>CONDUCTAS DE RIESGO DETECTADAS</td>
					<td>ACCIONES O ACUERDOS TOMADOS</td>
					<td>DERIVACION</td>
					<td style="display: none;">fecha_filtro</td>
					<td>FECHA</td>
					<td>PSICOLOGO</td>
                  </tr>
                </thead>
                <tbody>
                <?php do { ?>
        		<tr>
            		<td>
                	<?php
 						//los valores estan en el array $ArraymmPA
						if ($row_pen['PA']!=NULL)
						{
								$datarPA= explode(",", $row_pen['PA']); // convertimos el campoa en array
								$mmCUANTOSPA = count($datarPA); // contamos cuantos datos son
							if($mmCUANTOSPA>1) 
							{
								for ($i = 0; $i < $mmCUANTOSPA; $i++) 
									{
  										echo $PA[$datarPA[$i]].", ";
									} 
							} else {echo $PA[$row_pen['PA']];}
						} 
						else { echo "no selecciono"; }?>
            		</td>
            		<td>
                	<?php
 						//los valores estan en el array $PA
						if ($row_pen['PS']!=NULL)
						{
								$datarPS= explode(",", $row_pen['PS']); // convertimos el campoa en array
								$mmCUANTOSPS = count($datarPS); // contamos cuantos datos son
							if($mmCUANTOSPS>1){
								for ($i = 0; $i < $mmCUANTOSPS; $i++){
  									echo $PS[$datarPS[$i]].", ";
								} 
							}else {echo $PS[$row_pen['PS']];}
						}
						else{ echo "no selecciono"; }?>
                	<br><br>
            		</td>
            		<td>
                	<?php //los valores estan en el array $PD
						if ($row_pen['PD']!=NULL)
						{
							$datarPD= explode(",", $row_pen['PD']); // convertimos el campoa en array
							$mmCUANTOSPD = count($datarPD); // contamos cuantos datos son
								if($mmCUANTOSPD>1) 
								{
									for ($i = 0; $i < $mmCUANTOSPD; $i++) 
									{
  										echo $PD[$datarPD[$i]].", ";
									} 
								} else { echo $PD[$row_pen['PD']];}
						} 
						else { echo "no selecciono"; }
					?>
            		</td>
					<td><?php echo $row_pen['EVALDESCRIPCION']; ?></td>
					<td><?php echo $row_pen['EVACONDUCTAS']; ?></td>
					<td><?php echo $row_pen['EVALACCIONES']; ?></td>
					<td><?php echo $row_pen['EVALDERIVACION']; ?></td>
					<td style="display: none;"><?php $creteDate = new DateTime($row_pen['FECHA']);
                      $newDate = $creteDate->format('Y-m-d');
                      echo $newDate;?></td>
					<td><?php echo $row_pen['FECHA']; ?></td>
					<td><?php echo $row_pen['PSICOLOGO']; ?></td>
				</tr>
				<?php  } while ($row_pen = mysqli_fetch_array($pen)); ?>
                </tbody>
              </table>
              <?php
              mysqli_free_result($pen);//libera la memoria utilizada para generar la consulta y no saturar el sistema
              mysqli_close($pendientes);?>
            </div>
          </div><!-- end div.panel-body -->
        </div><!-- end div.panel -->
      </div><!-- col-md-9 -->
      <?php include("ban_lateral.php"); ?>
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

</section>
  
</body>

<script src="../lib/jquery/jquery.js"></script>
<script src="../lib/jquery-ui/jquery-ui.js"></script>
<script src="../lib/bootstrap/js/bootstrap.js"></script>
<script src="../lib/jquery-toggles/toggles.js"></script>
<script src="../lib/datatables/jquery.dataTables.js"></script>
<script src="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="../lib/select2/select2.js"></script>

<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>

<script src="../js/quirk.js"></script>

<script  type="text/javascript">
// Custom filtering function which will search data in column four between two values


$(document).ready(function() {
  

  $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min_date = document.getElementById("min").value;
      var min = new Date(min_date);
      var max_date = document.getElementById("max").value;
      var max = new Date(max_date);

      var startDate = new Date(data[7]);
      //window.confirm(startDate);
      if (!min_date && !max_date) {
        return true;
      }
      if (!min_date && startDate <= max) {
        return true;
      }
      if (!max_date && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
  );
//configuracion basica
var table = $('#tableRep').DataTable({
  "order": [[ 7, "desc" ]],
    dom: 'lBfrtip',
    buttons: [
		'excel'
          ],
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    
});//fin configuracion basica
    
$('#min, #max').change(function() {
    table.draw();
    console.log($(this).val());
  });

});//end document.ready
</script>
</html>