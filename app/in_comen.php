<?php
date_default_timezone_set('America/Mexico_City');
require_once('../Connections/asesorias.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}

if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }
    $hostname_pendientes = "localhost";
    $database_pendientes = "wwwasesorias";
    $username_pendientes = "webusasesor";
    $password_pendientes = "nikon12K@.2";
    $pendientes = mysqli_connect($hostname_pendientes, $username_pendientes, $password_pendientes, $database_pendientes) or trigger_error(mysql_error(), E_USER_ERROR);


    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($pendientes, $theValue) : mysqli_escape_string($mximg7, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}
$mm_ID = "-1";
$mm_IDCL = "-1";
if (isset($_GET['ID'])) {
  $mm_ID = $_GET['ID'];
}
if (isset($_GET['IDCL'])) {
  $mm_IDCL = $_GET['IDCL'];
}


mysqli_select_db($pendientes, $database_pendientes);
$query_usuario = sprintf("SELECT a.* FROM usuarios a WHERE a.ID=%s and a.IDEMAIL=%s", GetSQLValueString($mm_ID, "int"), GetSQLValueString($mm_IDCL, "text"));
$usuario = mysqli_query($pendientes, $query_usuario) or die(mysqli_error($pendientes));
$row_usuario = mysqli_fetch_array($usuario);
$totalRows_usuario = mysqli_num_rows($usuario);


/////////// insert nuevo 

$loginFormAction = $_SERVER['PHP_SELF'];
//$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $loginFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}



if ((isset($_POST["mm_cnuevo"])) && ($_POST["mm_cnuevo"] == "form2")) {

  ////// cASE PERTENECE
  $mmDQemail = $_POST['IDASIGNADO']; // Recibe pertenece
  switch ($mmDQemail) {
    case 6:
      $mmQmail = "jmunoz@ayp.mx";
      $mmQmailMan = "vmartinez@ayp.mx";
      break;
    case 7:
      $mmQmail = "vmartinez@ayp.mx";
      $mmQmailMan = "jmunoz@ayp.mx";
      break;
    case 8:
      $mmQmail = "jrcordova@ayp.mx";
      $mmQmailMan = "hector.esquivel@ayp.mx";
      break;
    case 9:
      $mmQmail = "hector.esquivel@ayp.mx";
      $mmQmailMan = "jrcordova@ayp.mx";
      break;
    case 10:
      $mmQmail = "lflores@ayp.mx";
      $mmQmailMan = "jmherrera@ayp.mx";
      break;
    case 11:
      $mmQmail = "jmherrera@ayp.mx";
      $mmQmailMan = "lflores@ayp.mx";
      break;
    case 12:
      $mmQmail = "tonatiuh.martinez@ayp.mx";
      $mmQmailMan = "rsanchez@ayp.mx";
      break;
    case 13:
      $mmQmail = "rsanchez@ayp.mx";
      $mmQmailMan = "tonatiuh.martinez@ayp.mx";
      break;
    case 14:
      $mmQmail = "sasanchez@ayp.mx";
      $mmQmailMan = "hgarcia@ayp.com.mx";
      break;
    case 106:
      $mmQmail = "hgarcia@ayp.com.mx";
      $mmQmailMan = "sasanchez@ayp.mx";
      break;
  }
  ///////////////////////// DEFINE MANCUERNA
  $mmMANCUERNA = $_SESSION['MM_IdQuien'];
  if ($_SESSION['MM_IdQuien'] == 6) {
    $mmMANCUERNA = "7";
  }
  if ($_SESSION['MM_IdQuien'] == 7) {
    $mmMANCUERNA = "6";
  }
  if ($_SESSION['MM_IdQuien'] == 8) {
    $mmMANCUERNA = "9";
  }
  if ($_SESSION['MM_IdQuien'] == 9) {
    $mmMANCUERNA = "8";
  }
  if ($_SESSION['MM_IdQuien'] == 10) {
    $mmMANCUERNA = "11";
  }
  if ($_SESSION['MM_IdQuien'] == 11) {
    $mmMANCUERNA = "10";
  }
  if ($_SESSION['MM_IdQuien'] == 12) {
    $mmMANCUERNA = "13";
  }
  if ($_SESSION['MM_IdQuien'] == 13) {
    $mmMANCUERNA = "12";
  }
  if ($_SESSION['MM_IdQuien'] == 14) {
    $mmMANCUERNA = "106";
  }
  if ($_SESSION['MM_IdQuien'] == 106) {
    $mmMANCUERNA = "14";
  }



  $mmTITULO = $_POST['PRODUCTO'] . " / " . $_POST['TIPO'] . " / " . $_POST['TIPODET'];

  $insertSQL = sprintf(
    "INSERT INTO pendiente (IDCLINTE, IDASIGNADO, IDQUIEN,  TITULO, ESTATUS, SOLICITUD, QUIEN, PRIORIDAD, ENTREGASOL) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
    GetSQLValueString(1, "int"),
    GetSQLValueString($_POST['IDASIGNADO'], "int"),
    GetSQLValueString($_POST['IDQUIEN'], "int"),
    GetSQLValueString($mmTITULO, "text"),
    GetSQLValueString(1, "int"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date"),
    GetSQLValueString($_POST['QUIEN'], "text"),
    GetSQLValueString(2, "text"),
    GetSQLValueString(date('Y-m-d H:i:s'), "date")
  );

  mysqli_query($pendientes, "SET NAMES 'utf8'");
  mysqli_select_db($pendientes, $database_pendientes);
  $Result1 = mysqli_query($pendientes, $insertSQL) or die(mysql_error($pendientes));
  $ultimo_id = mysqli_insert_id($pendientes);

  /////// inicia insert comentario  
  if ($_POST['COMENT'] != null) {
    $insertSQL2 = sprintf(
      "INSERT INTO coment (QUIEN, FECHA, IDPENDIENTE, ESTATUS, COMENT) VALUES (%s, %s, %s, %s, %s)",
      GetSQLValueString($_POST['QUIEN'], "text"),
      GetSQLValueString(date('Y-m-d H:i:s'), "date"),
      GetSQLValueString($ultimo_id, "int"),
      GetSQLValueString(1, "int"),
      GetSQLValueString($_POST['COMENT'], "text")
    );
    mysqli_query($pendientes, "SET NAMES 'utf8'");
    mysqli_select_db($pendientes, $database_pendientes);
    $Result2 = mysqli_query($pendientes, $insertSQL2) or die(mysql_error($pendientes));
  } //// fin de insert de comentarios

  $insertGoTo = "inicio.php?E=1";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }


  //-- ARMA MAIL
  $dest = $mmQmail;
  $head = "From: Pendientes <contacto@contactonikon.com>\r\n";
  $head .= "To: " . $mmQmail . "\r\n";
  $head .= "Cc: " . $mmQmailMan . "\r\n";
  $head .= "Bcc: guillermo@lohechoenmexico.mx\r\n";
  $head .= "MIME-Version: 1.0\r\n";
  $head .= "Content-type: text/html; charset=utf-8\r\n";
  // cuerpo del mensaje
  $msg = "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
  $msg .= "<title>Nikon</title></head><body style=\"font-family:Arial, Helvetica;\">\n";
  $msg .= "<h5>contactonikon.com</h5><h3>\n";
  $msg .= "<br /><br />Has recibido un pendiente mediante el la herramineta.<br />\n";
  $msg .= "<br />La solicitud de pendiente es la  <strong>" . $ultimo_id . "</strong><br /><br /></h3>\n";
  $msg .= "El tiempo de respuesta debe ser no mayor a 5 min. Para conocer el detalle, ingresa a <a href=\"http://www.lohechoenmexico.mx/asesorias/\"> panel de administración</a>\n";
  $msg .= "<br>Favor de no responder a este correo.<br /><br />\n";
  $msg .= "<h6>Todos los derechos reservdos contactonikon.com 2020<br/><br/></h6>\n";
  $msg .= "</body></html>\n";

  // envia mail
  mail($dest, "Solicitud Nikon - " . $ultimo_id, $msg, $head);
  //-- TERMINA MAIL	


  //// ///// ///// ///// ///// ///// ///// ///// ///// GENERA session
  if (isset($_GET['accesscheck'])) {
    $_SESSION['PrevUrl'] = $_GET['accesscheck'];
  }
  $loginUsername = $_POST['EMAIL'];
  $loginStrGroup = '5';
  $nombreStrGroup  = $_POST['QUIEN'];
  $idquienStrGroup  = $_POST['IDQUIEN'];


  if (PHP_VERSION >= 5.1) {
    session_regenerate_id(true);
  } else {
    session_regenerate_id();
  }
  //declare two session variables and assign them  

  $_SESSION['MM_Username'] = $loginUsername; // email
  $_SESSION['MM_UserGroup'] = $loginStrGroup; // TIPO
  $_SESSION['MM_Nombre'] = $nombreStrGroup; // NOMBRE
  $_SESSION['MM_IdQuien'] = $idquienStrGroup; // ID usuario


  if (isset($_SESSION['PrevUrl']) && false) {
    $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
  }
  ///// FIN DE SESSION  
  ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// 	


  header(sprintf("Location: %s", $insertGoTo));
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Contacto Nikon">
  <meta name="author" content="aaltaan.com">
  <link href="../images/favicon.ico" rel="SHORTCUT ICON">

  <title>Cont@cto Nikon</title>


  <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
  <link rel="stylesheet" href="../lib/select2/select2.css">

  <link rel="stylesheet" href="../css/quirk.css">

  <script src="../lib/modernizr/modernizr.js"></script>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->


</head>

<body class="signwrapper">

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <div class="signup">
    <div class="row">
      <div class="col-sm-5">
        <div class="panel">
          <div class="panel-heading">
            <h1>Cont@cto Nikon</h1>
          </div>
          <div class="panel-body">
            <!-- <button class="btn btn-primary btn-quirk btn-fb btn-block">Sign Up Using Facebook</button>
            <div class="or">or</div>-->
            <?php if ($totalRows_usuario >= 1) { ?>
              <form action="<?php echo $loginFormAction; ?>" method="POST" name="form2" id="form2">
                <div class="form-group mb15">
                  <?php echo $row_usuario['NOMBRE']; ?>
                </div>
                <div class="form-group mb15">
                  <textarea id="autosize" class="form-control" rows="5" name="COMENT" placeholder="Escribe tu Duda" required></textarea>
                </div>



                <div class="row mb15">
                  <div class="col-xs-8">
                    <div class="form-group" id="sProducto">
                    </div>
                  </div>
                </div>

                <div class="row mb15">
                  <div class="col-xs-8">
                    <div class="form-group" id="sTIPO">
                    </div>
                  </div>
                </div>

                <div class="row mb15">
                  <div class="col-xs-8">
                    <div class="form-group" id="subTIPO">
                    </div>
                  </div>
                </div>



                <input type="hidden" name="EMAIL" value="<?php echo $row_usuario['EMAIL']; ?>" />
                <input type="hidden" name="USTIPO" value="5" />
                <input type="hidden" name="IDASIGNADO" value="<?php echo $row_usuario['PERTENECE']; ?>" />
                <input type="hidden" name="IDQUIEN" value="<?php echo $row_usuario['ID']; ?>" />
                <input type="hidden" name="QUIEN" value="<?php echo $row_usuario['NOMBRE']; ?>" />
                <input type="hidden" name="mm_cnuevo" value="form2" />
                <div class="form-group">
                  <button class="btn btn-success btn-quirk btn-block">Enviar</button>
                </div>
              </form>
            <?php } ?>
          </div><!-- panel-body -->
        </div><!-- panel -->
      </div><!-- col-sm-5 -->
      <div class="col-sm-7">
        <div class="sign-sidebar">
          <h4 class="signtitle mb20"><strong>Bienveid@ </strong></h4>
          <p>Estimado Nikonista, bienvenido a este espacio virtual de consultas que fue diseñado para conectar con nuestros expertos de marca, quienes podrán apoyarte a resolver dudas de producto, características técnicas, compatibilidad de accesorios, precios, promociones y dónde comprar. </p>

          <br>

          <h4 class="reason">1. Ahora solo envíanos tu duda</h4>
          <p>Para que nuestros expertos puedan ponerse en contacto contigo.</p>

          <br>

          <h4 class="reason">2. Cómo funciona “Cont@cto Nikon”</h4>
          <p>1. Ingresa tus datos en la sección “Registro”<br>
            2. Escríbe tu duda. <br>

            3. Te enviaremos un correo electrónico a la dirección ingresada con el número de folio para el seguimiento a tu solicitud.<br>
            4. Tu información será enviada a nuestros expertos para asignar el ticket de atención.<br>
            5. Nuestro personal se pondrá en contacto contigo en periodo máximo de 5 minutos.
          </p>

          <hr class="invisible">

          <div class="form-group">
            <a href="signin.php" class="btn btn-default btn-quirk btn-stroke btn-stroke-thin btn-block btn-sign">Ya estas inscrit@? Entra ahora!</a>
          </div>
        </div><!-- sign-sidebar -->
      </div>
    </div>
  </div><!-- signup -->



  <script src="../lib/jquery/jquery.js"></script>
  <script src="../lib/bootstrap/js/bootstrap.js"></script>
  <script src="../lib/select2/select2.js"></script>

  <script>
    $(function() {

      // Select2 Box
      $("select.form-control").select2({
        minimumResultsForSearch: Infinity
      });

    });
  </script>
  <script>
    $(document).ready(function() {
      var arr = ['Producto', 'Cámara', 'Lente', 'Accesorio'];
      producto = '<select name="PRODUCTO" class="form-control" style="width: 100%" data-placeholder="Producto"  onchange="p_tipo(this.value)" required>';
      for (i = 0; i < arr.length; i++) {
        producto += '<option value="' + arr[i] + '">' + arr[i] + '</option>';
      };
      producto += '</select>';
      $("#sProducto").html(producto);


    })

    function p_tipo(vtipo) {

      if (vtipo == 'Producto') {
        var arr_tipo = ['Selecciona un producto'];
        v_tipo = '<select name="TIPO" class="form-control" style="width: 100%" data-placeholder="Tipo"  required>';
      }

      if (vtipo == 'Cámara') {
        var arr_tipo = ['Tipo de cámara', 'Sin espejo serie Z', 'DSLR', 'Compactas'];
        v_tipo = '<select name="TIPO" class="form-control" style="width: 100%" data-placeholder="Tipo" onchange="p_tipo_detalle(this.value)" required>';
      }

      if (vtipo == 'Lente') {
        var arr_tipo = ['Tipo de lente', 'ML', 'DSLR'];
        v_tipo = '<select name="TIPO" class="form-control" style="width: 100%" data-placeholder="Tipo" required>';
      }

      if (vtipo == 'Accesorio') {
        var arr_tipo = ['Tipo de accesorio', 'ML', 'DSLR'];
        v_tipo = '<select name="TIPO" class="form-control" style="width: 100%" data-placeholder="Tipo" required>';
      }


      for (i = 0; i < arr_tipo.length; i++) {
        v_tipo += '<option value="' + arr_tipo[i] + '">' + arr_tipo[i] + '</option>';
      };
      v_tipo += '</select>';
      $("#sTIPO").html(v_tipo);
      document.getElementById('subTIPO').innerHTML = '';

    }

    function p_tipo_detalle(vtipo_det) {

      if (vtipo_det == 'Sin espejo serie Z') {
        var arr_tipo_det = ['Modelo serie Z', 'Z7', 'Z6', 'Z50'];
      }
      if (vtipo_det == 'DSLR') {
        var arr_tipo_det = ['Modelo DSLR', 'D6', 'D5', 'D780', 'D750', 'D500', 'D7500', 'D5600', 'D3500'];
      }
      if (vtipo_det == 'Compactas') {
        var arr_tipo_det = ['Modelo de compacta', 'Acuatica w300', 'Acuatica w150', 'HiZoom P1000'];
      }

      v_tipo_det = '<select name="TIPODET" class="form-control" style="width: 100%" data-placeholder="Tipo" required>';
      for (i = 0; i < arr_tipo_det.length; i++) {
        v_tipo_det += '<option value="' + arr_tipo_det[i] + '">' + arr_tipo_det[i] + '</option>';
      };
      v_tipo_det += '</select>';
      $("#subTIPO").html(v_tipo_det);


    }
  </script>
  <?php mysqli_free_result($usuario); ?>
</body>

</html>